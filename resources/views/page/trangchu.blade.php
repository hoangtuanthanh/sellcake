@extends('layout.index')

@section('content')

<div class="fullwidthbanner-container">
	<div class="fullwidthbanner">
		<div class="bannercontainer" >
			<div class="banner" >
				<ul>
					@foreach($slide as $sl)
					<li data-transition="boxfade" data-slotamount="20" class="active-revslide" style="width: 100%; height: 100%; overflow: hidden; z-index: 18; visibility: hidden; opacity: 0;">
						<div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="undefined" data-kenburns="undefined" data-easeme="undefined" data-bgfit="undefined" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined">
							<div class="tp-bgimg defaultimg" data-lazyload="undefined" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" data-lazydone="undefined" src="frontend/slide/{{$sl->image}}" data-src="frontend/slide/{{$sl->image}}" style="background-color: rgba(0, 0, 0, 0); background-repeat: no-repeat; background-image: url('frontend/slide/{{$sl->image}}'); background-size: cover; background-position: center center; width: 100%; height: 100%; opacity: 1; visibility: inherit;">
							</div>
						</div>
					</li>
					@endforeach
				</ul>
			</div>	
		</div>
		<div class="tp-bannertimer"></div>
	</div>
</div>
<!--slider-->
</div>
<div class="container">
	<div id="content" class="space-top-none">
		<div class="main-content">
			<div class="space60">&nbsp;</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="beta-products-list">
						<div class="row">
							<div class="col-sm-4">
								<h4 class="{{-- wow --}} fadeInRight">Sản Phẩm Mới</h4>
							</div>
						</div>
						<div class="beta-products-details">
							<p class="pull-left">Tìm thấy {{count($new_product)}} sản phẩm</p>
							<div class="clearfix"></div>
						</div>
						<div class="row" >
							@foreach($new_product as $new)
							<div class="col-sm-3">
								<div class="single-item">
									<div class="single-item-header">
										<a href="{{route('chitietsp',$new->id)}}"><img class="{{-- wow --}} flash" src="frontend/product/{{$new->image}}" alt=""></a>
									</div>
									<div class="single-item-body">
										@if($new->promotion_pice != 0)
										<div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
										@endif
										<p class="single-item-title">{{$new->name}}<span>@if($new->soluong == $new->daban || $new->hansd < Carbon\Carbon::now()) <span style="padding: 3px;border-radius: 2px" class="btn-danger">Hết hàng</span> @endif</span></p>
										<p class="single-item-price">
											@if($new->promotion_pice == 0)
											<span class="flash-sale">{{number_format($new->unit_price)}} VNĐ</span>
											@else
											<span class="flash-del">{{number_format($new->unit_price)}} VNĐ</span>
											<span class="flash-sale">{{number_format($new->promotion_pice)}} VNĐ</span>
											@endif
										</p>
									</div>
									<div class="single-item-caption {{-- wow --}} bounceInLeft">
										@if($new->soluong > $new->daban && $new->hansd > Carbon\Carbon::now())
										<a class="add-to-cart pull-left" href="{{route('themgiohang',$new->id)}}"><i class="fa fa-shopping-cart"></i></a>
										@endif
										<a class="beta-btn primary "  href="{{route('chitietsp',$new->id)}}">Chi tiết <i class="fa fa-chevron-right"></i></a>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						{{-- <div class="row">{{ $new_product->links() }}</div> --}}
						<div class="row">{{ $new_product->appends(Request::all())->links() }}</div>

					
					</div> <!-- .beta-products-list -->
					
					<div class="space20">&nbsp;</div>

					<div class="beta-products-list">
						<h4 class="{{-- wow --}} fadeInRight"> Phẩm Khuyến Mãi</h4>
						<div class="beta-products-details">
							<p class="pull-left">Tìm thấy {{count($sp_kmai)}} sản phẩm</p>
							<div class="clearfix"></div>
						</div>
						<div class="row">
							@foreach($sp_kmai as $spkm)
							<div class="col-sm-3">			
								<div class="single-item">
									<div class="single-item-header">
										<a href="{{route('chitietsp',$spkm->id)}}"><img class="{{-- wow --}} flash" src="frontend/product/{{$spkm->image}}" alt=""></a>
									</div>
									<div class="single-item-body">
										@if($spkm->promotion_pice != 0)
										<div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
										@endif
										<p class="single-item-title">{{$spkm->name}} <span>@if($spkm->soluong == $spkm->daban || $spkm->hansd <Carbon\Carbon::now()) <span style="padding: 3px;border-radius: 2px" class="btn-danger">Hết hàng</span> @endif</span></p>
										<p class="single-item-price">
											<span class="flash-del">{{number_format($spkm->unit_price)}} VNĐ</span>
											<span class="flash-sale">{{number_format($spkm->promotion_pice)}} VNĐ</span>

										</p>
									</div>
									<div class="single-item-caption {{-- wow --}} bounceInLeft" >
										@if($spkm->soluong > $spkm->daban && $spkm->hansd > Carbon\Carbon::now())
											<a class="add-to-cart pull-left" href="{{route('themgiohang',$spkm->id)}}"><i class="fa fa-shopping-cart"></i></a>
										@endif
										<a class="beta-btn primary " href="{{route('chitietsp',$spkm->id)}}">Chi tiết <i class="fa fa-chevron-right"></i></a>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						<div class="row">{{ $sp_kmai->appends(Request::all())->links() }}</div>

						
					</div> <!-- .beta-products-list -->
					<div class="space20">&nbsp;</div>

					<div class="beta-products-list">
						<h4 class="{{-- wow --}} fadeInRight">Sản Phẩm Bán Chạy</h4>
						<div class="beta-products-details">
							<p class="pull-left">Tìm thấy {{count($sp_banchay)}} sản phẩm</p>
							<div class="clearfix"></div>
						</div>
						<div class="row">
							@foreach($sp_banchay as $spbc)
							<div class="col-sm-3">			
								<div class="single-item">
									<div class="single-item-header">
										<a href="{{route('chitietsp',$spbc->id)}}"><img class="{{-- wow --}} flash" src="frontend/product/{{$spbc->image}}" alt=""></a>
									</div>
									<div class="single-item-body">
										@if($spbc->promotion_pice != 0)
										<div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
										@endif
										<p class="single-item-title">{{$spbc->name}} <span>@if($spbc->soluong == $spbc->daban || $spbc->hansd < Carbon\Carbon::now()) <span style="padding: 3px;border-radius: 2px" class="btn-danger">Hết hàng</span> @endif</span></p>
										<p class="single-item-price">
											@if($spbc->promotion_pice != 0)
											<span class="flash-del">{{number_format($spbc->unit_price)}} VNĐ</span>
											<span class="flash-sale">{{number_format($spbc->promotion_pice)}} VNĐ</span>
											@else
											<span class="flash-sale">{{number_format($spbc->unit_price)}} VNĐ</span>
											@endif
										</p>
									</div>
									<div class="single-item-caption {{-- wow --}} bounceInLeft">
										@if($spbc->soluong > $spbc->daban && $spbc->hansd > Carbon\Carbon::now())
										<a class="add-to-cart pull-left" href="{{route('themgiohang',$spbc->id)}}"><i class="fa fa-shopping-cart"></i></a>
										@endif
										<a class="beta-btn primary" href="{{route('chitietsp',$spbc->id)}}">Chi tiết <i class="fa fa-chevron-right"></i></a>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						<div class="row">{{ $sp_banchay->appends(Request::all())->links() }}</div> 
				
						{{-- <div class="space20">&nbsp;</div> --}}
						
					</div> <!-- .beta-products-list -->
				</div>
			</div> {{-- end section with sidebar and main content --}} 
		</div> <!-- .main-content -->
	</div> <!-- #content -->
</div>	<!-- .container -->

@endsection
@section('script')
<script>
	function AddCart(id){
		// console.log(id);
		$.ajax({
			url: '',
			type: 'GET',
		}).done(function(responese){
			console.log(responese);
		});
	}
</script>
@endsection

