@extends('layout.index')
@section('content')

<div class="inner-header">
	<div class="container">
		<div class="pull-left">
			<h6 class="inner-title">Sản phẩm {{$loai_sp->name}}</h6>
		</div>
		<div class="pull-right">
			<div class="beta-breadcrumb font-large">
				<a href="{{route('index')}}">Home</a> / <span>Loại Sản phẩm</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="container">
	<div id="content" class="space-top-none">
		<div class="main-content">
			<div class="space60">&nbsp;</div>
			<div class="row">
				<div class="col-sm-3">
					<ul class="aside-menu" id="catID">
						@foreach($loai as $l)
						<li><a href="{{route('loaisp',$l->id)}}">{{$l->name}}</a></li>
						@endforeach
					</ul>
					<div class="space30">&nbsp;</div><hr>
				</div>
				<div class="col-sm-9 ">
					<div class="beta-products-list">
						<h4>Sản phẩm mới</h4>
						<div class="beta-products-details">
							<p class="pull-left">Tìm Thấy {{count($sp_theoloai)}} Sản Phẩm</p>
							<div class="clearfix"></div>
						</div>
						<div class="row" id="dataProduct">
							@foreach($sp_theoloai as $sp)
							<div class="col-sm-4">
								<div class="single-item">
									<div class="single-item-header">
										<a href=""><img src="frontend/product/{{$sp->image}}" alt=""></a>
									</div>
									<div class="single-item-body">
										@if($sp->promotion_pice != 0)
										<div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
										@endif
										<p class="single-item-title">{{$sp->name}}<span>@if($sp->soluong == $sp->daban || $sp->hansd < Carbon\Carbon::now()) <span style="padding: 3px;border-radius: 2px" class="btn-danger">Hết hàng</span> @endif</span></p>
										<p class="single-item-price">
											@if($sp->promotion_pice == 0)
											<span class="flash-sale">{{number_format($sp->unit_price)}} VNĐ</span>
											@else
											<span class="flash-del">{{number_format($sp->unit_price)}} VNĐ</span>
											<span class="flash-sale">{{number_format($sp->promotion_pice)}} VNĐ</span>
											@endif
										</p>
									</div>
									<div class="single-item-caption">
										@if($sp->soluong > $sp->daban && $sp->hansd > Carbon\Carbon::now())
											<a class="add-to-cart pull-left" href="{{route('themgiohang',$sp->id)}}"><i class="fa fa-shopping-cart"></i></a>
										@endif
										<a class="beta-btn primary" href="{{route('chitietsp',$sp->id)}}">Chi tiết <i class="fa fa-chevron-right"></i></a>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						<div class="row">{{$sp_theoloai->links()}}</div>
					</div> <!-- .beta-products-list -->

					<div class="space30">&nbsp;</div>

					<div class="beta-products-list">
						<h4>Sản Phẩm Khác</h4>
						<div class="beta-products-details">
							<p class="pull-left">Tìm Thấy {{count($sp_khac)}} Sản Phẩm</p>
							<div class="clearfix"></div>
						</div>
						<div class="row">
							@foreach($sp_khac as $spk)
							<div class="col-sm-4">
								<div class="single-item">
									<div class="single-item-header">
										<a href="product.html"><img src="frontend/product/{{$spk->image}}" alt=""></a>
									</div>
									<div class="single-item-body">
										@if($spk->promotion_pice != 0)
										<div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
										@endif
										<p class="single-item-title">{{$spk->name}}<span>@if($spk->soluong == $spk->daban || $spk->hansd < Carbon\Carbon::now()) <span style="padding: 3px;border-radius: 2px" class="btn-danger">Hết hàng</span> @endif</span></p>
										<p class="single-item-price">
											@if($spk->promotion_pice == 0)
											<span class="flash-sale">{{number_format($spk->unit_price)}} VNĐ</span>
											@else
											<span class="flash-del">{{number_format($spk->unit_price)}} VNĐ</span>
											<span class="flash-sale">{{number_format($spk->promotion_pice)}} VNĐ</span>
											@endif
										</p>
									</div>
									<div class="single-item-caption">
										@if($spk->soluong > $spk->daban && $spk->hansd > Carbon\Carbon::now())
											<a class="add-to-cart pull-left" href="{{route('themgiohang',$spk->id)}}"><i class="fa fa-shopping-cart"></i></a>
										@endif
										<a class="beta-btn primary" href="{{route('chitietsp',$spk->id)}}">Chi tiết <i class="fa fa-chevron-right"></i></a>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							@endforeach
							
						</div>
						<div class="row">{{$sp_theoloai->links()}}</div>
						<div class="space30">&nbsp;</div>
						
					</div> <!-- .beta-products-list -->
				</div>
			</div> <!-- end section with sidebar and main content -->
		</div> <!-- .main-content -->
	</div> <!-- #content -->
</div> <!-- .container -->

@endsection
@section('script')
	<script>
		function SubmitForm(formId) {
	    var oForm = document.getElementById(formId);
	    if (oForm) {
	        oForm.submit(); 
	    }
	    else {
	        alert("DEBUG - could not find element " + formId);
	    }
	}
	</script>
	
@endsection