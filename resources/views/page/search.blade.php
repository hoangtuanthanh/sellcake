@extends('layout.index')
@section('tilte','Tìm Kiếm')
@section('content')

<!--slider-->
</div>
<div class="container">
	<div id="content" class="space-top-none">
		<div class="main-content">
			<div class="space60">&nbsp;</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="beta-products-list">
						
						<h4>Thông Tin Tìm Kiếm</h4>
						<div class="beta-products-details">
							<p class="pull-left">Tìm thấy {{count($product)}} sản phẩm</p>
							<div class="clearfix"></div>
						</div>
						<div class="row">
							@foreach($product as $new)
							<div class="col-sm-3">
								<div class="single-item">
									<div class="single-item-header">
										<a href="{{route('chitietsp',$new->id)}}"><img src="frontend/product/{{$new->image}}" alt=""></a>
									</div>
									<div class="single-item-body">
										@if($new->promotion_pice != 0)
										<div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
										@endif
										<p class="single-item-title">{{$new->name}} <span>@if($new->soluong == $new->daban || $new->hansd < Carbon\Carbon::now()) <span style="padding: 3px;border-radius: 2px" class="btn-danger">Hết hàng</span> @endif</span></p>
										<p class="single-item-price">
											@if($new->promotion_pice == 0)
											<span class="flash-sale">{{number_format($new->unit_price)}} VNĐ</span>
											@else
											<span class="flash-del">{{number_format($new->unit_price)}} VNĐ</span>
											<span class="flash-sale">{{number_format($new->promotion_pice)}} VNĐ</span>
											@endif
										</p>
									</div>
									<div class="single-item-caption">
										@if($new->soluong > $new->daban && $new->hansd > Carbon\Carbon::now())
										<a class="add-to-cart pull-left" href="{{route('themgiohang',$new->id)}}"><i class="fa fa-shopping-cart"></i></a>
										@endif
										<a class="beta-btn primary" href="{{route('chitietsp',$new->id)}}">Details <i class="fa fa-chevron-right"></i></a>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					
					</div> <!-- .beta-products-list -->

				</div>
			</div> {{-- end section with sidebar and main content --}} 
		</div> <!-- .main-content -->
	</div> <!-- #content -->
	<!-- .container -->

	@endsection

