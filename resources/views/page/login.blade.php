@extends('layout.index')
@section('title','Home | login')
@section('content')

<div class="inner-header">
	<div class="container">
		<div class="pull-left">
			<h6 class="inner-title">Đăng nhập</h6>
		</div>
		<div class="pull-right">
			<div class="beta-breadcrumb">
				<a href="{{route('index')}}">Trang chủ</a> / <span>Đăng nhập</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<div class="container">
	<div id="content">
		<form action="{{route('login')}}" method="post" class="beta-form-checkout">
			@csrf()
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<h4>Đăng nhập</h4>
					<div class="space20">&nbsp;</div>
					<div class="form-block">
						<label >Email address</label>
						<input type="text" name="email" placeholder="Nhập email ...">
					</div>
					<div class="form-block">
						<label >Password</label>
						<input type="password" name="password" placeholder="Nhập password ..." >
					</div>
					<div class="form-block">
						<button type="submit" class="btn btn-primary">Đăng nhập</button>
						<a href="">Đăng kí tài khoản</a>
					</div>
					@if(Session::has('thongbao'))
					<div class="alert alert-danger">
						{{Session::get('thongbao')}}
					</div>
					@endif
				</div>
				<div class="col-sm-3"></div>
			</div>
		</form>
	</div> <!-- #content -->
</div> <!-- .container -->

@endsection