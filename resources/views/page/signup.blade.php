@extends('layout.index')
@section('content')

<div class="inner-header">
	<div class="container">
		<div class="pull-left">
			<h6 class="inner-title">Đăng kí</h6>
		</div>
		<div class="pull-right">
			<div class="beta-breadcrumb">
				<a href="{{route('index')}}">Trang chủ</a> / <span>Đăng kí</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<div class="container">
	<div id="content">
		
		<form action="" method="post">
			@csrf
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					
					<h4>Đăng kí</h4>
					<div class="space20">&nbsp;</div>
					<div class="form-block">
						<label for="email">Email*</label>
						<input type="email" name="email" value="{{old('email')}}" >
						<p class="help is-danger" style="color: red">{{ $errors->first('email') }}</p>
					</div>
					<div class="form-block">
						<label for="your_last_name">Họ tên*</label>
						<input type="text" id="your_last_name" value="{{old('name')}}" name="name" >
						<p class="help is-danger" style="color: red">{{ $errors->first('name') }}</p>
					</div>
					<div class="form-block">
						<label for="adress">Địa chỉ*</label>
						<input type="text" id="address" value="{{old('address')}}" placeholder="Nhập địa chỉ..."name="address" >
						<p class="help is-danger" style="color: red">{{ $errors->first('address') }}</p>
					</div>
					<div class="form-block">
						<label for="phone">Số điện thoại*</label>
						<input type="text" name="phone" value="{{old('phone')}}" >
						<p class="help is-danger" style="color: red">{{ $errors->first('phone') }}</p>
					</div>
					<div class="form-block">
						<label for="phone">Password*</label>
						<input type="password" name="password" value="{{old('password')}}" >
						<p class="help is-danger" style="color: red">{{ $errors->first('password') }}</p>
					</div>
					<div class="form-block">
						<label for="phone">Nhập lại password*</label>
						<input type="password" name="repassword" value="{{old('repassword')}}" >
						<p class="help is-danger" style="color: red">{{ $errors->first('repassword') }}</p>
					</div>
					<div class="form-block">
						<button type="submit" class="btn btn-primary">Đăng kí</button>
					</div>
					@if(Session::has('thongbao1'))
					<div class="alert alert-success">{{Session::get('thongbao1')}}</div>
					@endif
				</div>
				<div class="col-sm-3"></div>
			</div>
		</form>
	</div> <!-- #content -->
</div> <!-- .container -->

@endsection