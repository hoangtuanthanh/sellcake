@extends('layout.index')

@section('content')

<div class="inner-header">
	<div class="container">
		<div class="pull-left">
			<h6 class="inner-title">Giới thiệu</h6>
		</div>
		<div class="pull-right">
			<div class="beta-breadcrumb font-large">
				<a href="{{route('index')}}">Trang chủ</a> / <span>Giới thiệu</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="container">
	<div id="content">
		<div class="our-history">
			<h2 class="text-center wow fadeInDown">Mốc của cửa hàng đã đạt được</h2>
			<div class="space35">&nbsp;</div>

			<div class="history-slider">
				@foreach($about as $ab)
				<div class="history-navigation">
					<a data-slide-index="{{$ab->id}}" href="blog_with_2sidebars_type_e.html" class="circle"><span class="auto-center">{{$ab->time}}</span></a>
				</div>
				<div class="history-slides">
					<div> 
						<div class="row">
						<div class="col-sm-5">
							<img src="frontend/about/{{$ab->image}}" alt="">
						</div>
						<div class="col-sm-7">
							<h5 class="other-title">{{$ab->title}}</h5>
							<p>
								{{$ab->address}}
							</p>
							<div class="space20">&nbsp;</div>
							<p>{!!$ab->content!!}</p>
						</div>
						</div> 
					</div>
				</div>
				@endforeach
			</div>
		</div>

		<div class="space50">&nbsp;</div>
		<hr />
		<div class="space50">&nbsp;</div>
		<h2 class="text-center wow fadeInDown">Niềm đam mê của chúng tôi cho những gì chúng tôi làm chuyển vào dịch vụ của chúng tôi</h2>
		<div class="space20">&nbsp;</div>
		<p class="text-center wow fadeInLeft"> </p>
		<div class="space35">&nbsp;</div>

		<div class="row">
			<div class="col-sm-2 col-sm-push-2">
				<div class="beta-counter">
					<p class="beta-counter-icon"><i class="fa fa-user"></i></p>
					<p class="beta-counter-value timer numbers" data-to="19855" data-speed="2000">{{count($customer)}} +</p>
					<p class="beta-counter-title">Lượng khách</p>
				</div>
			</div>

			<div class="col-sm-2 col-sm-push-2">
				<div class="beta-counter">
					<p class="beta-counter-icon"><i class="fa fa-picture-o"></i></p>
					<p class="beta-counter-value timer numbers" data-to="3568" data-speed="2000">{{$bill}} +</p>
					<p class="beta-counter-title">Số đơn hàng</p>
				</div>
			</div>

			<div class="col-sm-2 col-sm-push-2">
				<div class="beta-counter">
					<p class="beta-counter-icon"><i class="fa fa-clock-o"></i></p>
					<p class="beta-counter-value timer numbers" data-to="258934" data-speed="2000">{{count($product)}} +</p>
					<p class="beta-counter-title">Sản phẩm của cửa hàng</p>
				</div>
			</div>

			<div class="col-sm-2 col-sm-push-2">
				<div class="beta-counter">
					<p class="beta-counter-icon"><i class="fa fa-pencil"></i></p>
					<p class="beta-counter-value timer numbers" data-to="150" data-speed="2000">{{count($news)}} +</p>
					<p class="beta-counter-title">Bài viết</p>
				</div>
			</div>
		</div> <!-- .beta-counter block end -->

		<div class="space50">&nbsp;</div>
		<hr />
		<div class="space50">&nbsp;</div>
	</div> <!-- #content -->
</div> <!-- .container -->

@endsection

@section('wow')
<script>
            /* <![CDATA[ */
            jQuery(document).ready(function($) {
                'use strict';

                             
try {		
		if ($(".animated")[0]) {
            $('.animated').css('opacity', '0');
			}
			$('.triggerAnimation').waypoint(function() {
            var animation = $(this).attr('data-animate');
            $(this).css('opacity', '');
            $(this).addClass("animated " + animation);

			},
                {
                    offset: '80%',
                    triggerOnce: true
                }
			);
	} catch(err) {

		}
		
var wow = new WOW(
  {
    boxClass:     'wow',      // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset:       150,          // distance to the element when triggering the animation (default is 0)
    mobile:       false        // trigger animations on mobile devices (true is default)
  }
);
wow.init();	 
// NUMBERS COUNTER START
                $('.numbers').data('countToOptions', {
                    formatter: function(value, options) {
                        return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
                    }
                });

                // start timer
                $('.timer').each(count);

                function count(options) {
                    var $this = $(this);
                    options = $.extend({}, options || {}, $this.data('countToOptions') || {});
                    $this.countTo(options);
                } // NUMBERS COUNTER END 

           
		
@endsection
@section('script')
<script type="text/javascript">
    $(function() {
        // this will get the full URL at the address bar
        var url = window.location.href;

        // passes on every "a" tag
        $(".main-menu a").each(function() {
            // checks if its the same on the address bar
            if (url == (this.href)) {
                $(this).closest("li").addClass("active");
				$(this).parents('li').addClass('parent-active');
            }
        });
    });   
 // color box

//color
      jQuery('#style-selector').animate({
      left: '-213px'
    });

    jQuery('#style-selector a.close').click(function(e){
      e.preventDefault();
      var div = jQuery('#style-selector');
      if (div.css('left') === '-213px') {
        jQuery('#style-selector').animate({
          left: '0'
        });
        jQuery(this).removeClass('icon-angle-left');
        jQuery(this).addClass('icon-angle-right');
      } else {
        jQuery('#style-selector').animate({
          left: '-213px'
        });
        jQuery(this).removeClass('icon-angle-right');
        jQuery(this).addClass('icon-angle-left');
      }
    });
				

            });

            /* ]]> */
        </script>

</script>
@endsection
	