@extends('layout.index')
@section('content')

<div class="inner-header">
	<div class="container">
		<div class="pull-left">
			<h6 class="inner-title">Tất cả sản phẩm</h6>
		</div>
		<div class="pull-right">
			<div class="beta-breadcrumb font-large">
				<a href="{{route('index')}}">Home</a> / <span>Tất cả sản phẩm/span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<style>
	p.filter{
		font-size: 15px;
		font-weight: bold;
		margin:7px;
	}
	
</style>
<div class="container">
	<div id="content" class="space-top-none">
		<div class="main-content">
			<div class="space60">&nbsp;</div>
			<div class="row">
				<div class="col-sm-3 menu" style="">
					<h6>Sắp xếp giá</h6>
					<p class="filter"><a href="{{route('upPro')}}">-- Tăng dần</a></p>
					<p class="filter"><a href="{{route('downPro')}}">-- Giảm dần</a></p>
					<div class="space30">&nbsp;</div><hr>
					<h6>Khoảng giá</h6>
					<p class="filter"><a href="{{route('pricePro0')}}">-- 0 - 100.000 VNĐ</a></p>
					<p class="filter"><a href="{{route('pricePro1')}}">-- 100.000 - 200.000 VNĐ</a></p>
					<p class="filter"><a href="{{route('pricePro2')}}">-- 200.000 - 300.000 VNĐ</a></p>
					<p class="filter"><a href="{{route('pricePro3')}}">-- 300.000 - ... VNĐ</a></p>
					<div class="space30">&nbsp;</div><hr>
					<h6>Size</h6>
					<p class="filter"><a href="{{route('sizeM')}}">-- Size M</a></p>
					<p class="filter"><a href="{{route('sizeS')}}">-- Size S</a></p>
					<p class="filter"><a href="{{route('sizeL')}}">-- Size L</a></p>
					<div class="space30">&nbsp;</div><hr>
				</div>
				<div class="col-sm-9 ">
					<div class="beta-products-list">
						<div class="row" id="dataProduct">
							@foreach($data as $sp)
							<div class="col-sm-4">
								<div class="single-item">
									<div class="single-item-header">
										<a href=""><img src="frontend/product/{{$sp->image}}" alt=""></a>
									</div>
									<div class="single-item-body">
										@if($sp->promotion_pice != 0)
										<div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
										@endif
										<p class="single-item-title">{{$sp->name}}<span>@if($sp->soluong == $sp->daban || $sp->hansd < Carbon\Carbon::now()) <span style="padding: 3px;border-radius: 2px" class="btn-danger">Hết hàng</span> @endif</span></p>
										<p class="single-item-price">
											@if($sp->promotion_pice == 0)
											<span class="flash-sale">{{number_format($sp->unit_price)}} VNĐ</span>
											@else
											<span class="flash-del">{{number_format($sp->unit_price)}} VNĐ</span>
											<span class="flash-sale">{{number_format($sp->promotion_pice)}} VNĐ</span>
											@endif
										</p>
									</div>
									<div class="single-item-caption">
										@if($sp->soluong > $sp->daban && $sp->hansd > Carbon\Carbon::now())
											<a class="add-to-cart pull-left" href="{{route('themgiohang',$sp->id)}}"><i class="fa fa-shopping-cart"></i></a>
										@endif
										<a class="beta-btn primary" href="{{route('chitietsp',$sp->id)}}">Chi tiết <i class="fa fa-chevron-right"></i></a>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
						{{-- <div class="row">{{$sp_theoloai->links()}}</div> --}}
					</div> <!-- .beta-products-list -->

					<div class="space30">&nbsp;</div>
				</div>
			</div> <!-- end section with sidebar and main content -->
		</div> <!-- .main-content -->
	</div> <!-- #content -->
</div> <!-- .container -->
@endsection
{{-- @section('script')
	<script>
		function SubmitForm(formId) {
	    var oForm = document.getElementById(formId);
	    if (oForm) {
	        oForm.submit(); 
	    }
	    else {
	        alert("DEBUG - could not find element " + formId);
	    }
	}
	</script>
	
@endsection --}}

{{-- @push('scripts')
	<script>
		function AddCart(id){
			// console.log(id);
			$.ajax({
				url: '',
				type: 'GET',
			}).done(function(responese){
				console.log(responese);
			});
		}
		// $("#findBtn").click(function(){
	 //        var cat = $("#catID").val();
	 //        var price = $('#price').val();
	        
	 //        $.ajax({
	 //          	type: 'get',
	 //          	dataType: 'json',
	 //          	url: '{{route('getAllCateProduct')}}',
	 //         	 // data: 'cat_id=' + cat + '&price=' + price,
	 //          	data:{
		//             cat:cat,
		//             price:price
		//         },
		//         success:function(response){
		//             console.log(response);
		//             var html = '';
		//             response.product.forEach(value =>{
		//               	html = html + '<div class="col-sm-3">'
		// 							+		'<div class="single-item">'
		// 							+			'<div class="single-item-header">'
		// 							+				'<a href=""><img class=" flash" src="frontend/product/'+value.image+'" alt=""></a>'
		// 							+			'</div>'
										
		// 							+		'</div>'
		// 							+	'</div>';
		//             })
		//             console.log(html);
		//             $("#dataProduct").html(html);
		//         }
	 //     	})
	 //    })
	</script>

@endpush --}}
{{-- @endsection --}}

