@extends('layout.index')
@section('content')
<div class="inner-header">
	<div class="container">
		<div class="pull-left">
			<h2 class="inner-title">Sản Phẩm {{$sanpham->name}}</h2>
		</div>
		<div class="pull-right">
			<div class="beta-breadcrumb font-large">
				<a href="{{route('index')}}">Trang chủ</a> / <span>Thông tin chi tiết sản phẩm</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<div class="container">
	<div id="content">
		<div class="row">
			<div class="col-sm-9">
				<div class="row">
					<div class="col-sm-4">
						<img src="frontend/product/{{$sanpham->image}}" alt="">
					</div>
					<div class="col-sm-8">
						<div class="single-item-body">
							@if($sanpham->promotion_pice != 0)
								<div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
							@endif
							<p class="single-item-title"><h3 style="display: inline;">{{$sanpham->name}} </h3> <span>@if($sanpham->soluong == $sanpham->daban || $sanpham->hansd < Carbon\Carbon::now()) <span style="padding: 3px;border-radius: 2px" class="btn-danger">Hết hàng</span> @endif</span></p>
							<p></p>
							<p class="single-item-price">
								<p class="single-item-price">
									@if($sanpham->promotion_pice == 0)
									<span class="flash-sale">{{number_format($sanpham->unit_price)}} VNĐ</span>
									@else
									<span class="flash-del">{{number_format($sanpham->unit_price)}} VNĐ</span>
									<span class="flash-sale">{{number_format($sanpham->promotion_pice)}} VNĐ</span>
									@endif
								</p>
							</p>
						</div>

						<div class="clearfix"></div>
						<div class="space20">&nbsp;</div>

						<div class="single-item-desc">
							<p>{!!$sanpham->desc!!}</p>
						</div>
						<div class="space20">&nbsp;</div>
						<div class="row">
							@if($sanpham->soluong > $sanpham->daban && $sanpham->hansd > Carbon\Carbon::now())
							<div class="col-sm-4">
								<p>Số lương: 1</p><br>	
							</div>
							<div class="single-item-options col-sm-3">
								<a class="add-to-cart" href="{{route('themgiohang',$sanpham->id)}}"><i class="fa fa-shopping-cart"></i></a>
								
							</div>
							@endif
						</div>
						<div class="clearfix"></div>
					</div>
				</div>

				<div class="space40">&nbsp;</div>
				<div class="woocommerce-tabs">
					<ul class="tabs">
						<li><a href="#tab-description">Mô tả</a></li>
						{{-- <li><a href="#tab-reviews">Reviews (0)</a></li> --}}
					</ul>

					<div class="panel" id="tab-description">
						<p>{!!$sanpham->desc!!}</p>
					</div>
					<div class="panel" id="tab-reviews">
						{{-- <p>No Reviews</p> --}}
					</div>
				</div>
				<div class="space50">&nbsp;</div>
				<div class="beta-products-list">
					<h4>Sản Phẩm Tương Tự</h4>
					<div class="row">
						@foreach($sp_giong as $sp_g)
						<div class="col-sm-4">
							<div class="single-item">
								<div class="single-item-header">
									<a href="{{route('chitietsp',$sp_g->id)}}"><img src="frontend/product/{{$sp_g->image}}" alt=""></a>
								</div>
								<div class="single-item-body">
									@if($sp_g->promotion_pice != 0)
									<div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
									@endif
									<p class="single-item-title">{{$sp_g->name}}<span>@if($sp_g->soluong == $sp_g->daban || $sp_g->hansd < Carbon\Carbon::now()) <span style="padding: 3px;border-radius: 2px" class="btn-danger">Hết hàng</span> @endif</span></p>
									<p class="single-item-price">
										@if($sp_g->promotion_pice == 0)
										<span class="flash-sale">{{number_format($sp_g->unit_price)}} VNĐ</span>
										@else
										<span class="flash-del">{{number_format($sp_g->unit_price)}} VNĐ</span>
										<span class="flash-sale">{{number_format($sp_g->promotion_pice)}} VNĐ</span>
										@endif
									</p>
								</div>
								<div class="single-item-caption">
									@if($sp_g->soluong > $sp_g->daban && $sp_g->hansd > Carbon\Carbon::now())
									<a class="add-to-cart pull-left" href="{{route('themgiohang',$sp_g->id)}}"><i class="fa fa-shopping-cart"></i></a>
									@endif
									<a class="beta-btn primary" href="{{route('chitietsp',$sp_g->id)}}"> Chi tiết <i class="fa fa-chevron-right"></i></a>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						@endforeach
					</div>
					<div class="row">
						{{$sp_giong->links()}}
					</div>
				</div> <!-- .beta-products-list -->
			</div>
			<div class="col-sm-3 aside">
				<div class="widget">
					<h3 class="widget-title">Giảm giá tốt nhất </h3>
					<div class="widget-body">
						<div class="beta-sales beta-lists">
							@foreach($best as $best)
							<div class="media beta-sales-item">
								<a class="pull-left" href="{{route('chitietsp',$best->id)}}"><img src="frontend/product/{{$best->image}}" alt=""></a>
								<div class="media-body">
									{{$best->name}}<br>
									@if($best->promotion_pice == 0)
									<span class="flash-sale">{{number_format($best->unit_price)}} VNĐ</span>
									@else
									<span class="flash-del">{{number_format($best->unit_price)}} VNĐ</span><br>
									<span class="flash-sale">{{number_format($best->promotion_pice)}} VNĐ</span>
									@endif
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div> <!-- best sellers widget -->
				<div class="widget">
					<h3 class="widget-title">Sản phẩm mới</h3>
					<div class="widget-body">
						<div class="beta-sales beta-lists">
							@foreach($n_pro as $new)
							<div class="media beta-sales-item">
								<a class="pull-left" href="{{route('chitietsp',$new->id)}}"><img src="frontend/product/{{$new->image}}" alt=""></a>
								<div class="media-body">
									{{$new->name}}<br>
									@if($new->promotion_pice == 0)
										<span class="flash-sale">{{number_format($new->unit_price)}} VNĐ</span>
									@else
										<span class="flash-del">{{number_format($new->unit_price)}} VNĐ</span><br>
										<span class="flash-sale">{{number_format($new->promotion_pice)}} VNĐ</span>
									@endif
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div> <!-- best sellers widget -->
			</div>
		</div>
	</div> <!-- #content -->
</div> <!-- .container -->
@endsection
@section('script')
@parent
<script>
	function updateCart(rowId,qty)
	{

       $.get("/update/"+rowId+"/"+qty,
       	function(data)
       	{
       		
           if(data == "success")
           {
            location.reload();
           }
           else
           {
           	alert("cập nhật thất bại");
           }
       	}
       	);
	}
</script>
@endsection