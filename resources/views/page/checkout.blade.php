
@extends('layout.index')

@section('content')

<div class="inner-header">
	<div class="container">
		<div class="pull-left">
			<h6 class="inner-title">Đặt hàng</h6>
		</div>
		<div class="pull-right">
			<div class="beta-breadcrumb">
				<a href="{{route('index')}}">Trang chủ</a> / <span>Đặt hàng</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<div class="container">
	<div id="content">
		@if(Session('thongbao'))
		<div class="alert alert-success">
			{{Session('thongbao')}}
		</div>
		@endif
		<form action="{{route('dat-hang')}}" method="post" class="beta-form-checkout">
			@csrf
			<div class="row">
				<div class="col-sm-6">
					<h5>Thông tin đặt hàng</h5>
					<div class="space20">&nbsp;</div>
					<div style="">
						<p><b>Vui lòng chọn địa chỉ giao hàng tại đây:</b></p>
						<div class="space20">&nbsp;</div>
						<form action="route('add.deli')" method="POST" enctype="multipart/form-data">
                             <input type="hidden" name="_token" value="{{csrf_token()}}">
                             <div class="row">
                             	<div class="form-group col-sm-3">
	                                <label>Chọn thành phố</label>
	                                <select name="city" id="city" class="form-control input-sm m-bot15 choose city">
	                                    <option value="">Chọn tỉnh thành phố</option>
	                                    @foreach($city as $ct)
	                                        <option value="{{$ct->matp}}">{{$ct->name_city}}</option>
	                                    @endforeach 
	                                </select>
	                            </div>
	                             <div class="form-group col-sm-3">
	                                <label>Chọn quận huyện</label>
	                                <select name="province" id="province" class="form-control input-sm m-bot15 choose province">
	                                    <option value="">Chọn quận huyện</option>
	                                    @foreach($province as $pro)
	                                        <option value="{{$pro->maqh}}">{{$pro->name_province}}</option>
	                                    @endforeach 
	                                </select>
	                            </div>
	                            <div class="form-group col-sm-3">
	                                <label>Chọn xã phường</label>
	                                <select name="wards" id="wards" class="form-control input-sm m-bot15 wards">
	                                    <option value="">Chọn xã phường</option>
	                                    @foreach($wards as $wards)
	                                        <option value="{{$wards->xaid}}">{{$wards->name_ward}}</option>
	                                    @endforeach 
	                                </select>
	                            </div>
	                            <div class="form-group col-sm-3">
	                            	<label>Phí vận chuyển</label>
	                               <input type="button" value="Tính phí VC " required name="calculate_order" class="btn btn-primary delivery calculate_delivery">
	                            </div>
                            </div>
                        </form>
                        <hr>
					</div>	
					
					<div class="form-block">
						<label for="name">Họ tên*</label>
						<input type="text" name="name" placeholder="Họ tên" required>
					</div>
					<div class="form-block">
						<label>Giới tính </label>
						<input id="gioitinh" type="radio" class="input-radio" name="gioitinh" value="nam" checked="checked" style="width: 10%"><span style="margin-right: 10%">Nam</span>
						<input id="gioitinh" type="radio" class="input-radio" name="gioitinh" value="nữ" style="width: 10%"><span>Nữ</span>

					</div>

					<div class="form-block">
						<label for="email">Email*</label>
						<input type="email" name="email" required placeholder="expample@gmail.com">
					</div>

					<div class="form-block">
						<label for="address">Địa chỉ*</label>
						<input type="text" name="address" placeholder="Street Address" required>
					</div>


					<div class="form-block">
						<label for="phone">Điện thoại*</label>
						<input type="text" name="phone" required>
					</div>

					<div class="form-block">
						<label for="notes">Ghi chú*</label>
						<textarea name="note" required></textarea>
					</div>
					
                     
				</div>
				<div class="col-sm-6">
					<div class="your-order">
						<div class="your-order-head"><h5>Đơn hàng của bạn</h5></div>
						<div class="your-order-body" style="padding: 0px 10px">
							<div class="your-order-item">
								<div>
									<!--  one item	 -->
									@if(Session::has('cart'))
										@foreach($product_cart as $cart)
										<div class="media">
											<img width="25%" src="frontend/product/{{$cart['item']['image']}}" alt="" class="pull-left">
											<p class="single-item-title">{{$cart['item']['name']}}</p>
											<p class="single-item-price">Đơn giá: 
												{{number_format($cart['price'])}} đồng
											</p>
											<p class="single-item-title">Số lượng: {{$cart['qty']}}</p>
											{{-- <p class="single-item-title">Mã giảm giá: {{Session::has('coupon')}}</p> --}}
										</div>
										@endforeach
									@endif
									<!-- end one item -->
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="your-order-item">
								<div class="pull-left"><p class="your-order-f18">Tiền sản phẩm</p></div>
								<div class="pull-right"><h5 class="color-black">
								
										@if(Session::has('cart'))
											{{number_format(Session('cart')->totalPrice)}}
										@endif VNĐ</h5></div>
								<div class="clearfix"></div>
							</div>
							<div class="your-order-item">
									@if(Session::get('fee'))
									<div class="pull-left"><p class="your-order-f18">Phí vận chuyển</p></div>
									<div class="pull-right"><h5 class="color-black"> {{number_format(Session::get('fee'))}} VNĐ</h5></div>
									 		<?php $ship_fee = Session::get('fee'); ?>
									@endif
									<div class="clearfix"></div>
							</div>
							<div class="your-order-item">
									@if(Session::get('coupon'))
									<div class="pull-left"><p class="your-order-f18" style="display: inline-table;">Mã giảm giá({{Session('coupon')['name']}})</p>
										<form action="{{route('delVoucher')}}" method="post" style="display: inline;">
											@csrf
											<button type="submit" class="btn btn-danger">Hủy mã</button>
										</form>
									</div>
									<div class="pull-right"><h5 class="color-black"> -{{number_format(Session::get('coupon')['discount_amount'])}} VNĐ</h5></div>
									 		<?php $coupon = Session::get('coupon'); ?>
									@endif
									<div class="clearfix"></div>
							</div>
							<div class="your-order-item">
								<div class="pull-left"><p class="your-order-f18">Tổng cộng</p></div>
								<div class="pull-right"><h5 class="color-black">
										@if(Session::has('cart') && Session::get('fee') && Session::get('coupon'))
											{{number_format( Session('cart')->totalPrice + Session::get('fee') -  Session::get('coupon')['discount_amount']) }}
										@elseif(Session::has('cart') && Session::get('fee') && !Session::get('coupon'))
											{{number_format( Session('cart')->totalPrice + Session::get('fee')) }}
										@elseif(Session::has('cart') && Session::get('coupon') && !Session::get('fee'))

										{{number_format( Session('cart')->totalPrice -  Session::get('coupon')['discount_amount']) }}

										@endif VNĐ</h5></div>
								
								<div class="clearfix"></div>
							</div>
							<div class="your-order-item">
								@if(!Session::get('coupon'))
								<div class="pull-left"><p class="your-order-f18">Bạn có mã giảm giá?</p></div>
								<div class="pull-left" style="display: inline-flex;margin-left: 50px">
									<form action="{{route('postVoucher')}}" method="post" class="row">
										@csrf
										<input type="text" name="coupon_code" id="coupon_code" style="display: inline;">
										<button style="display: inline;position: absolute;margin-left: 10px" type="submit" class=" btn btn-success">Apply</button>
									</form>
								</div>
								<div class="clearfix"></div>
								@endif
						</div>
						<div class="your-order-head"><h5>Hình thức thanh toán</h5></div>
						<div class="your-order-body">
							<ul class="payment_methods methods">
								<li class="payment_method_bacs">
									<input id="payment_method_bacs" type="radio" class="input-radio" name="payment" value="COD" checked="checked" data-order_button_text="">
									<label for="payment_method_bacs">Thanh toán khi nhận hàng </label>
									<div class="payment_box payment_method_bacs" style="display: block;">
										Cửa hàng sẽ gửi hàng đến địa chỉ của bạn, bạn xem hàng rồi thanh toán tiền cho nhân viên giao hàng
									</div>						
								</li>
								<li class="payment_method_cheque">
									{{-- <form action="{{ url('charge') }}" method="post"> --}}
									<input id="payment_method_cheque" type="radio" class="input-radio" name="payment" value="ATM" data-order_button_text="">
									 {{-- <input type="text" name="amount" /> --}}
										<label for="payment_method_cheque">Chuyển khoản </label>
										<div class="payment_box payment_method_cheque" style="">
											Chuyển tiền đến tài khoản sau:
											<br>- Số tài khoản: 123 456 789
											<br>- Chủ TK: Hoàng Tuấn Thành 
											<br>- Ngân hàng BIDV, Chi nhánh Hà Nội
										</div>						
								</li>

							</ul>
						</div>

						<div class="text-center"><button class="beta-btn primary" type="submit">Đặt hàng <i class="fa fa-chevron-right"></i></button></div>
					</div> <!-- .your-order -->
				</div>
			</div>
		</form>
	</div> <!-- #content -->
</div> <!-- .container -->

@section('script')
<script type="text/javascript">
	$(function() {
        // this will get the full URL at the address bar
        var url = window.location.href;

        // passes on every "a" tag
        $(".main-menu a").each(function() {
            // checks if its the same on the address bar
            if (url == (this.href)) {
            	$(this).closest("li").addClass("active");
            	$(this).parents('li').addClass('parent-active');
            }
        });
    });   

</script>
<script>
	jQuery(document).ready(function($) {
		'use strict';

		// color box

		//color
		jQuery('#style-selector').animate({
			left: '-213px'
		});

		jQuery('#style-selector a.close').click(function(e){
			e.preventDefault();
			var div = jQuery('#style-selector');
			if (div.css('left') === '-213px') {
				jQuery('#style-selector').animate({
					left: '0'
				});
				jQuery(this).removeClass('icon-angle-left');
				jQuery(this).addClass('icon-angle-right');
			} else {
				jQuery('#style-selector').animate({
					left: '-213px'
				});
				jQuery(this).removeClass('icon-angle-right');
				jQuery(this).addClass('icon-angle-left');
			}
		});
	});
</script>
@endsection
@endsection
