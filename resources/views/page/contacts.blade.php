@extends('layout.index')
@section('title','Home | Lien he')
@section('content')

	<div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title">Liên hệ</h6>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb font-large">
					<a href="{{route('index')}}">Trang Chủ</a> / <span>Liên hệ</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="beta-map">
		
		<div class="abs-fullwidth beta-map wow flipInX"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7444.114433002702!2d105.71423637209807!3d21.110284952658237!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31345587ec40aa27%3A0x182cb3507c1c67a0!2zQ2jhu6MgR2nDoHksIExpw6puIFRydW5nLCDEkGFuIFBoxrDhu6NuZywgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1593222954037!5m2!1svi!2s"  frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></div>
	</div>
	<div class="container">
		<div id="content" class="space-top-none">
			
			<div class="space50">&nbsp;</div>
			<div class="row">
				<div class="col-sm-8">
					<h2>LIÊN HỆ</h2>
					<div class="space20">&nbsp;</div>
					<p>@if(session('thongbao1')) <div class="alert alert-success">{{session('thongbao1')}}</div>@endif</p>
					<div class="space20">&nbsp;</div>
					<form action="{{route('post.lienhe')}}" class=" mt-4" method="POST" enctype="multipart/form-data">
                     @csrf
                    <div class="form-group pt-4">
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" id="exampleFormControlInput1" placeholder="Họ tên" > 
                       <p class="help is-danger" style="color: red">{{ $errors->first('name') }}</p>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}" id="exampleFormControlInput1" placeholder="email">
                        <p class="help is-danger" style="color: red">{{ $errors->first('email') }}</p>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="address" value="{{ old('address') }}" id="exampleFormControlInput1" placeholder="Địa chỉ">
                        <p class="help is-danger" style="color: red">{{ $errors->first('address') }}</p>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" value="{{ old('phone') }}" id="exampleFormControlInput1" name="phone" placeholder="Số điện thoại">
                        <p class="help is-danger" style="color: red">{{ $errors->first('phone') }}</p>
                    </div>
                    <div class="form-group">
                        <div>Nội dung</div>
                        <textarea class="form-control" value="" id="exampleFormControlTextarea1" name="content" rows="3">{{ old('content') }}</textarea>
                         <p class="help is-danger" style="color: red">{{ $errors->first('content') }}</p>
                    </div>
                    <button type="submit" class="btn btn-primary">Gửi</button>
					
                </form>
				</div>
				<div class="col-sm-4">
					<h2>Thông tin liên hệ</h2>
					<div class="space20">&nbsp;</div>

					<h6 class="contact-title">Địa chỉ</h6>
					<p>
						Liên Trung - Đan Phượng <br>
						Hà Nội<br>
						
					</p>
					<div class="space20">&nbsp;</div>
					<h6 class="contact-title">Thắc mắc công việc</h6>
					<p>Mọi thắc mắc của các bạn xin hãy gửi về hòm mail của của hàng chúng tôi
						 <br>
						Cảm ơn bạn đã quan tâm <br>
						<a href="mailto:hoangtuanthanh19061997@gmail.com">hoangtuanthanh19061997@gmail.com</a>
					</p>
					<div class="space30">&nbsp;</div>
				
					
				</div>
			</div>
		</div> <!-- #content -->
	</div> <!-- .container -->

@endsection