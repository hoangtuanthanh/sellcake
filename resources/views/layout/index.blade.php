<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>@yield('title') </title>
	<base href="{{asset('')}}">
	<link href='http://fonts.googleapis.com/css?family=Dosis:300,400' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="source/assets/dest/css/font-awesome.min.css">
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="source/assets/dest/css/bootstrap.min.css">

	{{-- <link rel="stylesheet" href="source/assets/dest/css/font-awesome.css"> --}}
	<link rel="stylesheet" href="source/assets/dest/vendors/colorbox/example3/colorbox.css">
	<link rel="stylesheet" href="source/assets/dest/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="source/assets/dest/rs-plugin/css/responsive.css">
	<link rel="stylesheet" title="style" href="source/assets/dest/css/style.css">
	<link rel="stylesheet" href="source/assets/dest/css/animate.css">
	<link rel="stylesheet" title="style" href="source/assets/dest/css/huong-style.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.css">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.js"></script>
    <script src="{{asset('admin_asset/dist/js/notify.js')}}"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <script src="https://code.jquery.com/jquery-3.5.0.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
	@yield('css')
</head>
<body>
	 @if (session('success'))
        <script>
            notify("<div style='font-size:15px'><i class='fa fa-check'></i> {{ session('success') }} </div>",'success');
        </script>
    @endif

    @if (session('danger'))
        <script>
            notify("<div style='font-size:15px'><i style='line-height: 20px;' class='fa ffa fa-exclamation-circle'><i/> {{ session('danger') }} </div>",'error');
        </script>
    @endif

	@include("layout.header")

	<div class="rev-slider">
		@yield('content')
	</div>
	@include('layout.footer')
	<a id="back-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
	<!-- include js files -->
  <script src="source/assets/dest/js/jquery.js"></script>
	<script src="source/assets/dest/js/jquery.min.js"></script>

	<script src="source/assets/dest/vendors/jqueryui/jquery-ui-1.10.4.custom.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>

  <script src="source/assets/dest/js/bootstrap.min.js"></script>

	<script src="source/assets/dest/vendors/bxslider/jquery.bxslider.min.js"></script>
	<script src="source/assets/dest/vendors/colorbox/jquery.colorbox-min.js"></script>
	<script src="source/assets/dest/vendors/animo/Animo.js"></script>
	<script src="source/assets/dest/vendors/dug/dug.js"></script>
	<script src="source/assets/dest/js/scripts.min.js"></script>
	<script src="source/assets/dest/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="source/assets/dest/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script src="source/assets/dest/js/waypoints.min.js"></script>
	<script src="source/assets/dest/js/wow.min.js"></script>
	
	{{-- picker --}}
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<!--customjs-->
	<script src="source/assets/dest/js/custom2.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="source/assets/dest/js/dataTables.bootstrap.min.js"></script>

  <script>
 		$(document).ready(function(){
 			$('.choose').on('change',function(){
              var action = $(this).attr('id');
              var ma_id = $(this).val();
              var _token = $('input[name="_token"]').val();
              var $result = '';

              if (action == 'city') {
                  result = 'province';
              }else{
                  result = 'wards';
              }
              $.ajax({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  url: "{{ route('select.delivery.home') }}",
                  method: "POST",
                  data:{action:action,ma_id:ma_id,_token:_token},
                  success:function(data){
                      $('#'+result).html(data);
                  }
              });
          })
   		});
   </script>
  <script>
  	$(document).ready(function(){
  		$('.calculate_delivery').click(function(){
  			var matp = $('.city').val();
  			var maqh = $('.province').val();
  			var xaid = $('.wards').val();
  			var _token = $('input[name="_token"]').val();
  			if(matp == '' && maqh =='' && xaid ==''){
  				alert('Hãy nhập đủ thông tin địa chỉ');
  			}else{
  				 $.ajax({
            url: "{{ route('calculate.fee') }}",
            method: "POST",
            data:{matp:matp,maqh:maqh,xaid:xaid,_token:_token},
            success:function(){
               location.reload();
            }
        });
  			}
  		});
    });
  </script>
  <script>
    $(document).ready(function(){
      
        // }).done(function (data) {
        //     $('#dataProduct').html('');
        //         $.each(data.data, function (data) {
        //           console.log(data);
        //         $("#dataProduct").html(data);
        //             // $('#dataProduct').append(  `
        //             //   <div class="col-sm-3">
        //             //     <div class="single-item">
        //             //       <div class="single-item-header">
        //             //         <a href="${route('chitietsp',value.id)}"><img class=" flash" src="frontend/product/${value.image}" alt=""></a>
        //             //       </div>
        //             //       <div class="single-item-body">
        //             //         if(${value.promotion_pice != 0}){
        //             //         <div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
        //             //         }
        //             //         <p class="single-item-price">
        //             //           if(${value.promotion_pice == 0}){
        //             //           <span class="flash-sale">${number_format(value.unit_price)} VNĐ</span>
        //             //           }else{
        //             //           <span class="flash-del">${number_format($data.unit_price)} VNĐ</span>
        //             //           <span class="flash-sale">${number_format(value.promotion_pice)} VNĐ</span>
        //             //          }
        //             //         </p>
        //             //       </div>
        //             //       <div class="single-item-caption  bounceInLeft">
        //             //         if(${value.soluong > value.daban }){
        //             //         <a class="add-to-cart pull-left" href="${route('themgiohang',value.id)}"><i class="fa fa-shopping-cart"></i></a>
        //             //        }
        //             //         <a class="beta-btn primary "  href="${route('chitietsp',value.id)}">Chi tiết <i class="fa fa-chevron-right"></i></a>
        //             //         <div class="clearfix"></div>
        //             //       </div>
        //             //     </div>
        //             //   </div>
        //             //   `)
        //             });
                });
      
 
  </script>
  @stack('scripts')
</body>
</html>
