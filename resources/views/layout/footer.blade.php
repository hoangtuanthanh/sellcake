
<div id="footer" class="color-div">
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="widget">
					<h4 class="widget-title">Theo dõi chúng tôi</h4>
					<div id="insta">
							<a style="" href="https://www.facebook.com/groups/700256606978414/?ref=bookmarks"><i style="font-size: 30px" class="fa fa-facebook-square"></i></a>
							<a href="https://www.facebook.com/groups/700256606978414/?ref=bookmarks"><i style="font-size: 30px" class="fa fa-linkedin"></i></a>
							<a href="https://www.facebook.com/groups/700256606978414/?ref=bookmarks"><i style="font-size: 30px" class="fa fa-envelope"></i></a>
							<a href="https://www.facebook.com/groups/700256606978414/?ref=bookmarks"><i style="font-size: 30px" class="fa fa-instagram"></i></a>
						
							
					</div>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="widget">
					<h4 class="widget-title">Thông tin về bánh </h4>
					@foreach($tintuc as $tintuc)
					<div>
						<ul>
							<li><a href="{{$tintuc->link}} "><i class="fa fa-chevron-right"></i> {{$tintuc->title}}</a></li>
						</ul>
					</div>
					@endforeach
				</div>
			</div>
			<div class="col-sm-4">
				<div class="col-sm-10">
					<div class="widget">
						<h4 class="widget-title">Liên hệ với chúng tôi</h4>
						<div>
							@foreach($Info as $Info)
							<div class="contact-info">
								<i class="fa fa-map-marker"></i>
								<p> Địa chỉ:{{$Info->address}}</p>
								<p>Phone: {{$Info->phone}}</p>
								<p>hotline: {{$Info->hotline}}</p>
								<p>Email: {{$Info->email}}</p>
								<p>Website: {{$Info->website}}</p>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="widget">
					@if(session('tb1')) <div class="alert alert-success">{{session('tb1')}}</div>
					@endif
					<h4 class="widget-title">Đăng kí nhận bản tin mới nhất</h4>
					<form action="{{route('subcriber')}}" method="post">
						@csrf
						<input type="email" name="email" placeholder="Nhập email...">
						<button class="pull-right" type="submit">Đăng kí <i class="fa fa-chevron-right"></i></button>
					</form>

				</div>
			</div>
		</div> <!-- .row -->
	</div> <!-- .container -->
</div> <!-- #footer --> 

<div class="copyright">
	<div class="container">
		<p class="pull-left">Privacy policy. (&copy;) 2020</p>
		<div class="clearfix"></div>
	</div> <!-- .container -->
</div> <!-- .copyright -->
