<div id="header">
	<div class="header-top">
		<div class="container">
			<div class="row">
				<div class="col-xs-9 col-sm-9">
				<a href="{{route('index')}}" id="logo" style="padding-top: 32px"><img src="source/assets/dest/images/logo-cake.png" width="200px"  alt=""></a>
				</div>
				<div class="col-xs-3 col-sm-3" style="border-bottom: 1px solid;text-align: center;">
					<ul class="top-details menu-beta l-inline" style="line-height: 80px">
					@if(Auth::check())
						@if(Auth::user()->role == 0)
							<li ><a href="{{route('index')}}">Chào bạn {{Auth::user()->name}}</a></li>
							<li style="float: right;"><a href="{{route('logout')}}">Đăng xuất</a></li>
						@elseif(Auth::user()->role == 1)
							<li ><a href="{{route('dash')}}">Chào bạn {{Auth::user()->name}}</a></li>
							<li style="float: right;"><a href="{{route('logout')}}">Đăng xuất</a></li>
						@elseif(Auth::user()->role == 2)
							<li ><a href="{{route('index.imployeed')}}">Chào bạn {{Auth::user()->name}}</a></li>
							<li style="float: right;"><a href="{{route('logout')}}">Đăng xuất</a></li>
						@else
							{{'Chưa có '}}
						@endif
					@else
					<li ><a href="{{route('register.get')}}">Đăng kí</a></li>
					<li style="float: right;"><a href="{{route('login-admin-post')}}">Đăng nhập</a></li>
					@endif
				</ul>
				</div>
			</div>
		{{-- 	<div class="pull-left">
				<a href="{{route('index')}}" id="logo"><img src="source/assets/dest/images/logo-cake.png" width="200px"  alt=""></a>
			</div>
			<div class="pull-right auto-width-right">
				<ul class="top-details menu-beta l-inline" style="">
					@if(Auth::check())
					<li><a href="">Chào bạn {{Auth::user()->name}}</a></li>
					<li><a href="{{route('logout')}}">Đăng xuất</a></li>
					@else
					<li><a href="{{route('register.get')}}">Đăng kí</a></li>
					<li><a href="{{route('login-admin-post')}}">Đăng nhập</a></li>
					@endif
				</ul>
			</div> --}}
			<div class="clearfix"></div>
		</div> <!-- .container -->
	</div> <!-- .header-top -->
	<div class="header-body">
		<div class="container beta-relative">
			
			<div class="pull-right beta-components space-left ov">
				{{-- <div class="space10">&nbsp;</div> --}}
				<div class="beta-comp">
					<form role="search" method="get" id="searchform" action="{{route('get.search')}}">
						<input type="text" value="" name="key" id="key" placeholder="Nhập từ khóa..." />
						<button class="fa fa-search" type="submit" id="searchsubmit"></button>
					</form>
				</div>

				<div class="beta-comp">
					@if(Session::has('cart'))
					<div class="cart">
						<div class="beta-select "><i class="fa fa-shopping-cart" >
							</i> Giỏ hàng 	(@if(Session::has('cart')){{Session('cart')->totalQty}} 
											@else 0
											@endif) <i class="fa fa-chevron-down"></i>
						</div>
						<div class="beta-dropdown cart-body">	
							@foreach($product_cart as $product)
							<div class="cart-item change-item-cart">
								{{-- <a class="cart-item-edit" href="#"><i class="fa fa-pencil"></i></a> --}}
								<a class="cart-item-delete fa fa-times"  href="{{route('delete-cart',$product['item']['id'])}}"></a>
								<div class="media">
									<a class="pull-left" href="#"><img src="frontend/product/{{$product['item']['image']}}" alt=""></a>
									<div class="media-body">
										<span class="cart-item-title">{{ $product['item']['name']}}</span>

										<span class="cart-item-amount">{{$product['qty']}}*<span>
											
										@if($product['item']['promotion_pice'] ==0){{number_format($product['item']['unit_price'])}} 
										@else {{number_format($product['item']['promotion_pice'])}}  VNĐ 
										@endif</span></span>
									</div>
								</div>
							</div>
							@endforeach
							<div class="cart-caption">
								<div class="cart-total text-right">Tổng tiền: <span class="cart-total-value">

										{{number_format(Session('cart')->totalPrice)}} VNĐ 		
								</div>
								<div class="clearfix"></div>

								<div class="center">
									<div class="space10">&nbsp;</div>
									<a href="{{route('dat-hang')}}" class="beta-btn primary text-center">Đặt hàng <i class="fa fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
					</div> <!-- .cart -->
					@endif
				</div>
			</div>
			<div class="clearfix"></div>
		</div> <!-- .container -->
		<div class="space20">&nbsp;</div>
	</div> <!-- .header-body -->
	<div class="header-bottom" style="background-color: #0277b8;">
		<div class="container">
			<a class="visible-xs beta-menu-toggle pull-right" href="#"><span class='beta-menu-toggle-text'>Menu</span> <i class="fa fa-bars"></i></a>
			<div class="visible-xs clearfix"></div>
			<nav class="main-menu">
				<ul class="l-inline ov">
					<li><a href="{{route('index')}}">Trang chủ</a></li>
					<li><a href="#">Loại Sản phẩm</a>
						<ul class="sub-menu">
							<li><a href="{{route('getAllProduct')}}">Tất cả sản phẩm</a></li>
							@foreach($loai_sp as $loai) {{-- đọc loại sản phẩm trong appserviceProvider --}}
							<li><a href="{{route('loaisp',$loai->id)}}">{{$loai->name}}</a></li>
							@endforeach
						</ul>
					</li>
					<li><a href="{{route('gioithieu')}}">Giới thiệu</a></li>
					<li><a href="{{route('lienhe')}}">Liên hệ</a></li>
				</ul>
				<div class="clearfix"></div>
			</nav>
		</div> <!-- .container -->
	</div> <!-- .header-bottom -->
	</div> <!-- #header -->