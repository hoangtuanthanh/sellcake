
  <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                       
                        <li>
                            <a href="imployeed/bill/list"><i class="fa fa-comet"></i>Đơn đặt hàng<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('get.list.bill.im')}}">Danh sách đơn hàng </a>
                                </li>
                                <li>
                                    <a href="{{route('get.success.bill.im')}}">Đơn hàng đã giao</a>
                                </li>
                                <li>
                                    <a href="{{route('get.refuse.bill.im')}}">Đơn hàng từ chối nhận</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
  </div>