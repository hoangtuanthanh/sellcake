<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Quản lý tiệm bánh gia đình ">
    <meta name="author" content="">
    <meta name="_csrf" content="${_csrf.token}" />
    <title>Quản lý Tiệm Bánh</title>
    <base href="{{asset('')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.js"></script>
    <script src="{{asset('admin_asset/dist/js/notify.js')}}"></script>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('admin_asset/bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{asset('admin_asset/bower_components/metisMenu/dist/metisMenu.min.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{asset('admin_asset/dist/css/sb-admin-2.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{asset('admin_asset/bower_components/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!-- DataTables CSS -->
    <link href="{{asset('admin_asset/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="{{asset('admin_asset/bower_components/datatables-responsive/css/dataTables.responsive.css')}}" rel="stylesheet">
    <link href="{{asset('admin_asset/dist/css/alertify.css')}}" rel="stylesheet">
    <link href="{{asset('admin_asset/dist/css/alertify.min.css')}}" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.0.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>

<body>
     @if (session('success'))
        <script>
            notify("<div style='font-size:15px'><i class='fa fa-check'></i> {{ session('success') }} </div>",'success');
        </script>
    @endif

    @if (session('danger'))
        <script>
            notify("<div style='font-size:15px'><i style='line-height: 20px;' class='fa ffa fa-exclamation-circle'><i/> {{ session('danger') }} </div>",'error');
        </script>
    @endif
    <div id="wrapper">
    	 
       @include('imployeed.layout.header')

       @yield('content')
       
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

    
    <script src="{{asset('admin_asset/bower_components/jquery/dist/jquery.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('admin_asset/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{asset('admin_asset/bower_components/metisMenu/dist/metisMenu.min.js')}}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{asset('admin_asset/dist/js/sb-admin-2.js')}}"></script>
     <script src="{{asset('admin_asset/dist/js/alertify.js')}}"></script>
      <script src="{{asset('admin_asset/dist/js/alertify.min.js')}}"></script>


    <!-- DataTables JavaScript -->
    <script src="{{asset('admin_asset/bower_components/DataTables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin_asset/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>


    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
            
            var date = new Date();      
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

            $('#start_date').val(moment(firstDay).format('YYYY-MM-DD'));
            $('#end_date').val(moment(lastDay).format('YYYY-MM-DD'));


        });
         function searchBills(){
            var start = $('#start_date').val();
            var end = $('#end_date').val();
            if (start != "" && end != "") {
                $.ajax({
                     headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "{{ route('bill.count.date.Im') }}",
                    data: {
                        start: start,
                        end: end,
                    },
                }).done(function (data) {
                    $('#tbody_bill').html('');
                        $.each(data.bills, function (key,value) {
                            $('#tbody_bill').append(`<tr>
                                <td>${ key + 1 }</td>
                                <td>00000${ value.id}</td>
                                <td>${ value.id_customer }</td>
                                <td>${ value.date_order }</td>
                                <td style="">${value.total}<sup>đ</sup></td>
                                <td style="">${value.fee_ship }<sup>đ</sup></td>
                                <td>${ value.payment }</td>
                                <td>${ value.note }</td>
                                <td>${ value.updated_at }</td>
                            </tr>`)
                            });
                     
                }).fail(function (Responsive){
                    console.log(Responsive);
                })
            }
        }
         function searchBillsRefuse(){
            var start = $('#start_date').val();
            var end = $('#end_date').val();
            if (start != "" && end != "") {
                $.ajax({
                     headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "{{ route('bill.count.date.Im.Refuse') }}",
                    data: {
                        start: start,
                        end: end,
                    },
                }).done(function (data) {
                    $('#tbody_bill').html('');
                        $.each(data.bills, function (key,value) {
                            $('#tbody_bill').append(`<tr>
                                <td>${ key + 1 }</td>
                                <td>00000${ value.id}</td>
                                <td>${ value.id_customer }</td>
                                <td>${ value.date_order }</td>
                                <td style="">${value.total}<sup>đ</sup></td>
                                <td style="">${value.fee_ship }<sup>đ</sup></td>
                                <td>${ value.payment }</td>
                                <td>${ value.note }</td>
                                <td>${ value.updated_at }</td>
                            </tr>`)
                            });
                     
                }).fail(function (Responsive){
                    console.log(Responsive);
                })
            }
        }
       
    </script>

    @yield('script')
    <script>
        CKEDITOR.replace( 'content' );
    </script>
    <script>
    CKEDITOR.replace( 'content', {
        filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });
</script>
</body>

</html>
