 @extends('imployeed.layout.index')
 @section('content')<!-- Page Content -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Bill
                            <small>Danh sách đơn hàng bị từ chối</small>
                        </h1>
                        <h4 style="float: right; color: red"><a href="{{route('get.list.bill.im')}}">Danh sách đơn hàng</a></h4>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                             {{session('thongbao')}}
                        </div>
                    @endif
                    <div class="row" style="margin-bottom:10px">
                        <div class="col-md-3 col-sm-5" style="margin-bottom:10px">
                            Từ ngày:<input type="date" id="start_date" class="form-control">
                        </div>
                        <div  class="col-md-3 col-sm-5" style="margin-bottom:10px">
                            Đến ngày:<input type="date" id="end_date" class="form-control">
                        </div>
                       <div class="col-md-3 col-sm-3" style="margin-top: 20px;">
                            <button type="button" id="fill" onclick="searchBillsRefuse()" class="btn btn-default">Tìm kiếm</button>
                        </div>                 
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                               
                                <th>Mã đơn hàng</th>
                                <th>Khách hàng</th>
                                <th>Ngày đặt hàng</th>
                                {{-- <th>Phí ship</th> --}}
                                <th>Thanh toán</th>
                                <th>Ghi chú</th>
                                <th>Ngày xử lý</th>
                                <th>Tiền bánh</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_bill">
                            @foreach($bill as $bill)
                            <tr class="odd gradeX" align="center">
                               <td>00000{{$bill->id}}</td>
                                <td>{{$bill->Customer['name']}}</td>
                                <td>{{ Carbon\Carbon::parse($bill->date_order)->format('d-m-Y')}}</td>
                                 {{-- <td>@if($bill->fee_ship != 0){{number_format($bill->fee_ship)}} @else {{'10,000'}}<sup>đ</sup> @endif</td>  --}}
                                <td>{{$bill->payment}}</td>
                                <td>{!!$bill->note!!}</td>
                                 <td>{{ Carbon\Carbon::parse($bill->updated_at)->format('d-m-Y')}}</td>
                                <td>{{number_format($bill->total)}}<sup>đ</sup></td> 
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- <div> Tổng số doanh thu thu được:{{}}</div> --}}
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection
@section('script')
{{-- <script type="text/javascript">

    $('#fill').click(function () {
        var start = $('#from_date').val();
        var end = $('#to_date').val();
        if (start != "" && end != "") {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "{{ route('bill.count.date') }}",
                data: {
                    start: start,
                    end: end,
                },
            }).done(function (data) {
                $('#tbody_bill').html('');
                    $.each(data.bills, function (key, value) {
                        $('#tbody_bill').append(  `<tr>
                            <td>${ key + 1 }</td>
                            <td style="color:#007a6e"> <b>${ value.Customer['name'] }</b> </td>
                            <td>${ value.date_order }</td>
                            <td>
                                <td style="color:#007a6e">${ number_format(value.total) }<sup>đ</sup></td>
                            </td>
                            <td>${ value.payment }</td>
                            <td>${ value.note }</td>
                            <td>${ moment(value.updated_at).format('DD-MM-YYYY') }</td>
                        </tr>`)
                        });
            }).fail(function (Responsive){
                console.log(Responsive);
            })
        }        
    })
</script> --}}
@endsection