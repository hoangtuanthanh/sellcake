     @extends('imployeed.layout.index')
     @section('content')<!-- Page Content -->
     <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"style=" margin: 56px 0 20px;">Bill
                        <small>Danh sách đơn hàng</small>
                    </h1>
                {{--                         <h4 style="float: right; color: red"><a href="{{route('list.bill.processed')}}">Danh sách đơn hàng đã xử lý</a></h4>
                --}}                 
                </div>
                <!-- /.col-lg-12 -->
                @if(session('thongbao'))
                <div class="alert alert-success">
                   {{session('thongbao')}}
                </div>
                @endif
               <select name="statusID" id="statusID" class="col-lg-4" style="margin-bottom: 10px;padding: 4px">
                    <option value="">--Chọn trạng thái cần tìm</option>
                    <option value="1">Chờ giao hàng</option>
                    <option value="2">Xác nhận giao hàng</option>
                    <option value="3">Đang giao hàng</option>
                </select>
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr align="center">
                            <th>Mã đơn hàng</th>
                            <th>Khách hàng</th>
                            <th>Ngày đặt hàng</th>
                            <th>Thanh toán</th>
                            <th>Ghi chú</th>
                            <th>Tổng tiền</th>
                            <th>Chi tiết</th>
                            <th>Trạng thái đơn</th>
                            <th>Chuyển trạng thái đơn hàng</th>
                        </tr>
                    </thead>
                    <tbody id="getOrder">
                        @foreach($bill as $bill)
                        <tr class="odd gradeX" align="center">
                            <td>00000{{$bill->id}}</td>
                            <td>{{$bill->Customer->name}}</td>
                            <td>{{ Carbon\Carbon::parse($bill->date_order)->format('d-m-Y')}}</td>
                             {{-- <td>@if($bill->fee_ship != 0){{number_format($bill->fee_ship)}}<sup>đ</sup> @else {{'10,000'}}<sup>đ</sup> @endif</td>  --}}
                            <td>{{$bill->payment}}</td>
                            <td>{!!$bill->note!!}</td>
                            <td>{{number_format($bill->total)}}<sup>đ</sup></td>
                            <td><a href="{{route('product.detail.im',$bill->id)}}">Chi tiết</a></td>
                            <td>
                                @if($bill->status ==1)
                               <span style="color: blue">{{'Chờ giao hàng'}}</span>
                                @elseif($bill->status ==2)
                                <span style="color: blue">{{'Xác nhận giao hàng'}}</span>
                                @else
                                <span style="color: blue">{{'Đang giao hàng'}}</span>
                                @endif
                            </td>
                            <td> 
                                @if($bill->status ==1)  
                               <a href="{{ route('get.status.ship2',['id' =>$bill->id]) }}" class="btn btn-sm btn-link"><i class="fa fa-rocket fa-fw"></i> <span style="color: blue">{{'Xác nhận giao hàng'}}</span></a>
                                @elseif($bill->status ==2)
                               <a href="{{ route('get.status.ship3',['id' =>$bill->id]) }}" class="btn btn-sm btn-link"><i class="fa fa-rocket fa-fw"></i><span style="color: blue">{{'Đang giao hàng'}}</span></a>
                                 @else
                                <a href="{{ route('get.status.ship4',['id' =>$bill->id]) }}" class="btn btn-sm btn-link"><i class="fa fa-rocket fa-fw"></i> <span style="color: green">{{'Giao hàng thành công'}}</span></a>
                                <a href="{{ route('get.status.ship5',['id' =>$bill->id]) }}" class="btn btn-sm btn-link"><i class="fa fa-rocket fa-fw"></i><span style="color: red">{{'Từ chối nhận hàng'}}</span></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        <!-- /.row -->
        </div>
    <!-- /.container-fluid -->
    </div>
 
@endsection
@section('script')
   <script type="text/javascript">
        $('#statusID').on('change',function(e){
            console.log(e);
            var status_id = e.target.value;
            // alert(status_id);
            $.get('imployeed/orderFilter/'+status_id,function(data){
               $('#getOrder').html(data);
            });
        });
    </script>
@endsection



    