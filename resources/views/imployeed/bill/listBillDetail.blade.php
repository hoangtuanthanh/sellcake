 @extends('imployeed.layout.index')
 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Chi tiết đơn hàng
                            <small>List</small>
                        </h1>
                        <a href="{{route('get.list.bill.im')}}" style="float: right; font-size: 18px;">Danh sách đơn hàng</a>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                             {{session('thongbao')}}
                        </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Mã đơn hàng </th>
                                <th>Sản phẩm</th>
                                <th>Số lượng</th>  
                                <th>Gía sản phẩm</th>
                            
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($bill_detail as $bd)
                            <tr class="odd gradeX" align="center">
                                <td>00000{{$bd->id}}</td>
                                <td>{{$bd->id_bill}}</td>
                                <td>{{$bd->Product->name}}</td>
                                <td>{{$bd->quantity}}</td>
                                <td>{{number_format($bd->unit_price)}}<sup>đ</sup></td> 
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection