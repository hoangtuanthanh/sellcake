 @extends('admin.layout.index')
 @section('title','Danh sách user');
 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">User
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                             {{session('thongbao')}}
                        </div>
                    @endif

                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Tên</th>                               
                                <th>Email</th>
                                <th>Password</th>
                                <th>Trạng thái</th>    
                                <th>Quyền người dùng</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($user as $user)
                            <tr class="odd gradeX" align="center">
                                <td>{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->password}}</td>
                                <td>
                                    @if($user->role == 0 ||$user->role == 2)
                                        @if($user->lock ==1) <p style="color: green">{{'active'}}</p>
                                        @else <p style="color: red">{{'unactive'}}</p>
                                        @endif
                                    @else <p style="color: green">{{'active'}}</p>  
                                    @endif
                                </td>
                                <td>
                                    @if($user->role == 0) {{'Khách hàng'}}
                                    @elseif($user->role == 1) {{'Admin'}}
                                    @else {{'Nhân viên giao hàng'}}
                                    @endif
                                </td>
                                <td>
                                    @if($user->role == 0 ||$user->role == 2)
                                        @if($user->lock == 1)
                                            <a href="{{route('get.lock.user',['id'=>$user->id])}}" class="btn btn-sm btn-link"><i class="fa fa-lock fa-fw"></i>Khóa tài khoản</a>
                                        @else
                                            <a href="{{route('get.unlock.user',['id'=>$user->id])}}" class="btn btn-sm btn-link"><i class="fa fa-lock fa-fw"></i> Mở khóa tài khoản</a>
                                        @endif
                                    @endif
                                </td>
                               
                            
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection