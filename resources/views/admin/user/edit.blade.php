 @extends('admin.layout.index')
 @section('title','Sửa user');
 @section('content')
  <!-- Page Content -->
         <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">User
                            <small>Edit</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>    
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="admin/user/edit/{{$user->id}}" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label>Tên</label>
                                 <input class="form-control" name="name" placeholder="Please Enter Name" value="{{$user->name}}" />
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input class="form-control" type="" name="email" placeholder="Please Enter Email" value="{{$user->email}}"  readonly />
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="check" id="check" />
                                <label>Đổi mật khẩu</label>
                                <input type="password" class="form-control password" name="password" placeholder="Nhập mật khẩu"
                                disabled="" value="{{$user->password}}"/>
                            </div>
                             <div class="form-group">
                                <label>Nhập lại mật khẩu</label>
                                <input type="password" class="form-control password" name="passwordAgain" placeholder="Nhập lại mật khẩu" disabled="" value="{{$user->password}}"/>
                            </div>

                            <div class="form-group">
                                <label>Quyền người dùng</label>
                                <label class="radio-inline">
                                    <input name="role" value="0"
                                       @if($user->role == 0)
                                            {{"checked"}}
                                       @endif
                                        type="radio">Thường
                                </label>
                                <label class="radio-inline">
                                    <input name="role" value="1"
                                        @if($user->role == 1)
                                       {{"checked"}}
                                       @endif
                                    type="radio">Admin
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">User Edit</button>
                            {{-- <button type="reset" class="btn btn-default">Reset</button> --}}
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection
<!-- <script src="{{('admin_asset/js/jquery-3.4.1.min.js')}}"></script> -->
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
       $("#check").change(function(){
           if($(this).is(":checked"))
           {
            $(".password").removeAttr('disabled');
           }
           else
           {
            $(".password").attr('disabled','');
           }
       });

    });

</script>
@endsection