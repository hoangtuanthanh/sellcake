 @extends('admin.layout.index')
  @section('title','Thông tin công ty');
 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">CompanyInfo
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                             {{session('thongbao')}}
                        </div>
                    @endif

                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Địa chỉ </th>  
                                 <th>Mã số thuế</th>
                                <th>Điện thoại</th>                            
                                 <th>email</th>
                                <th>hotline</th>
                                <th>website</th>
                                 <th>Edit</th>
                                 {{-- <th>Delete</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($company as $company)
                            <tr class="odd gradeX" align="center">
                                <td>{{$company ->id}}</td>
                                <td>{{$company ->address}}</td>
                                <td>{{$company ->tax_number}}</td>
                                <td>{!!$company ->phone!!}</td>
                                <td>{{$company ->email}}</td>
                                <td>{{$company ->hotline}}</td>
                                <td>{!!$company ->website!!}</td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/companyinfo/edit/{{$company->id}}">Edit</a></td>
                             {{--    <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/companyinfo/delete/{{$company->id}}">Delete</a></td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection