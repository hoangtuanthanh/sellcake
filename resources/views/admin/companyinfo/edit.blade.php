@extends('admin.layout.index')
  @section('title','Sửa thông tin công ty');
 @section('content')
         <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Company Infomation
                            <small>{{$company->website}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                     @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                       <form action="admin/companyinfo/edit/{{$company->id}}" method="POST" enctype="multipart/form-data">
                             <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label>Địa chỉ</label>
                                <input class="form-control" name="address" placeholder="Please Enter address" value="{{$company->address}}" />
                            </div>
                            <div class="form-group">
                                <label>Mã số thuế</label>
                               <input class="form-control" name="tax_number" placeholder="Please Enter tax_number" value="{{$company->tax_number}}" />
                            </div>
                              <div class="form-group">
                                <label>Địa thoại</label>
                                 <input class="form-control" name="phone" placeholder="Please Enter phone" value="{{$company->phone}}" />
                            </div>
                            <div class="form-group">
                                <label>email</label>
                                 <input class="form-control" name="email" placeholder="Please Enter email" value="{{$company->email}}" />
                            </div>
                            <div class="form-group">
                                <label>hotline</label>
                                <input class="form-control" name="hotline" placeholder="Please Enter hotline" value="{{$company->hotline}}" />
                            <div class="form-group">
                                <label>website</label>
                               <input class="form-control" name="website" placeholder="Please Enter website" value="{{$company->website}}" />
                            </div>
                            <button type="submit" class="btn btn-default">company edit</button>
                            {{-- <button type="reset" class="btn btn-default">Reset</button> --}}
                        <form>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection