 @extends('admin.layout.index')
 @section('title','Danh sách đăng kí')
 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Subcriber
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Email</th>
                                <th>Ngày gửi</th> 
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($sub as $sub)
                            <tr class="odd gradeX" align="center">
                                <td>{{$sub->id}}</td>
                                <td>{{$sub->email}}</td>
                                <td>{{$sub->created_at}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection