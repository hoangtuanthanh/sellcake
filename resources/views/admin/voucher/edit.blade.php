@extends('admin.layout.index')
@section('title','Sửa mã giảm giá');
 @section('content')
         <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Voucher
                            <small>{{$voucher->name}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                     @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                       <form action="{{route('edit.voucher',$voucher->id)}}" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                           <div class="form-group">
                                <label>Mã giảm giá</label>
                                <input class="form-control" name="code"value="{{$voucher->code}}" placeholder="Please Enter code" />
                            </div>
                            <div class="form-group">
                                <label>Tên mã giảm giá</label>
                                <input class="form-control" name="name" value="{{$voucher->name}}" placeholder="Please Enter name" />
                            </div>
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea style="resize: none" rows="8"value="{{$voucher->description}}" class="form-control" name="description" id="description" placeholder="Nhập nội dung"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Số lượt sử dụng tối đa</label>
                                <input class="form-control" name="max_uses_user"value="{{$voucher->max_uses_user}}" placeholder="Please Enter " />
                            </div>
                            <div class="form-group">
                                <label>Gía giảm</label>
                                <input class="form-control" name="discount_amount"value="{{$voucher->discount_amount}}" placeholder="Please Enter" />
                            </div>
                            <div class="form-group">
                                <label>Ngày tạo</label>
                                <input class="form-control" type="date" name="starts_at"value="{{$voucher->starts_at}}" placeholder="Please Enter " />
                            </div>
                            <div class="form-group">
                                <label>Ngày hết hạn</label>
                                <input class="form-control" type="date" name="expires_at"value="{{$voucher->expires_at}}" placeholder="Please Enter " />

                            </div>
                              
                            <button type="submit" class="btn btn-default">Voucher Edit</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection