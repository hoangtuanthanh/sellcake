 @extends('admin.layout.index')
 @section('title','Danh sách mã giảm giá');
 @section('content')<!-- Page Content -->
 <div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"style=" margin: 56px 0 20px;">Voucher
                    <small>List</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            @if(session('thongbao'))
            <div class="alert alert-success">
                {{session('thongbao')}}
            </div>
            @endif


            <table class="table  table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr align="center">
                        <th>Mã giảm giá</th>
                        <th>Tên mã</th>
                        {{-- <th>Miêu tả </th>   --}}
                        <th>Giới hạn sử dụng</th>
                        <th>Đã sử dụng được</th>  
                        <th>Gía giảm</th>                                       
                        <th>Ngày tạo</th>
                        <th>Ngày hết hạn</th>
                        <th>Delete</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($voucher as $voucher)
                    <tr class="odd gradeX" align="center">
                        <td>{{$voucher->code}}</td>
                        <td>{{$voucher->name}}</td>
                        {{-- <td style=" width: 400px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;display: -webkit-box;-webkit-box-orient: vertical;">{!!$voucher->description!!}</td> --}}
                        <td>{{$voucher->max_uses_user}}</td>
                        <td>{{$voucher->used_user}}</td>
                        <td> {{ number_format($voucher->discount_amount)}} <sup>đ</sup></td>
                        <td>{{$voucher->starts_at}}</td>
                        <td>{{$voucher->expires_at}}</td>

                        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/voucher/delete/{{$voucher->id}}"> Delete</a></td>
                        <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/voucher/edit/{{$voucher->id}}">Edit</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection