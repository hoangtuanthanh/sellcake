 @extends('admin.layout.index')
 @section('title','Danh sách loại sản phẩm');
 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Type Product
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                             {{session('thongbao')}}
                        </div>
                    @endif

                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Image</th>
                                <th>Tên loại</th>                               
                                <th>Miêu tả</th>
                                <th>Slug</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($type_pro as $tp)
                            <tr class="odd gradeX" align="center">
                                <td>{{$tp->id}}</td>
                                <td><img src="frontend/typeproduct/{{$tp->image}}" width="100px" height="100px" alt=""></td>
                                <td>{{$tp->name}}</td>
                                <td style=" width: 300px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;display: -webkit-box;-webkit-box-orient: vertical;">{!!$tp->desc!!}</td>
                                 <td>{{$tp->slug}}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/typeproduct/delete/{{$tp->id}}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/typeproduct/edit/{{$tp->id}}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection