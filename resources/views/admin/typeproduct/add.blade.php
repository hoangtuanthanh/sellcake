 @extends('admin.layout.index')
 @section('title','Thêm loại sản phẩm');
 @section('content')
  <!-- Page Content -->
         <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Type Product
                            <small>Add</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="admin/typeproduct/add" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            @csrf()
                            <div class="form-group">
                                <label>image </label>
                                <input class="form-control" type="file" name="image" placeholder="Please Enter Image" />
                                @if(session('loi'))
                                <div class="alert alert-success">
                                    {{session('loi')}}
                                </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Tên loại</label>
                                 <input class="form-control" name="name" placeholder="Please Enter Name" />
                            </div>
                            <div class="form-group">
                                <label>Miêu tả</label>
                                <textarea name="desc" class="form-control" id="content" rows="10" ></textarea>
                            </div>
                           
                            <button type="submit" class="btn btn-default">Type Product Add</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection