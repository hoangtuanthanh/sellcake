@extends('admin.layout.index')
@section('title','Sửa loại sản phẩm');
 @section('content')
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Type Product
                            <small>{{$type_pro->name}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    @if(count($errors)> 0)
                        @foreach($errors as $err)
                            <div class="alert alert-danger">
                                {{$err}}<br>
                            </div>
                        @endforeach
                     @endif()
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="admin/typeproduct/edit/{{$type_pro->id}}" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label>Tên loại</label>
                                 <input class="form-control" name="name" placeholder="Please Enter Name" value="{{$type_pro->name}}" />
                            </div>
                            <div class="form-group">
                                <label>Miêu tả</label>
                                <textarea style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis; " name="desc" class="form-control" id="content" rows="10" >{!!$type_pro->desc!!}</textarea>
                            </div>
                            <div class="form-group">
                                <label>image </label>
                                <p><img src="frontend/typeproduct/{{$type_pro->image}}" width="100px" height="100px" alt=""></p>
                                <input class="form-control" type="file" name="image" placeholder="Please Enter Image" />
                            </div>
                            <button type="submit" class="btn btn-default">Type Product Edit</button>
                         
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection