 @extends('admin.layout.index')
@section('title','Danh sách liên hệ chưa xử lý')
@section('content')
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Contact
                            <small>Danh sách liên hệ chưa xử lý</small>
                        </h1>
                        <a href="{{route('list.processed')}}" style="float: right; font-size: 18px;">Danh sách liên hệ đã xử lý</a>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                             {{session('thongbao')}}
                        </div>
                    @endif

                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th style="width:10%">ID</th>
                            <th style="width:10%">Tên</th>
                            <th style="width:20%" >Email</th>
                            <th style="width:6%">Địa chỉ</th>
                            <th style="width:6%">Số điện thoại</th>
                            <th style="width:10%" >Nội dung</th>
                            <th style="width:15%" >Ngày gửi liên hệ</th>
                            <th style="width:15%" >Hành động</th>
                            {{-- <th style="width:15%" >Liên hệ được chỉnh sửa</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($contact as $ct)
                        {{--  @dd($ct) --}}
                        <tr class="odd gradeX" align="center">
                            <td class="v-align-middle">{{$ct->id}}</td>
                            <td class="v-align-middle">{{$ct->name}}</td>
                            <td class="v-align-middle" >{{$ct->email}}</td>
                            <td class="v-align-middle">{{$ct->address}}</td>
                            <td class="v-align-middle">{{$ct->phone}}</td>
                            <td class="v-align-middle" style=" width: 300px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;display: -webkit-box;-webkit-box-orient: vertical;">{!!$ct->content!!}</td>
                            <td class="v-align-middle">{{$ct->created_at}}</td>
                            <td>
                                <a  href="{{ route('get.xu.ly.contact',['id' => $ct->id]) }}" onclick="return confirm('bạn đã chắc chắn muốn xử lý chưa?')">  <button type="button"  class="btn btn-primary">Chờ xử lý</button></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
                </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
@endsection
