 @extends('admin.layout.index')
  @section('title','Danh sách đơn hàng');
 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Bill
                            <small>Danh sách đơn hàng</small>
                        </h1>
                        <h4 style="float: right; color: red"><a href="{{route('list.bill.processed')}}">Danh sách đơn hàng đã giao</a></h4>
                        <h4 style="float: right;margin-right: 20px; display: inline-block;color: red;"><i class="fas fa-house-user"></i><a href="{{route('list.bill.refuse')}}">Danh sách đơn hàng từ chối</a></h4>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                             {{session('thongbao')}}
                        </div>
                    @endif
                    <select name="statusID" id="statusID" class="col-lg-4" style="margin-bottom: 10px;padding: 4px">
                        <option value="0">--Chọn trạng thái cần tìm--</option>
                        <option value="1">Chờ giao hàng</option>
                        <option value="2">Xác nhận giao hàng</option>
                        <option value="3">Đang giao hàng</option>
                    </select>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Mã đơn hàng</th>
                                <th>Khách hàng</th>
                                <th>Ngày đặt</th>
                                <th>Thanh toán</th>
                                <th>Tổng tiền</th>
                                <th>Trạng thái</th>
                                <th>Chi tiết sản phẩm</th>
                            </tr>
                        </thead>
                        <tbody id="getOrder">
                            @foreach($bill as $bill)
                            <tr class="odd gradeX" align="center">
                                <td>00000{{$bill->id}}</td>
                                <td>{{ $bill->Customer->name }}</td>
                                <td>{{ Carbon\Carbon::parse($bill->date_order)->format('d-m-Y')}}</td>
                                <td>{{$bill->payment}}</td>
                                <td>
                                    {{ number_format($bill->total)}}<sup>đ</sup>
                                </td>
                                <td>
                                    @if($bill->status ==1)
                                    <span style="color: blue">{{'Chờ giao hàng'}}</span>
                                    @elseif($bill->status ==2)
                                    <span style="color: blue">{{'Xác nhận giao hàng'}}</span>
                                    @elseif($bill->status ==3)
                                     <span style="color: blue">{{'Đang giao hàng'}}</span>
                                     @elseif($bill->status ==4)
                                    <span style="color: green">{{'Giao hàng thành công'}}</span>
                                    @else
                                    <span style="color: red">{{'Từ chối nhận hàng'}}</span>
                                    @endif
                                </td>
                                 <td><a href="{{route('product.detail',['id' => $bill->id])}}">Chi tiết</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection
@section('script')
   <script type="text/javascript">
            $('#statusID').on('change',function(e){
                console.log(e);
                var status_id = e.target.value; //lấy giá trị e
                // alert(status_id);
                $.get('admin/bill/orderFilter_bill/'+status_id,function(data){
                   $('#getOrder').html(data);
                });
            });
    </script>
@endsection