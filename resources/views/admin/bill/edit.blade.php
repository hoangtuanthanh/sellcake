@extends('admin.layout.index')
 {{-- @section('title','Sửa '); --}}
 @section('content')
         <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Bill
                            <small>{{"Edit"." ".$bill->id}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                    {{$err}}<br>
                                @endforeach
                            </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif

                        <form action="admin/bill/edit/{{$bill->id}}" method="POST" enctype="">
                            @csrf()
                            <input type="hidden" name="_token" value="{{csrf_token()}}"> {{-- phải khai báo token - không sẽ không truyền lên được --}}
                            <div class="form-group">
                                <label>Khách hàng</label>*
                                 <select class="form-control" name="id_customer">
                                    @foreach($customer as $cm)
                                        <option
                                        @if($bill->id_bill == $cm->id)
                                           {{"selected"}} 
                                        @endif
                                         value="{{$cm->id}}">{{$cm->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Ngày đặt</label>
                                <input type="date" name="date_order" class="form-control" placeholder="Nhập tên Date_order" value="{{ Carbon\Carbon::parse($bill->date_order)->format('d-m-Y')}}">
                            </div>
                            <div class="form-group">
                                <label>Tổng tiền</label>*
                                <input type="Ten" name="total" class="form-control" placeholder="Nhập tên Total" value="{{number_format($bill->total)}}">
                            </div>
                            <div class="form-group">
                                <label>Thanh toán</label>
                                <input type="text" name="payment" class="form-control" placeholder="Nhập tên Payment" value="{{$bill->payment}}">
                            </div>
                            <div class="form-group">
                                <label>Ghi chú</label>
                               <textarea name="note" id="content" class="form-control" id="content" rows="10" >{{$bill->note}}</textarea>
                            </div>
                            <button type="submit" class="btn btn-default">Bill Edit</button>
                            {{-- <button type="reset" class="btn btn-default">Reset</button> --}}
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection