 @extends('admin.layout.index')
  @section('title','Chi tiết đơn hàng');
 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Chi tiết đơn hàng
                            {{-- <small>Mã đơn hàng:{{$bill_d->id}}</small> --}}
                        </h1>
                         <a href="{{route('list.bill')}}" style="float: right; font-size: 18px;">Danh sách đơn hàng</a>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                             {{session('thongbao')}}
                        </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Mã đơn hàng </th>
                                <th>Sản phẩm</th>
                                <th>Số lượng</th>  
                                <th>Gía Thực </th>
                            </tr>
                        </thead>
                        <tbody> 
                            @foreach($bill_detail as $bill_detail)
                            <tr class="odd gradeX" align="center">
                                <td>00000{{$bill_detail->id_bill}}</td>
                                <td>{{$bill_detail->Product->name}}</td>
                                <td>{{$bill_detail->quantity}}</td>
                                <td>{{number_format($bill_detail->unit_price)}}<sup>đ</sup></td> 
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection