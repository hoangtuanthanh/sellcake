 @extends('admin.layout.index')
  @section('title',' Đơn hàng đã giao');

 @section('content')<!-- Page Content -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Bill
                            <small>Danh sách đơn hàng đã giao</small>
                        </h1>
                        <h4 style="float: right; color: red"><a href="{{route('list.bill')}}">Danh sách đơn hàng đang xử lý</a></h4>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                             {{session('thongbao')}}
                        </div>
                    @endif
                    <hr>
                    <form action="{{route('dowload.excel')}}" method="post" class="row">
                    @csrf
                        <div class="col-md-3 col-sm-5" style="margin-bottom:10px">
                            Từ ngày:<input type="date" id="start_date" class="form-control" name="start_date">
                        </div>
                        <div  class="col-md-3 col-sm-5" style="margin-bottom:10px">
                            Đến ngày:<input type="date" id="end_date" class="form-control" name="start_date">
                        </div>
                        <input type="text" name="sellBuy" value="1" hidden>
                        <div class="col-md-3 col-sm-3" style="margin-top: 20px;">
                            <button type="button" id="fill" onclick="searchBills()" class="btn btn-default">Tìm kiếm</button>
                        </div>
                        <div class="col-md-3 col-sm-3" style="margin-top: 20px;">
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-cloud-download"></i>Xuất File Excel
                            </button>
                        </div>
                    </form>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Mã đơn hàng</th> 
                                <th>Khách hàng</th>
                                <th>Ngày đặt hàng</th>
                                {{-- <th>Phí ship</th> --}}
                                <th>Thanh toán</th>
                                <th>Ghi chú</th>
                                <th>Ngày xử lý</th>
                                 <th>Tiền bánh</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_bill">
                            @foreach($bills as $bill)
                            <tr class="odd gradeX" align="center">
                               <td>00000{{$bill->id}}</td>
                                <td>{{$bill->Customer['name']}}</td>
                                <td>{{ Carbon\Carbon::parse($bill->date_order)->format('d-m-Y')}}</td>
                                {{-- <td>@if($bill->fee_ship != 0){{number_format($bill->fee_ship)}} @else {{'10,000'}} @endif <sup>đ</sup></td>  --}}
                                <td>{{$bill->payment}}</td>
                                <td>{!!$bill->note!!}</td>
                                 <td>{{ Carbon\Carbon::parse($bill->updated_at)->format('d-m-Y')}}</td>
                                <td>{{number_format($bill->total)}}<sup>đ</sup></td>
                                 
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div > Tổng số doanh thu thu được <span id="total">của tháng {{date('m')}} : {{ number_format($billCount) }}</span><sup>đ</sup> </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection
@section('script')
    <script>
        $('#fill').change(function(){
            var start = $('#start_date').val();
            var end = $('#end_date').val();
            if (start != "" && end != "") {
                $.ajax({
                     headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "{{ route('bill.count.date') }}",
                    data: {
                        start: start,
                        end: end,
                    },
                }).done(function (data) {
                    $('#total').html($bill_count);
                   
                        
                }).fail(function (Responsive){
                    console.log(Responsive);
                })
            }
        })
    </script>
@endsection