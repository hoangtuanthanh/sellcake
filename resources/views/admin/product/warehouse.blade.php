 @extends('admin.layout.index')
 @section('title','Quản lý kho hàng');
 @section('content')<!-- Page Content -->
    {{-- <?php use App\BillDetail; ?> --}}
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Product
                            <small>Warehouse</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                             {{session('thongbao')}}
                        </div>
                    @endif
                    <style>
                        span.left{
                            float: left;
                            border-radius: 2px;
                            margin-bottom: 2px;
                        }
                    </style>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Image</th>
                                <th>Tên sản phẩm</th>
                                <th>Loại Sản Phẩm </th> 
                                <th>Sản phẩm-</th>
                                <th>Size</th>
                                <th>Gía nhập</th>
                                <th>Gía bán</th>
                                <th>Gía khuyễn mãi</th>                         
                                <th>Số lượng</th>
                                <th>Đã bán</th>
                                <th>Lợi nhuận</th>
                                <th>Ngày sản xuất</th>
                                <th>Hạn sử dụng</th>
                                <th>Trạng thái</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($product as $pro)
                            <tr class="odd gradeX" align="center">
                                <td><img src="frontend/product/{{$pro->image}}" width="100px" height="100px" alt=""></td>
                                <td>{{$pro->name}}</td>
                                <td>{{$pro->TypeProduct->name}}</td> 
                                <td>{{$pro->new}}</td>
                                <td>{{$pro->size}}</td>
                                {{-- <td>{{$pro->gianhap}}</td> --}}
                                <td>{{number_format($pro->gianhap)}}<sup>đ</sup></td>
                                
                                <td>{{number_format($pro->unit_price)}}<sup>đ</sup> </td>
                                <td>{{number_format($pro->promotion_pice)}}<sup>đ</sup></td>
                                <td>{{$pro->soluong}}</td>
                                <td>
                                    {{ $pro->daban}}
                                </td>
                                <td>{{number_format($pro->loinhuan)}}<sup>đ</sup></td>

                                <td>{{Carbon\Carbon::parse($pro->ngaysx)->format('d-m-Y') }}</td>
                                <td>{{Carbon\Carbon::parse($pro->hansd)->format('d-m-Y')}}</td>
                                <td>
                                    @if($pro->daban == $pro->soluong )
                                        @if($pro->hansd <= Carbon\Carbon::now())
                                            <p><span class="btn-danger left" style="padding: 3px">Hết hàng</span><br><span class="btn-danger left" style="padding: 3px">Hết hạn sử dụng</span></p>
                                        @elseif($pro->hansd > Carbon\Carbon::now())
                                             <p><span class="btn-danger left" style="padding: 3px">Hết hàng</span>
                                        @else
                                                {{'...'}}
                                        @endif
                                    @elseif($pro->daban < $pro->soluong)
                                        @php $spcon = $pro->soluong - $pro->daban @endphp
                                        @php 
                                            $ngaysx = $pro->ngaysx;
                                            $hansd = $pro->hansd;
                                            $datetime1 = new DateTime($ngaysx);
                                            $datetime2 = new DateTime($hansd);
                                            $interval = $datetime1->diff($datetime2); //diff : so sánh 
                                            $days = $interval->format('%a'); // %a chả về tổng số ngày
                                        @endphp
                                        @if($pro->hansd == Carbon\Carbon::now() || $pro->hansd < Carbon\Carbon::now())
                                            <p><span class="btn-success left" style="padding: 3px">Còn <span class="" style="color: blue;font-weight: bold;">{{$spcon}}</span> sản phẩm</span><br><span class="btn-danger left" style="padding: 3px">Hết hạn sử dụng</span></p>
                                        @elseif($pro->hansd > Carbon\Carbon::now()) 
                                            <p><span class="btn-success left" style="padding: 3px">Còn <span class="" style="color: blue;font-weight: bold;">{{$spcon}}</span> sản phẩm</span><br><span class="btn-success left" style="padding: 3px"> Hạn sử dụng còn <span class="" style="color: blue;font-weight: bold;">{{$days}} </span>Ngày</span></p>
                                        @endif
                                    @else {{'.?.?.'}}
                                    @endif 
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection