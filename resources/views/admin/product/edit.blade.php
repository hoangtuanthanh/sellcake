@extends('admin.layout.index')
@section('title','Sửa sản phẩm');
 @section('content')
       <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Product
                            <small>{{"Edit"." ".$product->name}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                     @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                    @endif

                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="admin/product/edit/{{$product->id}}" method="POST" enctype="multipart/form-data" >
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                 <div class="form-group">
                                    <label>Tên sản phẩm</label>
                                    <input class="form-control" name="name" placeholder="Vui lòng nhập Name" value="{{$product->name}}" />
                                </div>
                                <label>Loại sản phẩm</label>
                                <select class="form-control" name="id_type">
                                    @foreach($type as $type)
                                        <option
                                        @if($product->id_type == $type->id) {{-- tự động nhảy ra cái id loại sp luôn --}}
                                        {{"selected"}}                  {{--                                     --}}
                                        @endif
                                        value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Miêu tả</label>
                               <textarea name="desc" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis; " class="form-control" id="content" rows="10" >{!!$product->desc!!}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Gía thực(VNĐ) </label>
                                <input class="form-control" name="unit_price" placeholder="Vui lòng nhập Unit_price" value="{{$product->unit_price}}" />
                            </div>
                            <div class="form-group">
                                <label>Gía khuyến mãi(VNĐ) </label>
                                <input class="form-control" name="promotion_pice" placeholder="Vui lòng nhập Promotion_pice" value="{{$product->promotion_pice}}"đ />
                            </div>
                             <div class="form-group">
                                <label>Đơn vị</label>
                                <input class="form-control" name="unit" placeholder="Vui lòng nhập Unit" value="{{$product->unit}}" />
                            </div>
                            <div class="form-group">
                                <label>Sản phẩm</label>
                                <select name="new" id="" class="form-control">
                                    <option value="khuyến mãi">Sản phẩm khuyến mãi</option>
                                    <option value="Mới">Sản phẩm mới</option>
                                    <option value="Bán chạy">Sản phẩm bán chạy </option>
                                </select>
                                  
                            </div>
                             <div class="form-group">
                                <label>Số lượng </label>
                                <input class="form-control" name="soluong" placeholder="Vui lòng nhập Số lượng" value="{{$product->soluong}}" />
                                 @if(session('loiSL'))
                                    <div class="alert alert-danger">
                                         {{session('loiSL')}}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Ngày sản xuất </label>
                                <input class="form-control" type="date" name="ngaysx" placeholder="Vui lòng nhập Ngày sản xuất"value="{{$product->ngaysx}}" />
                            </div>
                             <div class="form-group">
                                <label>Hạn sử dụng</label>
                                <input class="form-control" type="date" name="hansd" placeholder="Vui lòng nhập Hạn sử dụng"value="{{$product->hansd}}" />
                                 @if(session('loiNSX'))
                                    <div class="alert alert-danger">
                                         {{session('loiNSX')}}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Size</label>
                                <select name="size" id="" class="form-control">
                                    <option value="M">Nhỏ</option>
                                    <option value="S">Vừa</option>
                                    <option value="L">Lớn</option>
                                </select>  
                            </div>
                            <div class="form-group">
                                <label>image</label>
                                <p><img src="frontend/product/{{$product->image}}" width="100px" height="100px" alt=""></p>
                                <input class="form-control" type="file" name="image" />
                                 @if(session('loi'))
                                    <div class="alert alert-success">
                                         {{session('loi')}}
                                    </div>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-default">Product Edit</button>
                            {{-- <button type="reset" class="btn btn-default">Reset</button> --}}
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection