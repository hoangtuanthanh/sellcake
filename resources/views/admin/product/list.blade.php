 @extends('admin.layout.index')
 @section('title','Danh sách sản phẩm');
 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Product
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                             {{session('thongbao')}}
                        </div>
                    @endif
                    <style>
                        td{
                            
                        }
                    </style>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Image</th>  
                                <th>Tên sản phẩm</th>
                                <th>Loại Sản Phẩm </th>                             
                                <th>Gía gốc </th>
                                <th>Gía bán </th>
                                <th>Gía khuyễn mãi</th>                         
                                <th>Đơn vị</th>
                                <th>Sản phẩm-</th>
                                <th>Miêu tả</th>
                               {{--  <th>Size</th>
                                <th>Số lượng</th>
                                <th>Đã bán</th>
                                <th>Ngày sản xuất</th>
                                <th>Hạn sử dụng</th> --}}
                                <th>Delete</th>
                                <th>Edit</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($product as $pro)
                            <tr class="odd gradeX" align="center">
                                <td>{{$pro->id}}</td>
                                <td><img src="frontend/product/{{$pro->image}}" width="100px" height="100px" alt=""></td>
                                <td>{{$pro->name}}</td>
                                <td>{{$pro->TypeProduct->name}}</td>       
                                <td>{{number_format($pro->gianhap)}}<sup>đ</sup> </td>
                                <td>{{number_format($pro->unit_price)}}<sup>đ</sup> </td>
                                <td>{{number_format($pro->promotion_pice)}}<sup>đ</sup></td>
                                <td>{{$pro->unit}}</td>
                                <td>{{$pro->new}} </td>
                                <td style=" width: 300px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;display: -webkit-box;-webkit-box-orient: vertical;">{!!$pro->desc!!}</td>
                               {{--  <td>{{$pro->size}}</td>
                                <td>{{$pro->soluong}}</td>
                                <td>{{$pro->daban}}</td>
                                <td>{{Carbon\Carbon::parse($pro->ngaysx)->format('d-m-Y') }}</td>
                                <td>{{Carbon\Carbon::parse($pro->hansd)->format('d-m-Y')}}</td> --}}
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/product/delete/{{$pro->id}}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/product/edit/{{$pro->id}}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection