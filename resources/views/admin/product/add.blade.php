 @extends('admin.layout.index')
 @section('title','Thêm sản phẩm');
 @section('content')
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Product
                            <small>Add</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->

                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                      @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="admin/product/add" method="POST" enctype="multipart/form-data" >
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                             <div class="form-group">
                                <label>image</label>
                                <input class="form-control" type="file" name="image" placeholder="Vui lòng nhập đường dẫn ảnh" />
                                 @if(session('loiIMG'))
                                    <div class="alert alert-success">
                                         {{session('loiIMG')}}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                 <div class="form-group">
                                    <label>Tên</label>
                                    <input class="form-control" name="name" placeholder="Vui lòng nhập tên" />
                                </div>
                                <label>Loại sản phẩm</label>
                                <select class="form-control" name="id_type">
                                    @foreach($type as $type)
                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Desc</label>
                               <textarea name="desc" class="form-control" id="content" rows="10" ></textarea>
                            </div>
                            <div class="form-group">
                                <label>Gía thực </label>
                                <input class="form-control" name="unit_price" placeholder="Vui lòng nhập giá thực" />
                            </div>
                            <div class="form-group">
                                <label>Gía khuyến mãi </label>
                                <input class="form-control" name="promotion_pice" placeholder="Vui lòng nhập giá khuyến mãi" />
                            </div>
                             <div class="form-group">
                                <label>Đơn vị  </label>
                                <input class="form-control" name="unit" placeholder="Vui lòng nhập đơn vị" />
                            </div>
                            <div class="form-group">
                                <label>Sản phẩm- </label>
                               <select name="new" id="" class="form-control">
                                   <option value="khuyến mãi">Sản phẩm khuyến mãi</option>
                                    <option value="Mới">Sản phẩm mới</option>
                                    <option value="Bán chạy">Sản phẩm bán chạy </option>
                               </select>
                            </div>
                             <div class="form-group">
                                <label>Số lượng </label>
                                <input class="form-control" name="soluong" placeholder="Vui lòng nhập Số lượng" />
                                 @if(session('loiSL'))
                                    <div class="alert alert-success">
                                         {{session('loiSL')}}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Ngày sản xuất </label>
                                <input class="form-control" type="date" name="ngaysx" placeholder="Vui lòng nhập Ngày sản xuất" />
                                
                            </div>
                             <div class="form-group">
                                <label>Hạn sử dụng</label>
                                <input class="form-control" type="date" name="hansd" placeholder="Vui lòng nhập Hạn sử dụng" />
                                 @if(session('loiNSX'))
                                    <div class="alert alert-success">
                                         {{session('loiNSX')}}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Size </label>
                               <select name="size" id="" class="form-control">
                                   <option value="L">Lớn</option>
                                    <option value="S">Vừa</option>
                                    <option value="M">Nhỏ </option>
                               </select>
                            </div>
                          
                            <button type="submit" class="btn btn-default">Product Add</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection