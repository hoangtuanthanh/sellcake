 @extends('admin.layout.index')
 @section('title','Danh sách tin tức');
 @section('content')<!-- Page Content -->
 <div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"style=" margin: 56px 0 20px;">News
                    <small>List</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            @if(session('thongbao'))
            <div class="alert alert-success">
                {{session('thongbao')}}
            </div>
            @endif


            <table class="table  table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr align="center">
                        <th>ID</th>
                        <th>Image</th>
                        <th>Tiêu đề </th>  
                        <th>Slug</th>
                        <th>Nội dung</th>  
                        <th>Đường dẫn link</th>                                       
                        <th>Delete</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($new as $news)
                    <tr class="odd gradeX" align="center">
                        <td>{{$news ->id}}</td>
                         <td><img src="{{asset('frontend/news/' .$news->image)}}" width="100px" height="100px" alt=""></td>
                        <td>{{$news ->title}}</td>
                        <td>{{$news ->slug}}</td>
                        <td style=" width: 400px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;display: -webkit-box;-webkit-box-orient: vertical;">{!!$news ->content!!}</td>
                        <td>{{$news ->link}}</td>
                       
                        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/news/delete/{{$news->id}}"> Delete</a></td>
                        <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/news/edit/{{$news->id}}">Edit</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection