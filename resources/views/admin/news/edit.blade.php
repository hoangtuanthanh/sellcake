@extends('admin.layout.index')
@section('title','Sửa tin tức');
 @section('content')
         <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">News
                            <small>{{$news->title}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                     @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                       <form action="admin/news/edit/{{$news->id}}" method="POST" enctype="multipart/form-data">
                             <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input class="form-control" name="title" placeholder="Please Enter Title" value="{{$news->title}}" />
                            </div>
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content" class="form-control" id="content" rows="10" >{{$news->content}}</textarea>
                            </div>
                             <div class="form-group">
                                <label>Đường dẫn link</label>
                                <input class="form-control" name="link" placeholder="Please Enter Title" value="{{$news->link}}" />
                            </div>
                            <div class="form-group">
                                <label>Image</label>
                                <p><img src="frontend/news/{{$news->image}}" width="100px" height="100px" alt=""></p>
                                <input class="form-control" type="file"  name="image"/>
                                @if(session('loi'))
                                    <div class="alert alert-success">
                                         {{session('loi')}}
                                    </div>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-default">News edit</button>
                            {{-- <button type="reset" class="btn btn-default">Reset</button> --}}
                        <form>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection