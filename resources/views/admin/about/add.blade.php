 @extends('admin.layout.index')
 @section('title','Thêm thành tích');
 @section('content')
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Introduce
                            <small>Add</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                       <form action="admin/about/add" method="POST" enctype="multipart/form-data">
                             <input type="hidden" name="_token" value="{{csrf_token()}}">
                              <div class="form-group">
                                <label>Image</label>
                                <input class="form-control" value="{{old('image')}}" type="file" name="image"/>
                                @if(session('loi'))
                                    <div class="alert alert-success">
                                         {{session('loi')}}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input class="form-control" name="title" placeholder="Please Enter Title" value="{{old('title')}}" />
                            </div>
                             <div class="form-group">
                                <label>Thời gian</label>
                                <input class="form-control" name="time" placeholder="Please Enter time" value="{{old('time')}}" />
                            </div>
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea style="resize: none" rows="8" class="form-control" name="content" value="{{old('content')}}" id="content" placeholder="Nhập nội dung"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Địa chỉ</label>
                                <input class="form-control" name="address" value="{{old('address')}}" placeholder="Please Enter address" />
                            </div>
                             
                            <button type="submit" class="btn btn-default">about ADD</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection