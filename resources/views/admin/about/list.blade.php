 @extends('admin.layout.index')
  @section('title','Danh sách thành tích');
 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">About
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                             {{session('thongbao')}}
                        </div>
                    @endif

                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Image</th>
                                <th>Tiêu đề </th>  
                                 <th>Thời gian </th>  
                                 <th>Địa chỉ</th>
                                <th>Nội dung</th>                         
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($about as $about)
                            <tr class="odd gradeX" align="center">
                                <td>{{$about ->id}}</td>
                                <td><img src="{{asset('frontend/about/'.$about->image)}}" width="100px" height="100px" alt=""></td>
                                <td>{{$about ->title}}</td>
                                <td>{{$about ->time}}</td>
                                <td>{{$about ->address}}</td>
                                <td>{!!$about ->content!!}</td>
                                
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/about/delete/{{$about->id}}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/about/edit/{{$about->id}}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection