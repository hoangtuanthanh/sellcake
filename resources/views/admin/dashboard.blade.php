@extends('admin.layout.index')
@section('content')
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
 <script src="{{ asset('vendor/larapex-charts/apexchart.js') }}"></script> 
 <div id="page-wrapper">
    <div class="container-fluid">
      <div class="col-lg-12">
          <h1 class="page-header"style=" margin: 56px 0 20px;">Trang chủ admin</h1>
      </div>
        <div class="row">
          <div class="col-lg-3 col-md-6">
              <div class="panel panel-primary">
                  <div class="panel-heading">
                      <div class="row">
                          <div class="col-xs-3">
                             <i class="fa fa-shopping-cart fa-5x"></i>
                          </div>
                          <div class="col-xs-9 text-right">
                              <div class="huge">{{$product_count}}</div>
                              <div>Tổng số sản phẩm</div>
                          </div>
                      </div>
                  </div>
                  <a href="{{route('list.product')}}">
                      <div class="panel-footer">
                          <span class="pull-left">Xem chi tiết</span>
                          <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                          <div class="clearfix"></div>
                      </div>
                  </a>
              </div>
          </div>
          <div class="col-lg-3 col-md-6">
              <div class="panel panel-green">
                  <div class="panel-heading">
                      <div class="row">
                          <div class="col-xs-3">
                              <i class="fa fa-tasks fa-5x"></i>
                          </div>
                          <div class="col-xs-9 text-right">
                              <div class="huge">{{count($bill_count)}}</div>
                              <div>Tổng số đơn hàng</div>
                          </div>
                      </div>
                  </div>
                  <a href="{{route('list.bill')}}">
                      <div class="panel-footer">
                          <span class="pull-left">Xem chi tiết</span>
                          <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                          <div class="clearfix"></div>
                      </div>
                  </a>
              </div>
          </div>
          <div class="col-lg-3 col-md-6">
              <div class="panel panel-yellow">
                  <div class="panel-heading">
                      <div class="row">
                          <div class="col-xs-3">
                              <i class="fa fa-comments fa-5x"></i>
                             
                          </div>
                          <div class="col-xs-9 text-right">
                              <div class="huge">{{$news_count}}</div>
                              <div>Tổng bài viết đã đăng</div>
                          </div>
                      </div>
                  </div>
                  <a href="{{route('list.news')}}">
                      <div class="panel-footer">
                          <span class="pull-left">Xem chi tiết</span>
                          <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                          <div class="clearfix"></div>
                      </div>
                  </a>
              </div>
          </div>
          <div class="col-lg-3 col-md-6">
              <div class="panel panel-red">
                  <div class="panel-heading">
                      <div class="row">
                          <div class="col-xs-3">
                              <i class="fa fa-support fa-5x"></i>
                          </div>
                          <div class="col-xs-9 text-right">
                              <div class="huge">{{$customer_count}}</div>
                              <div>Số khách hàng liên hệ </div>
                          </div>
                      </div>
                  </div>
                 <a href="{{route('list.contact')}}">
                      <div class="panel-footer">
                          <span class="pull-left">Xem chi tiết</span>
                          <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                          <div class="clearfix"></div>
                      </div>
                  </a>
              </div>
          </div>
      </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    
    <div class="row">
      <h3>Thống kê các đơn hàng trong tháng</h3>
      <div class="col-md-6 col-sm-6">
        {!! $chart_status->container() !!}
        {!! $chart_status->script() !!}
      </div>
      <div class="col-md-6 col-sm-6">
        {!! $chart_bill->container() !!}
        {!! $chart_bill->script() !!}
      </div>
    </div>
    <div class="row">
      <h3>Thống kê doanh thu tháng trong năm</h3>
      <div class="col-md-12 col-sm-6">
        {!! $chart_total->container() !!} 
        {!! $chart_total->script() !!}
      </div>
    </div>
     <div class="row">
      <h3>Thống kê số tài khoản User và số lượng khách hàng đăng kí nhận thông tin</h3>
      <div class="col-md-6 col-sm-6">
        {!! $user->container() !!}
        {!! $user->script() !!}
      </div>
      <div class="col-md-6 col-sm-6">
        {!! $chart_sub->container() !!}
        {!! $chart_sub->script() !!}
      </div>
    </div>
     <div class="row">
          <div class="col-lg-12">
              <div class="panel panel-default">
                 <h3> Đơn đặt hàng mới </h3>
                     <select name="statusIDDash" id="statusIDDash" class="col-sm-4" style="margin-bottom: 10px;padding: 4px">
                        <option value="0">-- Chọn trạng thái cần tìm --</option>
                        <option value="1">Chờ giao hàng</option>
                        <option value="2">Xác nhận giao hàng</option>
                        <option value="3">Đang giao hàng</option>
                    </select>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Mã đơn hàng</th>
                                <th>Khách hàng</th>
                                <th>Ngày đặt</th>
                                <th>Tổng tiền</th>
                                {{-- <th>Phí ship</th> --}}
                                <th>Thanh toán</th>
                                <th>Trạng thái</th>
                                <th>Chi tiết sản phẩm</th>
                            </tr>
                        </thead>
                        <tbody id="getOrder">
                            @foreach($bill as $bill)
                            <tr class="odd gradeX" align="center">
                                <td>00000{{$bill->id}}</td>
                                <td>{{$bill->Customer['name']}}</td>
                                <td>{{ Carbon\Carbon::parse($bill->date_order)->format('d-m-Y')}}</td>
                               {{--  <td>
                                    @if($bill->fee_ship){{number_format($bill->fee_ship)}}
                                    @else {{'10,000'}}
                                    @endif
                                    <sup>đ</sup>
                                </td> --}}
                                <td>{{number_format($bill->total)}}<sup>đ</sup></td>
                                <td>{{$bill->payment}}</td>
                                <td>
                                  @if($bill->status ==1)
                                  <span style="color: blue">{{'Chờ giao hàng'}}</span>
                                  @elseif($bill->status ==2)
                                  <span style="color: blue">{{'Xác nhận giao hàng'}}</span>
                                  @elseif($bill->status ==3)
                                   <span style="color: blue">{{'Đang giao hàng'}}</span>
                                   @elseif($bill->status ==4)
                                  <span style="color: green">{{'Giao hàng thành công'}}</span>
                                  @else
                                  <span style="color: red">{{'Từ chối nhận hàng'}}</span>
                                  @endif
                                </td>
                                 <td><a href="{{route('product.detail',['id' => $bill->id])}}">Chi tiết</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ csrf_field() }}
              </div>
          </div>
          <!-- /.col-lg-4 -->
      </div>
</div>

@endsection
@section('script')
<script type="text/javascript">
   $(document).ready(function (){
      var date = new Date();      
      var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
      var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

      $('#start_date').val(moment(firstDay).format('YYYY-MM-DD'));
      $('#end_date').val(moment(lastDay).format('YYYY-MM-DD'));
      //Loc don hang
});
</script>
<script type="text/javascript">
      $('#statusIDDash').on('change',function(e){
      console.log(e);
      var status_id = e.target.value;
      $.get('admin/orderFilter_bill_dash/'+status_id,function(data){
         $('#getOrder').html(data);
      });
  });
</script>
@endsection
