 @extends('admin.layout.index')
 @section('title','Thêm slide');
 @section('content')
  <!-- Page Content -->
         <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Slide
                            <small>Add</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="admin/slide/add" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            @csrf()
                            <div class="form-group">
                                <label>Đường dẫn link</label>
                                 <input class="form-control" name="link" placeholder="Please Enter Link"  />
                            </div>
                            <div class="form-group">
                                <label>image </label>
                                {{-- <p><img src="frontend/typeproduct/{{$type_pro->image}}" width="100px" height="100px" alt=""></p> --}}
                                <input class="form-control" type="file" name="image" placeholder="Please Enter Image" />
                            </div>
                            <button type="submit" class="btn btn-default">Slide Add</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection