@extends('admin.layout.index')
@section('title','Sửa slide');
 @section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"style=" margin: 56px 0 20px;">Slide
                        <small>{{"Edit"." ".$slide->id}}</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                @endif
                @if(count($errors)> 0)
                    @foreach($errors as $err)
                        <div class="alert alert-danger">
                            {{$err}}<br>
                        </div>
                    @endforeach
                 @endif()
                <div class="col-lg-7" style="padding-bottom:120px">
                    <form action="admin/slide/edit/{{$slide->id}}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        @csrf()
                        <div class="form-group">
                            <label>Đường dẫn link</label>
                             <input class="form-control" name="link" placeholder="Please Enter Link" value="{{$slide->link}}" />
                        </div>
                        <div class="form-group">
                            <label>image </label>
                            <p><img src="frontend/slide/{{$slide->image}}" width="100px" height="100px" alt=""></p>
                            <input class="form-control" type="file" name="image" placeholder="Please Enter Image" />
                        </div>
                        <button type="submit" class="btn btn-default">Slide Edit</button>
                        {{-- <button type="reset" class="btn btn-default">Reset</button> --}}
                    <form>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
@endsection