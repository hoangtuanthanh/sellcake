@extends('admin.layout.index')
 @section('content')
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Bill Detail
                            <small>{{"Edit"."  ".$billdetail->id}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    @if(count($errors)> 0)
                        @foreach($errors as $err)
                            <div class="alert alert-danger">
                                {{$err}}<br>
                            </div>
                        @endforeach
                     @endif()
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="admin/billdetail/edit/{{$billdetail->id}}" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            @csrf()
                            <div class="form-group">
                                <label>Mã đơn hàng</label>
                                 <select name="id_bill" id="id_bill" class="form-control" >
                                   @foreach($bill as $bill)
                                        <option
                                        @if($billdetail->id_bill == $bill->id)
                                        {{"selected"}}
                                        @endif
                                         value="{{$bill->id}}">{{$bill->id}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" >
                                <label>Tên sản phẩm</label>
                                <select name="id_product" id="id_product" class="form-control">
                                    @foreach($product as $pro)
                                        <option
                                        @if($billdetail->id_product == $pro->id)
                                        {{"selected"}}
                                        @endif 
                                        value="{{$pro->id}}">{{$pro->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Số lượng  </label>
                                <input class="form-control" type="text" name="quantity" placeholder="Please Enter quantity" value="{{$billdetail->quantity}}" />
                            </div>
                             <div class="form-group">
                                <label>Gía  </label>
                                <input class="form-control" type="text" name="unit_price" placeholder="Please Enter quantity" value="{{ number_format($billdetail->unit_price) }} đ"  />
                            </div>
                            <button type="submit" class="btn btn-default">Billdetai Edit</button>
                            {{-- <button type="reset" class="btn btn-default">Reset</button> --}}
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection