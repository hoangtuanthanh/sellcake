 @extends('admin.layout.index')
 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Bill Detail
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                             {{session('thongbao')}}
                        </div>

                    @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Mã đơn hàng </th>
                                <th>Sản phẩm</th>
                                <th>Số lượng</th>  
                                <th>Gía</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($billdetail as $bd)
                            <tr class="odd gradeX" align="center">
                                <td>{{$bd->id_bill}}</td>
                                <td>{{$bd->Product['name']}}</td>
                                <td>{{$bd->quantity}}</td>
                                <td>{{number_format($bd->unit_price)}}<sup>đ</sup></td> 
                            
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/billdetail/delete/{{$bd->id}}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/billdetail/edit/{{$bd->id}}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection