 @extends('admin.layout.index')
 @section('title','Danh sách phí vận chuyển');

 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Delivery
                            <small>List</small>
                        </h1>
                    </div>
 
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                {{-- <th>ID</th> --}}
                                <th>Mã thành phố </th>
                                <th>Mã quận huyện</th>
                                <th>Mã xã</th>  
                                <th>Phí vận chuyển</th>
                               {{--  <th>Delete</th>
                                <th>Edit</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ship_all as $ship_all)
                            <tr class="odd gradeX" align="center">
                                <td>{{$ship_all->City->name_city}}</td>
                                <td>{{$ship_all->Province->name_province}}</td>
                                <td>{{$ship_all->Wards->name_ward}}</td>
                                <td> {{number_format($ship_all->feeship) }}<sup>đ</sup></td> 
                            
                                {{-- <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/billdetail/delete/{{$ship_all->id}}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/billdetail/edit/{{$ship_all->id}}">Edit</a></td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection