 @extends('admin.layout.index')
  @section('title','Thêm phí vận chuyển');
 @section('content')
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Delivery
                            <small>Add</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    
                  {{--   @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                            {{$err}}<br>
                            @endforeach
                        </div>
                    @endif --}}
                    <div class="col-lg-7" style="padding-bottom:120px">
                       <form action="route('add.deli')" method="POST" enctype="multipart/form-data">
                             <input type="hidden" name="_token" value="{{csrf_token()}}">
                             
                            <div class="form-group">
                                <label>Chọn thành phố</label>
                                <select name="city" id="city" class="form-control input-sm m-bot15 choose city">
                                    <option value="">>--Chọn tỉnh thành phố--<</option>
                                    @foreach($city as $ct)
                                        <option value="{{$ct->matp}}">{{$ct->name_city}}</option>
                                    @endforeach 
                                </select>
                            </div>
                             <div class="form-group">
                                <label>Chọn quận huyện</label>
                                <select name="province" id="province" class="form-control input-sm m-bot15 choose province">
                                    <option value="">>--Chọn quận huyện--<</option>
                                    @foreach($province as $pro)
                                        <option value="{{$pro->maqh}}">{{$pro->name_province}}</option>
                                    @endforeach 
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Chọn xã phường</label>
                                <select name="wards" id="wards" class="form-control input-sm m-bot15 wards">
                                    <option value="">>--Chọn xã phường--<</option>
                                    @foreach($wards as $wards)
                                        <option value="{{$wards->xaid}}">{{$wards->name_ward}}</option>
                                    @endforeach 
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Phí vận chuyển</label>
                                 <input class="form-control fee_ship"  type="text" name="feeship" placeholder="Please Enter feeship"   />
                            </div>
                            <button type="button" name="delivery" class="btn btn-default delivery">Delivery ADD</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection