
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Đăng nhập</title>

    <!-- Bootstrap Core CSS -->
    <link href="source/assets/dest/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="source/assets/dest/css/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="source/assets/dest/css/startmin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="source/assets/dest/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>
<body>

   
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="login-panel panel panel-default">

                <div class="panel-heading">
                    <h3 class="panel-title">Vui lòng đăng nhập </h3>
                </div>
                <div class="panel-body">
                    <form action="" method="post" class="beta-form-checkout" >
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                              <p class="help is-danger" style="color: red">{{ $errors->first('email') }}</p>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Password" name="password" type="password" value="">
                            <p class="help is-danger" style="color: red">{{ $errors->first('password') }}</p>
                        </div>
                        <div class="checkbox">
                            <label>
                                {{-- <input name="remember" type="checkbox" value="Remember Me" class="btn btn-primary">Remember Me --}}
                            </label>
                        </div>
                        <div style="margin-bottom: 10px;">
                            <button type="submit" class="l-btn font-weight-bold text-white mb-3 btn btn-primary">Đăng nhập</button>
                            <a href="{{route('register.get')}}" style="float: right;margin-bottom: 10px;">Đăng kí tài khoản</a>
                        </div>
                            
                         @if(session('thongbao'))
                            <div class="alert alert-danger">
                                {{session('thongbao')}}
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="source/assets/dest/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="source/assets/dest/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="source/assets/dest/js/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="source/assets/dest/js/startmin.js"></script>

</body>
</html>
