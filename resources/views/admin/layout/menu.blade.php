
  <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Tìm kiếm...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="admin/dash"><i class="fa fa-dashboard fa-fw"></i> Trang chủ admin</a>
                        </li>
                        <li>
                            <a href="admin/bill/list"><i class="fa fa-bar-chart-o fa-fw"></i> Quản lý đơn hàng</a>
                             
                        </li>
                        <li>
                            <a href="admin/customer/list"><i class="fa fa-users fa-fw"></i> Thông tin khách hàng <span class="label label-primary" style="margin-left: 0px;">{{$customer_count}}</span></a>
                    
                        </li>
                         <li>
                            <a href="admin/contact/list"><i class="fa fa-building"></i> Thông tin liên hệ<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/contact/list">Danh sách liên hệ</a>
                                </li>
                                <li>
                                    <a href="{{route('list.processed')}}">Danh sách liên hệ đã xử lý</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="admin/news/list">  <i class="fa fa-building"></i> Thông tin tin tức <span class="label label-primary" style="margin-left: 20px;">{{$new_count}}</span><span class="fa arrow pull-right "></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/news/list">Danh sách tin tức</a>
                                </li>
                                <li>
                                    <a href="admin/news/add">Thêm tin tức</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="admin/product/list"><i class="fa fa-tasks"></i> Quản lý sản phẩm<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/product/list"> Danh sách sản phẩm </a>
                                </li>
                                <li>
                                    <a href="admin/product/add">Thêm sản phẩm</a>
                                </li>
                                 <li>
                                    <a href="{{route('warehouse')}}">Quản lý kho</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="admin/slide/list"><i class="fa fa-sliders"></i>Slide<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/slide/list">Danh sách Slide</a>
                                </li>
                                <li>
                                    <a href="admin/slide/add">Thêm Slide</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="admin/typeproduct/list"><i class="fa fa-tasks"></i> Quản lý loại sản phẩm <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/typeproduct/list">Danh sách loại sản phẩm</a>
                                </li>
                                <li>
                                    <a href="admin/typeproduct/add">Thêm loại sản phẩm</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                         <li>
                            <a href="admin/about/list"><i class="fa fa-archive"></i> Thông tin giới thiệu <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/about/list">Danh sách </a>
                                </li>
                                <li>
                                    <a href="admin/about/add">Thêm giới thiệu </a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="admin/subcriber/list"><i class="fa fa-envelope-square"></i> Thông tin đăng kí <span class="label label-primary" style="margin-left: 20px;">{{$sub_count}}</span></a>
                         
                        </li>
                        <li>
                            <a href="admin/companyinfo/list"><i class="fa fa-building"></i> Thông tin cửa hàng</a>
                          
                        </li>
                         <li>
                            <a href="admin/delivery/list"><i class="fa fa-dashboard fa-fw"></i> Quản lý phí vận chuyển <span class="label label-primary" style="margin-left: 20px;"></span><span class="fa arrow pull-right "></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/delivery/list">Danh sách </a>
                                </li>
                                <li>
                                    <a href="admin/delivery/add">Thêm phí vận chuyển</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                         <li>
                            <a href="admin/voucher/list"><i class="fa fa-users"></i> Quản lý mã giảm giá <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/voucher/list">Danh sách mã giảm giá</a>
                                </li>
                                <li>
                                    <a href="admin/voucher/add">Thêm mã giảm giá</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="admin/user/list"><i class="fa fa-users"></i> Quản lý tài khoản <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/user/list">Danh sách tài khoản </a>
                                </li>
                                <li>
                                    <a href="admin/user/add">Thêm tài khoản</a>
                                </li>
                            </ul>
                        </li>
                       
                       
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
  </div>