
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Quản lý tiệm bánh gia đình ">
    <meta name="author" content="">
    <title>Quản lý Tiệm Bánh|@yield('title')</title>

    <base href="{{asset('')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.js"></script>
    <script src="{{asset('admin_asset/dist/js/notify.js')}}"></script>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('admin_asset/bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{asset('admin_asset/bower_components/metisMenu/dist/metisMenu.min.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{asset('admin_asset/dist/css/sb-admin-2.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{asset('admin_asset/bower_components/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!-- DataTables CSS -->
    <link href="{{asset('admin_asset/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="{{asset('admin_asset/bower_components/datatables-responsive/css/dataTables.responsive.css')}}" rel="stylesheet">
    <link href="{{asset('admin_asset/dist/css/alertify.css')}}" rel="stylesheet">
    <link href="{{asset('admin_asset/dist/css/alertify.min.css')}}" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <script src="https://code.jquery.com/jquery-3.5.0.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8">

</script>
 <script src="https://cdn.jsdelivr.net/npm/apexcharts" charset="utf-8">

</script>

    
     {{-- @if($chart)
    {!! $chart->script() !!}
    @endif --}}
</head>
    <style>
        th,td{
            font-size: 12px;
        }
    </style>
<body>
     @if (session('success'))
        <script>
            notify("<div style='font-size:15px'><i class='fa fa-check'></i> {{ session('success') }} </div>",'success');
        </script>
    @endif

    @if (session('danger'))
        <script>
            notify("<div style='font-size:15px'><i style='line-height: 20px;' class='fa ffa fa-exclamation-circle'><i/> {{ session('danger') }} </div>",'error');
        </script>
    @endif
    <div id="wrapper">
        
       @include('admin.layout.header')

       @yield('content')
       
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

    
    <script src="{{asset('admin_asset/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('admin_asset/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{asset('admin_asset/bower_components/metisMenu/dist/metisMenu.min.js')}}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{asset('admin_asset/dist/js/sb-admin-2.js')}}"></script>
     <script src="{{asset('admin_asset/dist/js/alertify.js')}}"></script>
      <script src="{{asset('admin_asset/dist/js/alertify.min.js')}}"></script>


    <!-- DataTables JavaScript -->
    <script src="{{asset('admin_asset/bower_components/DataTables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin_asset/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->


    <script>
        $(document).ready(function(){
            $('.delivery').click(function(){
                var _token = $('input[name="_token"]').val();
                var city = $('.city').val();
                var province = $('.province').val();
                var wards =  $('.wards').val();
                var fee_ship =  $('.fee_ship').val();

             $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ route('insert.delivery') }}",
                    method: "POST",
                    data:{city:city,province:province,wards:wards,fee_ship:fee_ship,_token:_token},
                    success:function(data){
                       alert('Thêm phí ship thành công');
                    }
                });

            });
          
            $('.choose').on('change',function(){
                var action = $(this).attr('id');
                var ma_id = $(this).val();
                var _token = $('input[name="_token"]').val();
                var $result = '';

                if (action == 'city') {
                    result = 'province';
                }else{
                    result = 'wards';
                }
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ route('select.delivery') }}",
                    method: "POST",
                    data:{action:action,ma_id:ma_id,_token:_token},
                    success:function(data){
                        $('#'+result).html(data);
                    }
                });
            })
        });
        
    </script>
    <script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
        var date = new Date();      
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

        $('#start_date').val(moment(firstDay).format('YYYY-MM-DD'));
        $('#end_date').val(moment(lastDay).format('YYYY-MM-DD'));
    });
     function searchBills(){
        var start = $('#start_date').val();
        var end = $('#end_date').val();
        if (start != "" && end != "") {
            $.ajax({
                 headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "{{ route('bill.count.date') }}",
                data: {
                    start: start,
                    end: end,
                },
            }).done(function (data) {
                $('#tbody_bill').html('');
                    $.each(data.bills, function (key,value) {
                        $('#tbody_bill').append(`<tr>
                            <td>00000${ value.id }</td>
                            <td>${ value.id_customer }</td> 
                            <td>${moment(value.date_order).format('DD-MM-YYYY') }</td>
                            <td>${ value.payment }</td>
                            <td>${ value.note }</td>
                            <td>${moment(value.updated_at).format('DD-MM-YYYY') }</td>
                            <td style="">${ value.total }<sup>đ</sup></td>
                        </tr>`)
                        });
                 $('#total').html(data.total);
            }).fail(function (Responsive){
                console.log(Responsive);
            })
        }
    }

     function searchBillsRefuse(){
        var start = $('#start_date').val();
        var end = $('#end_date').val();
        if (start != "" && end != "") {
            $.ajax({
                 headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "{{ route('bill.count.date.ad.refuse') }}",
                data: {
                    start: start,
                    end: end,
                },
            }).done(function (data) {
                $('#tbody_bill').html('');
                    $.each(data.bills, function (key,value) {
                        $('#tbody_bill').append(`<tr>
                            <td>00000${ value.id }</td>
                            <td>${ value.id_customer}</td>
                            <td>${moment(value.date_order).format('DD-MM-YYYY') }</td>
                            <td style="">${ value.total }<sup>đ</sup></td>
                            <td>${ value.payment }</td>
                            <td>${ value.note }</td>
                            <td>${moment(value.updated_at).format('DD-MM-YYYY') }</td>
                        </tr>`)
                        });
            }).fail(function (Responsive){
                console.log(Responsive);
            })
        }
    }
    </script>
   
    @yield('script')
    <script>
        CKEDITOR.replace( 'content' );
        $(window).on('load', function (){
        $( '#content' ).ckeditor();
    });

    </script>
    <script>
    CKEDITOR.replace( 'content', {
        filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
    });

    </script>

</body>

</html>
