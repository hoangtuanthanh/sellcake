 @extends('admin.layout.index')
  @section('title','Thông tin khách hàng');
 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Customer
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                             {{session('thongbao')}}
                        </div>
                    @endif

                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Name</th>                               
                                <th>Giới Tính </th>
                                <th>Email</th>
                                <th>Địa chỉ</th>                           
                                <th>Điện thoại</th>
                                {{--  <th>Note</th> --}}
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($customer as $ct)
                            <tr class="odd gradeX" align="center">
                                <td>{{$ct->id}}</td>
                                <td>{{$ct->name}}</td>
                                <td>{{$ct->gioitinh}}</td>
                                <td>{{$ct->email}}</td>
                                <td>{{$ct->address}}</td>
                                <td>{{$ct->phone}}</td>
                              {{--   <td>{!!$ct->note!!}</td> --}}
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/customer/delete/{{$ct->id}}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/customer/edit/{{$ct->id}}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection