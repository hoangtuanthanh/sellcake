@extends('admin.layout.index')
 @section('title','Sửa thông tin khách hàng');
 @section('content')
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"style=" margin: 56px 0 20px;">Customer
                            <small>Edit</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                     @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    @if(count($errors)> 0)
                        @foreach($errors as $err)
                            <div class="alert alert-danger">
                                {{$err}}<br>
                            </div>
                        @endforeach
                    @endif()
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="admin/customer/edit/{{$customer->id}}" method="POST">
                            @csrf()
                            <div class="form-group">
                                <label>Name</label>
                                 <input class="form-control" name="name" placeholder="Please Enter Name" value="{{$customer->name}}" />
                            </div>
                            <div class="form-group">
                                <label>Giới Tính</label>
                                <input class="form-control" name="gioitinh" placeholder="Please Enter Giới Tính"value="{{$customer->gioitinh}}" />
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input class="form-control" name="email" placeholder="Please Enter Email"value="{{$customer->email}}" />
                            </div>
                            <div class="form-group">
                                <label>Địa chỉ</label>
                                <input class="form-control" name="address" placeholder="Please Enter Address" value="{{$customer->address}}"/>
                            </div>
                            <div class="form-group">
                                <label>Điện thoại</label>
                               <input class="form-control" name="phone" placeholder="Please Enter Phone" value="{{$customer->phone}}"/>
                            </div>
                            <div class="form-group">
                                <label>Ghi chú</label>
                              <textarea name="note" class="form-control" id="content" rows="10" >{{$customer->note}}</textarea>
                            </div>
                            <button type="submit" class="btn btn-default">Customer Edit</button>
                            {{-- <button type="reset" class="btn btn-default">Reset</button> --}}
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection