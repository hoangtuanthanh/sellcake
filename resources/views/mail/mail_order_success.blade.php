@extends('mail.layout.index')
 @section('content_mail')
      <tr>
        <td align="center" valign="middle" style="background:#ffffff">
            <table style="width:580px" cellpadding="0" cellspacing="0" border="0">
                <tbody>
                    <tr>
                        <td align="left" valign="middle" style="font-family:Arial,Helvetica,sans-serif;font-size:24px;color:#ff3333;text-transform:uppercase;font-weight:bold;padding:25px 10px 15px 10px">
                            Thông báo đặt hàng thành công
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;padding:0 10px 20px 10px;line-height:17px">
                            Chào <b>{{ $bill->Customer->name }}</b>,
                            <br> Cám ơn bạn đã mua sắm tại Bakers Alley
                            <br>
                            <br> Đơn hàng của bạn đang sẽ được gửi đến trong vòng 24h tới,<br> Thời hạn sử dụng: <b>7 ngày kể từ ngày giao hàng</b> và phải bảo quản trong găn tủ lạnh có nhiệt độ từ 0 - 8 độ C.
                            <br>Chúc bạn ăn bánh ngon miệng 
                            {{-- <b>chờ shop</b>  
                            <b>xác nhận</b> (trong vòng 24h) --}}
                            {{-- <br> Chúng tôi sẽ thông tin <b>trạng thái đơn hàng</b> trong email tiếp theo. --}}
                            {{-- <br> Bạn vui lòng kiểm tra email thường xuyên nhé. --}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="middle" style="background:#ffffff">
            <table style="width:580px;border:1px solid #ff3333;border-top:3px solid #ff3333" cellpadding="0" cellspacing="0" border="0">
                <tbody>
                    <tr>
                        <td colspan="2" align="left" valign="top" style="font-family:Arial,Helvetica,sans-serif;font-size:14px;color:#666666;padding:10px 10px 20px 15px;line-height:17px"> 
                            <b>Đơn hàng của bạn #</b> 
                            <a href="#" style="color:#ed2324;font-weight:bold;text-decoration:none" target="_blank">0000{{ $bill->id }}
                            </a>
                            <span style="font-size:12px">({{ $bill->created_at }})</span>
                        </td>
                    </tr>
                    @foreach($bill_detail as $b)
                        <tr>
                            <td align="left" valign="top" style="width:120px;padding-left:15px">
                                <a href="#_" style="border:0"> 
                                    <img src="https://img2.thuthuatphanmem.vn/uploads/2018/12/06/be-tap-to-mau-banh-sinh-nhat_112150043.jpg" height="120" width="120" style="display:block;border:0px"> 
                                </a>
                            </td>
                            <td align="left" valign="top">
                                <table style="width:100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td align="left" valign="top" style="width:120px;font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;padding-left:15px;padding-right:10px;line-height:20px;padding-bottom:5px"> 
                                                <b>Sản phẩm</b>
                                            </td>
                                            <td align="left" valign="top" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;line-height:20px;padding-bottom:5px">:</td>
                                            <td align="left" valign="top" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;line-height:20px;padding-left:10px;padding-bottom:5px">
                                                 <a href="#" style="color:#115fff;text-decoration:none" target="_blank">
                                                   {{-- {{$b->Product['name']}} --}}
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" style="width:120px;font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;padding-left:15px;padding-right:10px;line-height:20px;padding-bottom:5px"> 
                                                <b>Tên Shop</b>
                                            </td>
                                            <td align="left" valign="top" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;line-height:20px;padding-bottom:5px">:</td>
                                            <td align="left" valign="top" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;line-height:20px;padding-left:10px;padding-bottom:5px"> 
                                                <a href="#" style="color:#115fff;text-decoration:none" target="_blank">
                                                    Bakers Alley
                                                </a>
                                                - 0967461697
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" style="width:120px;font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;line-height:20px;padding-left:15px;padding-right:10px;padding-bottom:5px"> 
                                                <b>Giá tiền: </b>
                                            </td>
                                            <td align="left" valign="top" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;line-height:20px;padding-bottom:5px">:</td>
                                            <td align="left" valign="top" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;line-height:20px;padding-left:10px;padding-bottom:5px">
                                                {{-- {{ number_format($b->unit_price) }} --}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" style="width:120px;font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;line-height:20px;padding-left:15px;padding-right:10px;padding-bottom:5px"> 
                                                <b>Người nhận</b>
                                            </td>
                                            <td align="left" valign="top" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;line-height:20px;padding-bottom:5px">:</td>
                                            <td align="left" valign="top" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;line-height:20px;padding-left:10px;padding-bottom:5px"> 
                                                <b>{{ $bill->Customer['name'] }}</b> - {{ $bill->Customer['phone'] }}
                                                <br>
                                                {{ $bill->Customer['address'] }}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    @endforeach
                     <tr>
                        <td align="left" valign="top" style="width:120px;font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;line-height:20px;padding-left:15px;padding-right:10px;padding-bottom:5px"> 
                            <b>Phí vận chuyển: </b>
                        </td>
                        <td align="left" valign="top" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;line-height:20px;padding-bottom:5px">:</td>
                        <td align="left" valign="top" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;line-height:20px;padding-left:10px;padding-bottom:5px">
                            {{ number_format(Session::get('fee')) }} <sup>đ</sup>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="width:120px;font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;line-height:20px;padding-left:15px;padding-right:10px;padding-bottom:5px"> 
                            <b>Tổng tiền: </b>
                        </td>
                        <td align="left" valign="top" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;line-height:20px;padding-bottom:5px">:</td>
                        <td align="left" valign="top" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#666666;line-height:20px;padding-left:10px;padding-bottom:5px">
                            @if(Session::has('cart') && Session::get('fee') )
                                            {{number_format( Session('cart')->totalPrice + Session::get('fee') ) }}
                                    @elseif(Session::has('cart') && !Session::get('fee'))
                                        {{number_format(Session('cart')->totalPrice) }}
                                    @endif <sup>đ</sup>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" valign="top" style="padding-top:20px;padding-bottom:20px;border-bottom:1px solid #ebebeb">
                            <a href="#" style="border:0px" target="_blank"> 
                               {{--  <img src="https://i.imgur.com/f92hL68.jpg" height="29" width="191" alt="Chi tiết đơn hàng" style="border:0px">  --}}
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
 @endsection