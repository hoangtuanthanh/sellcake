<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\News;
use Faker\Generator as Faker;

$factory->define(News::class, function (Faker $faker) {
	$name = $faker->sentence;
	$slug = Str::slug($name);
    return [
        'title'  => $faker->title,
        'content' => $faker->paragraph(3),
        'slug' =>  $slug,
        'image' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Good_Food_Display_-_NCI_Visuals_Online.jpg/1200px-Good_Food_Display_-_NCI_Visuals_Online.jpg'
 
    ];
});
