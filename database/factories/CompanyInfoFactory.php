<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\CompanyInfo;
use Faker\Generator as Faker;

$factory->define(CompanyInfo::class, function (Faker $faker) {
    return [
        'address' =>  $faker->address,
        'tax_number' =>  $faker->tax_number,
        'phone' =>  $faker->phone,
        'email'  =>  $faker->email,
        'hotline' =>  $faker->hotline,
        'website' =>  $faker->website
    ];
});
