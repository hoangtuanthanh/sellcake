<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Contact;
use Faker\Generator as Faker;

$factory->define(Contact::class, function (Faker $faker) {
	
    return [
        'name'  =>  $faker->name,
        'email'=>  $faker->email,
        'address' => $faker->address,
        'phone' =>  $faker->phoneNumber,
        'content'  => $faker->paragraph(3),
        'status' =>  $status
    ];
});
