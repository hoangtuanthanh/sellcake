<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(VoucherSeeder::class);
        $this->call(userSeeder::class);
        $this->call(Seeder1::class);

    }
}
// class billdetail extends seeder
// {
// 	public function run()
// 	{
// 		DB::table('bill_detail')->insert([
// 			// ['id_bill'=>'1','id_product'=>'Bánh Trứng muối','quantity'=>'20','unit_price'=>'400'],
// 			// ['id_bill'=>'2','id_product'=>'Bánh ngọt','quantity'=>'20','unit_price'=>'40']
// 		]);
// 	}
// }
	/**
	 * 
	 */
	class userSeeder extends Seeder
	{
		public function run(){
			DB::table('users')->delete();
			DB::table('users')->insert([
				['name'=>'admin1','email'=>'admin1@gmail.com','phone'=>'0987654321','address'=>'lientrung','password'=>bcrypt('admin1'),'role'=>1,'lock'=>1],
				['name'=>'admin2','email'=>'admin2@gmail.com','phone'=>'0987654331','address'=>'lientrung','password'=>bcrypt('admin2'),'role'=>1,'lock'=>1],
				['name'=>'user1','email'=>'user1@gmail.com','phone'=>'0987655421','address'=>'lientrung','password'=>bcrypt('user1'),'role'=>0,'lock'=>1],
				['name'=>'user2','email'=>'user2@gmail.com','phone'=>'0987664321','address'=>'lientrung','password'=>bcrypt('user2'),'role'=>0,'lock'=>1],
				['name'=>'nhanvien1','email'=>'nhanvien1@gmail.com','phone'=>'0987655421','address'=>'lientrung','password'=>bcrypt('nhanvien1'),'role'=>2,'lock'=>1],
				['name'=>'nhanvien2','email'=>'nhanvien2@gmail.com','phone'=>'0987664321','address'=>'lientrung','password'=>bcrypt('nhanvien2'),'role'=>2,'lock'=>1]
			]);
		}
	}
	class Seeder1 extends Seeder
	{
		public function run(){
			DB::table('companyinfo')->delete();
			DB::table('companyinfo')->insert([
				['address'=>'Liên Trung - Đan Phượng - Hà Nội','tax_number'=>'3242781','phone'=>'0967461697','email'=>'Hoangtuanthanh19061997@gmail.com','hotline'=>19001009,'website'=>'Hoangtuanthanh.com'],
			]);
		}
	}
	class VoucherSeeder extends Seeder
	{
	  public function run()
	    {	
	    	DB::table('voucher')->delete();
	        DB::table('voucher')->insert([
	        	'code' => 'ABC123',
	        	'name' => 'Voucher',
	        	'description' => 'loại mã giảm giá ',
	        	'max_uses_user' => '5',
	        	'used_user' => '2',
	        	'discount_amount' => '20000',
	        	'starts_at' => '2020-08-18',
	        	'expires_at' => '2020-09-19',
	        ]);
	      DB::table('voucher')->insert([
	        	'code' => '123ABC',
	        	'name' => 'Voucher',
	        	'description' => 'loại mã giảm giá ',
	        	'max_uses_user' => '1',
	        	'used_user' => '0',
	        	'discount_amount' => '50000',
	        	'starts_at' => '2020-08-19',
	        	'expires_at' => '2020-09-10',
	        ]);
	    }
	}
