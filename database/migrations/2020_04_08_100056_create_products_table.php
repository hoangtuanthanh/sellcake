<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('id_type');
            $table->foreign('id_type')->references('id')->on('type_products')->onDelete('cascade');
            $table->text('desc');
            $table->float('unit_price');
            $table->float('promotion_pice')->default(0);
            $table->string('image');
            $table->string('unit');
            $table->string('new');
            $table->string('slug');

            $table->integer('soluong')->default(0);
            $table->integer('daban')->default(0);
            $table->string('size');
            $table->date('ngaysx');
            $table->date('hansd');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
