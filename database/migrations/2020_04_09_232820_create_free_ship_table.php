<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreeShipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('free_ship', function (Blueprint $table) {
           $table->bigIncrements('id');
            $table->unsignedBigInteger('matp');
            $table->foreign('matp')->references('matp')->on('city')->onDelete('cascade');
            $table->unsignedBigInteger('maqh');
            $table->foreign('maqh')->references('maqh')->on('province')->onDelete('cascade');
            $table->unsignedBigInteger('xaid');
            $table->foreign('xaid')->references('xaid')->on('ward')->onDelete('cascade');
            $table->string('feeship');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('free_ship');
    }
}

 