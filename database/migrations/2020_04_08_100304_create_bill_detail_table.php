<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_detail', function (Blueprint $table) {
           $table->bigIncrements('id');
            $table->unsignedBigInteger('id_bill');
            $table->foreign('id_bill')->references('id')->on('bills')->onDelete('cascade');
            $table->unsignedBigInteger('id_product');
            $table->foreign('id_product')->references('id')->on('products')->onDelete('cascade');
            $table->integer('quantity');
            $table->float('unit_price');
            $table->timestamps();
        });
    }
            // $table->unsignedBigInteger('id_bill');
            // $table->foreign('id_bill')
            //     ->references('id')
            //     ->on('bills')
            //     ->onDelete('cascade');
            // $table->unsignedBigInteger('id_product');
            // $table->foreign('id_product')
            //     ->references('id')
            //     ->on('products')
            //     ->onDelete('cascade');
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_detail');
    }
}
