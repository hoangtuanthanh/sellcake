<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/
//---Xuất Excel-------/

   
//---/Xuất Excel-------/

//home
//
route::get('/','PageController@getIndex')->name('index');
route::get('loaisp/{id}','PageController@getLoaisp')->name('loaisp');
route::get('chitietsp/{id}','PageController@getChitiet')->name('chitietsp');
route::get('themgiohang/{id}','PageController@getThemGioHang')->name('themgiohang'); //gio hàng
route::get('delete-cart/{id}','PageController@getDelCart')->name('delete-cart');
// Route::get('update/{rowId}/{qty}','PageController@getUpdateCart');
route::get('shopping-cart','PageController@getShopCart')->name('shopping-cart');

//danh sach đơn đặt hàng dashboarch
route::get('list-bill','LoginController@getlistBill')->name('list.bill');


//giới thiệu
route::get('gioithieu','PageController@getGioithieu')->name('gioithieu');

//lấy tất cả sản phẩm
route::get('all-pro','PageController@getAllProduct')->name('getAllProduct');
route::get('cate-pro','PageController@getAllCateProduct')->name('getAllCateProduct');
//filter
route::get('up-pro','PageController@upPro')->name('upPro');
route::get('down-pro','PageController@downPro')->name('downPro');
route::get('price-pro-0','PageController@pricePro0')->name('pricePro0');
route::get('price-pro-1','PageController@pricePro1')->name('pricePro1');
route::get('price-pro-2','PageController@pricePro2')->name('pricePro2');
route::get('price-pro-3','PageController@pricePro3')->name('pricePro3');
route::get('sizeM','PageController@sizeM')->name('sizeM');
route::get('sizeS','PageController@sizeS')->name('sizeS');
route::get('sizeL','PageController@sizeL')->name('sizeL');
//đặt hàng
route::get('dat-hang','PageController@getDatHang')->name('dat-hang');
route::post('dat-hang','PageController@postDatHang');
 Route::post('select-delivery-home','PageController@selectDeliveryHome')->name('select.delivery.home');
 Route::post('calculate-fee','PageController@CalculateFee')->name('calculate.fee');

//giảm giá
Route::post('voucher','VoucherController@postVoucher')->name('postVoucher');
Route::post('voucher-del','VoucherController@delVoucher')->name('delVoucher');

 //loc size
 // Route::post('filterSize','PageController@filterSize')->name('filterSize');
 

//lien hệ
route::get('lienhe','PageController@getLienhe')->name('lienhe');
route::post('lienhe-post','PageController@postLienhe')->name('post.lienhe');

route::get('gioithieu','PageController@getGioithieu')->name('gioithieu');

//subcriber
route::get('subcriber','PageController@getSub');
route::post('subcriber','PageController@postSub')->name('subcriber');

//search
route::get('search','PageController@getSearch')->name('get.search');
//home //-------------

//----------Imployeed
Route::group(['prefix'=>'imployeed'],function(){

   // route::get('header','LoginController@getIndex');
   route::get('index-imployeed','Imployeed\ImployeedController@getIndexImployeed')->name('index.imployeed');
   route::get('list-bill','Imployeed\ImployeedController@getListImployeed')->name('get.list.bill.im');
   route::get('success-bill','Imployeed\ImployeedController@getSuccessImployeed')->name('get.success.bill.im');
   route::get('refuse-bill','Imployeed\ImployeedController@getRefuseImployeed')->name('get.refuse.bill.im');

   route::get('ship-bill-waiting/{id}','Imployeed\ImployeedController@getShipWaiting')->name('get.status.ship1');
   route::get('ship-bill-except/{id}','Imployeed\ImployeedController@getShipExcept')->name('get.status.ship2');
   route::get('ship-bill-shipping/{id}','Imployeed\ImployeedController@getShipShipping')->name('get.status.ship3');
   route::get('ship-bill-success/{id}','Imployeed\ImployeedController@getShipSuccess')->name('get.status.ship4');
   route::get('ship-bill-refuse/{id}','Imployeed\ImployeedController@getShipRefuse')->name('get.status.ship5');

   Route::get('edit/{id}','Imployeed\ImployeedController@getEditIm')->name('get.edit.im');
   Route::post('edit/{id}','Imployeed\ImployeedController@postEdit'); //{id}: biết muốn sửa thể loại nào

    //lọc đơn hàng theo trạng thái
   Route::get('orderFilter/{id}','Imployeed\ImployeedController@getLocDonHang')->name('loc.don.hang');
   //Kieemr tra don hang theo ngay
    route::post('processed/bill-filter-date','Imployeed\ImployeedController@postBillCountDateIm')->name('bill.count.date.Im');
    route::post('processed/bill-filter-date-refuse','Imployeed\ImployeedController@postBillCountDateRefuseIm')->name('bill.count.date.Im.Refuse');
   

    //
    Route::get('listBillDetailIm/{id}','Imployeed\ImployeedController@getListDetailIm')->name('product.detail.im');
});
//----------Imployeed--------//

//login---------user
// route::get('login-user','Frontend\LoginController@getLogin')->middleware('CheckUser');
// route::post('login-user','Frontend\LoginController@postLogin')->name('login');
route::get('logout','Frontend\LoginController@getLogout')->name('logout');
// route::get('logout','LoginController@getLogout');
route::get('register','Frontend\LoginController@getRegister')->name('register.get');
route::post('register','Frontend\LoginController@postRegister')->name('register.post');
//login---------user ----------   //
Route::post('ckeditor/image_upload', 'CKEditorController@upload')->name('upload');

// payment
Route::get('payment', 'PaymentController@index');
Route::post('charge', 'PaymentController@charge')->name('charge');
Route::get('paymentsuccess', 'PaymentController@payment_success');
Route::get('paymenterror', 'PaymentController@payment_error');

//login---------admin
route::get('login','LoginController@getLoginAdmin')->name('login-admin-get')->middleware('CheckLogout');
// route::get('login-im','LoginController@getLoginAdmin')->name('login-admin-get')->middleware('ChckeLogout');
route::post('login','LoginController@postLoginAdmin')->name('login-admin-post');

//login---------admin -------------//

//admin----------------
Route::group(['prefix'=>'admin','middleware'=>'CheckUser'],function(){

   // route::get('header','LoginController@getIndex');
   route::get('dash','LoginController@getDash')->name('dash');
   Route::get('orderFilter_bill_dash/{id}','LoginController@getLocDonHangBill')->name('loc.don.han.bill.dash');

   route::get('date.dash','LoginController@getDateDash')->name('get.date.dash');

   //list bill dash
   route::get('list-bill-dash','LoginController@getlistBill')->name('list.bill.dash');
   
   //loc don hang theo ngay thang
   route::post('fetch_data','LoginController@fetch_data')->name('daterange.fetch_data');

      //bill
   Route::group(['prefix'=>'bill'],function(){
      Route::get('list','BillController@getList')->name('list.bill');
      Route::get('add','BillController@getAdd');
      route::post('add','BillController@postAdd');
      route::get('xuly/{id}','BillController@getXuly')->name('get.xu.ly');
      route::get('processed','BillController@getProcessed')->name('list.bill.processed');
      route::get('refuse','BillController@getRefuseAD')->name('list.bill.refuse');
      route::get('delete/{id}','BillController@getDelete')->name('get.delete.bill');
      Route::get('listBillDetail/{id}','BillController@getListDetail')->name('product.detail');

      //tìm kiếm theo ngày
      route::post('processed/bill_date','BillController@postBillCountDate')->name('bill.count.date');
      //refuse
      route::post('processed/bill_date_ad','BillController@postBillCountDateAD')->name('bill.count.date.ad.refuse');
      
      
      //
      route::post('processed/bill_date_count','BillController@postBillCountDatechange')->name('bill.count.date.change');
      //lọc đơn hàng theo trạng thái
       Route::get('orderFilter_bill/{id}','BillController@getLocDonHangBill')->name('loc.don.han.bill');

       //Xuaat Excel
       Route::post('/dowload-excel','BillController@getDowloadExcel')->name('dowload.excel');
       //store Excel
       Route::get('/store-excel','BillController@getStoreExcel')->name('store.excel');


     
   });
      //billdetial
   Route::group(['prefix'=>'billdetail'],function(){
      Route::get('list','BillDetailController@getList');
      Route::get('add','BillDetailController@getAdd');
      route::post('add','BillDetailController@postAdd');
      Route::get('edit/{id}','BillDetailController@getEdit');
      Route::post('edit/{id}','BillDetailController@postEdit'); //{id}: biết muốn sửa thể loại nào
      route::get('delete/{id}','BillDetailController@getDel');
   });
      //customer
   Route::group(['prefix'=>'customer'],function(){
      Route::get('list','CustomerController@getList')->name('list.customer');
      Route::get('add','CustomerController@getAdd');
      route::post('add','CustomerController@postAdd');
      Route::get('edit/{id}','CustomerController@getEdit');
      Route::post('edit/{id}','CustomerController@postEdit'); //{id}: biết muốn sửa thể loại nào
      route::get('delete/{id}','CustomerController@getDel');
   });
    //news
   Route::group(['prefix'=>'news'],function(){
      Route::get('list','NewsController@getList')->name('list.news');
      Route::get('add','NewsController@getAdd');
      route::post('add','NewsController@postAdd');
      Route::get('edit/{id}','NewsController@getEdit');
      Route::post('edit/{id}','NewsController@postEdit'); //{id}: biết muốn sửa thể loại nào
      route::get('delete/{id}','NewsController@getDel');
   });
     //product
   Route::group(['prefix'=>'product'],function(){
      Route::get('list','ProductController@getList')->name('list.product');
      Route::get('add','ProductController@getAdd');
      route::post('add','ProductController@postAdd');
      Route::get('edit/{id}','ProductController@getEdit');
      Route::post('edit/{id}','ProductController@postEdit'); //{id}: biết muốn sửa thể loại nào
      route::get('delete/{id}','ProductController@getDel');
      route::get('warehouse','ProductController@warehouse')->name('warehouse');
   });
      //slide
   Route::group(['prefix'=>'slide'],function(){
      Route::get('list','SlideController@getList');
      Route::get('add','SlideController@getAdd');
      route::post('add','SlideController@postAdd');
      Route::get('edit/{id}','SlideController@getEdit');
      Route::post('edit/{id}','SlideController@postEdit'); //{id}: biết muốn sửa thể loại nào
      route::get('delete/{id}','SlideController@getDel');
   });
      //type product
   Route::group(['prefix'=>'typeproduct'],function(){
      Route::get('list','TypeProductController@getList')->name('list.typeproduct');
      Route::get('add','TypeProductController@getAdd');
      route::post('add','TypeProductController@postAdd');
      Route::get('edit/{id}','TypeProductController@getEdit');
      Route::post('edit/{id}','TypeProductController@postEdit'); //{id}: biết muốn sửa thể loại nào
      route::get('delete/{id}','TypeProductController@getDel');
   });
       //user
   Route::group(['prefix'=>'user'],function(){
      Route::get('list','UserController@getList');
      Route::get('add','UserController@getAdd');
      route::post('add','UserController@postAdd');
      Route::get('edit/{id}','UserController@getEdit');
      Route::post('edit/{id}','UserController@postEdit'); //{id}: biết muốn sửa thể loại nào
      route::get('delete/{id}','UserController@getDel');
      //Khóa- mỏ khóa tài khoản
      route::get('getLockUser/{id}','UserController@getLockUser')->name('get.lock.user');
      route::get('getUnlockUser/{id}','UserController@getUnlockUser')->name('get.unlock.user');
   });
   Route::group(['prefix'=>'about'],function(){
      Route::get('list','AboutController@getList');
      Route::get('add','AboutController@getAdd');
      route::post('add','AboutController@postAdd');
      Route::get('edit/{id}','AboutController@getEdit');
      Route::post('edit/{id}','AboutController@postEdit'); //{id}: biết muốn sửa thể loại nào
      route::get('delete/{id}','AboutController@getDel');
   });
      //contact
   Route::group(['prefix'=>'contact'],function(){
      Route::get('list','ContactController@getList')->name('list.contact');
      Route::get('get.xu.ly/{id}','ContactController@getXuly')->name('get.xu.ly.contact');
      Route::get('list-processed','ContactController@getProcessed')->name('list.processed');

   });
      //infor
   Route::group(['prefix'=>'companyinfo'],function(){
      Route::get('list','CompanyInforController@getList');
      Route::get('edit/{id}','CompanyInforController@getEdit');
      Route::post('edit/{id}','CompanyInforController@postEdit');
      route::get('delete/{id}','CompanyInforController@getDel');
   });

    Route::group(['prefix'=>'subcriber'],function(){
      Route::get('list','SubController@getList');
   });
    Route::group(['prefix'=>'delivery'],function(){
      Route::get('list','DeliveryController@getList')->name('list.deli');
      Route::get('add','DeliveryController@getAdd')->name('add.deli');
      Route::post('select-delivery','DeliveryController@selectDelivery')->name('select.delivery');
      Route::post('insert-delivery','DeliveryController@insertDelivery')->name('insert.delivery');
   });
    Route::group(['prefix'=>'voucher'],function(){
      Route::get('list','VoucherController@getList')->name('list.voucher');
      Route::get('add','VoucherController@getAdd');
      route::post('add','VoucherController@postAdd')->name('add.voucher');
      Route::get('edit/{id}','VoucherController@getEdit');
      Route::post('edit/{id}','VoucherController@postEdit')->name('edit.voucher'); //{id}: biết muốn sửa thể loại nào
      route::get('delete/{id}','VoucherController@getDel')->name('delete.voucher');
   });
});
