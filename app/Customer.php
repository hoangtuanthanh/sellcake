<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = "customer";
    public function Bill(){
    	return $this->hasMany('App\Bill','id_customer','id');
    }
    public function BillDetail(){
    	return $this->hasManyThrough('App\BillDetail','App\Bill','id_customer','id_bill','id');
    }
}
