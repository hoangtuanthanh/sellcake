<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
     protected $table = "bills";
     public function BillDetail(){
     	return $this->hasMany('App\BillDetail','id_bill','id');
     }
     public function Customer(){
     	return $this->belongsTo('App\Customer','id_customer','id');
     }
      public function Fee_ship(){
        return $this->belongsTo('App\Feeship','id_bill','id');
     }
     public function Product(){
    	return $this->hasManyThrough('App\Product','App\BillDetail','id_type','id_product','id');
    }
    // public function Feeship(){
    //     return $this->belongsTo('App\Feeship','id_type','id_product','id');
    // }
}
