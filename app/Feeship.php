<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feeship extends Model
{
    protected $table = 'free_ship';
    public function City(){
    	return $this->belongsTo('App\City','matp','matp');
    }
     public function Province(){
    	return $this->belongsTo('App\Province','maqh','maqh');
    }
     public function Wards(){
    	return $this->belongsTo('App\Wards','xaid','xaid');
    }
     public function Bill(){
        return $this->belongsTo('App\Bill','id_bill','id');
     }
}
