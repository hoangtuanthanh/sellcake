<?php

namespace App;
use App\BillDetail;
use Illuminate\Database\Eloquent\Model;

class BillDetail extends Model
{
    protected $table = "bill_detail";
    public function Bill(){
    	return $this->belongsTo('App\Bill','id_bill','id');
    }
    public function Product(){
    	return $this->belongsTo('App\Product','id_product','id');
    }
    public function TypeProduct(){
    	return $this->hasManyThrough('App\TypeProduct','App\Product','id_product','id_type','id');
    }

    

    // public static function billDetailCount($id_product){
    //     $billDetailCount = BillDetail::where(['id_product'=>$id_product])
    //                     ->join('products','bill_detail.id_product','products.id')
    //                     ->join('bills','bill_detail.id_bill','bills.id')
    //                     // ->where('ngaysx', '<', 'hansd')
    //                     ->whereIn('status',[1,2,3,5])
    //                     ->count();
    //     return $billDetailCount;
    // }
}
