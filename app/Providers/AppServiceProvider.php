<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;
use App\TypeProduct;
use App\Cart;
use App\CompanyInfo;
use App\News;
use App\Subcriber;
use App\Customer;
use Session;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() //truyeenf thoong tin 
    {
        // đổ loại sản phẩm
        view()->composer('layout/header',function($view){
            $loai_sp = TypeProduct::all();
            
            $view->with('loai_sp',$loai_sp);
        });
        view()->composer('layout/footer',function($view){
            $Info = CompanyInfo::all();
            $view->with('Info',$Info);
        });
        view()->composer('layout/footer',function($view){
            $tintuc = News::orderBy('id','desc')->limit(3)->get();
            $view->with('tintuc',$tintuc);
        });
        view()->composer('layout/footer',function($view){
            $sub = new Subcriber;
            $view->with(['sub','email'=>$sub->email]);
        });
          view()->composer('admin/layout/menu',function($view){
            $new_count =News::count();
            $customer_count =Customer::count();
            $sub_count =Subcriber::count();
                
            $view->with('new_count',$new_count);
            $view->with('customer_count',$customer_count);
            $view->with('sub_count',$sub_count);
        });
       
        //thêm giỏ hàng
        view()->composer(['layout/header','page.checkout'],function($view){
            if(Session('cart')){
            $oldCart =Session::get('cart');//laay gio hang cua minh va gan vao gio cu~
            $cart = new Cart($oldCart);
            $view->with(['cart'=>Session::get('cart'),'product_cart'=>$cart->items,'totalPrice'=>$cart->totalPrice,'totalQty'=>$cart->totalQty]);
        }
        

    });
    }
}
