<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Auth;
class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if(Auth::check())
        {
         $user = Auth::user();
          if($user->role == 1)
            return $next($request);
          elseif($user->role == 0)
            return redirect(route('index'));
          elseif($user->role == 2)
            return redirect(route('get.list.bill.im'));
          else{
            return redirect(route('login-admin-get'));
          }
        }
       else
       {
         return redirect(route('login-admin-get'));
       }
    }
      // if(Auth::check())
      //     {
      //         if(Auth::user()->role == 0)
      //         {
      //             return redirect(route('index'));
      //         }
      //         elseif(Auth::user()->role == 1)
      //         {
      //             return redirect(route('dash'));
      //         }
              
      //         elseif(Auth::user()->role == 2)
      //         {
      //             return redirect(route('index.imployeed'));
      //         } 
      //     }
      //     else
      //     {
      //         return $next($request);
      //     }
      // }
}
