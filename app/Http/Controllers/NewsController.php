<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NewsRequest;
use Illuminate\Support\Str;
use App\News;

class NewsController extends Controller
{
    public function getList(){
        $new = News::all();
    	return view('admin.news.list',['new'=>$new]);
    }
    public function getAdd(){
    	return view('admin.news.add');
    }
    public function postAdd(Request $req){
    $this->validate($req,
        [
            'title'=>'required|min:3|max:50',
            'content'=>'required',
            'link'=>'required',
            'image'=>'required'
        ],
        [
            'title.required'=>'title không được trống !!!',
            'content.required'=>'content không được trống !!!',
            'image.required'=>'image không được trống !!!',
            'link.required'=>'link không được trống !!!',
        ]);
        $news = new News;
        $news->title = $req->title ;
        $news->content = $req->content ;
        $news->link = $req->link ;
        if ($req->hasFile('image')) {
             $file = $req->file('image');
             $duoi = $file->getClientOriginalExtension();
             if ($duoi !='jpg'&&$duoi !='png'&&$duoi !='jpeg' &&$duoi !='gif') {
                return redirect('admin/news/add')->with('danger','không đúng định dạng');
             }
             $name = $file->getClientOriginalName();
             $image = $name;
             while(file_exists("frontend/news".$image)){
                $image = $name;
             }
             $file->move("frontend/news",$image);
             $news->image = $image;
        }
        $news->slug  = Str::slug($req->title,'-');
        $news->save();
        return redirect('admin/news/add/')->with('success','Bạn thêm thành công');
    }
    public function getEdit($id){
        $news = News::find($id);
        return view('admin.news.edit',['news'=>$news]);
    }
    public function postEdit(Request $req, $id){
         $this->validate($req,
        [
            'title'=>'required|min:3|max:50',
            'content'=>'required',
            'link'=>'required',
            'image'=>'required'
        ],
        [
            'title.required'=>'title không được trống !!!',
            'content.required'=>'content không được trống !!!',
            'image.required'=>'image không được trống !!!',
            'link.required'=>'link không được trống !!!',
        ]);

        $news = News::find($id);
        $news->title = $req->title;
        $news->content   = $req->content;
        $news->link   = $req->link;
        if ($req->hasFile('image')) {
             $file = $req->file('image');
             $duoi = $file->getClientOriginalExtension();
             if ($duoi !='jpg'&&$duoi !='png'&&$duoi !='jpeg' &&$duoi !='gif') {
                return redirect('admin/news/edit/'.$id)->with('danger','không đúng định dạng');
             }
             $name = $file->getClientOriginalName();
             $image = $name;
             while(file_exists("frontend/news/".$image)){
                $image = $name;
             }
             $file->move("frontend/news/",$image);
             $news->image=$image;
        }
        $news->slug  = Str::slug($req->title,'-');
        $news->save();
        return redirect('admin/news/edit/'.$id)->with('success','Bạn sửa thành công');
    }
    public function getDel($id)
       {
          $news = News::find($id);
          $news->delete();

          return redirect('admin/news/list')->with('success','Bạn đã xóa thành công.');
       }
}
