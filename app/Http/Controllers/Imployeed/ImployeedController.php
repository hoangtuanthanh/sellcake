<?php

namespace App\Http\Controllers\Imployeed;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bill;
use App\Customer;
use App\Product;
use App\BillDetail;
use DB;
use App\User;
use Auth;

class ImployeedController extends Controller
{
    public function getIndexImployeed(){
    	return view('imployeed.layout.index');
    }
    public function getListImployeed(){

        $bill = Bill::whereBetween('status',[1,3])
                ->get();
    	return view('imployeed.bill.list',compact('bill'));
    }
      public function getSuccessImployeed(){
    	// lấy tháng hiện tại và lọc danh sách theo tháng
        $month = date('m');
        $data['bills'] = Bill::where('status', 4)->whereMonth('created_at', $month)->orderBy('id','desc')->get();
        return view('imployeed.bill.success',$data);
    }
    public function getRefuseImployeed(){
    	 $month = date('m');
        $data['bill'] = Bill::where('status', 5)->whereMonth('created_at', $month)->orderBy('id','desc')->get();
        return view('imployeed.bill.refuse',$data); 
        
    }
    public function getShipExcept($id){
    	$except = Bill::find($id);
        $except->status = 2;
        $except->save();
        return redirect()->back()->with('success','Xác nhận giao hàng');
    }
    public function getShipShipping($id){
    	$except = Bill::find($id);
        $except->status = 3;
        $except->save();
        return redirect()->back()->with('success','Đang giao hàng');
    }
      public function getShipSuccess($id){
    	$except = Bill::find($id);
      $except->status = 4;
      $except->save();
        return redirect()->back()->with('success','Giao hàng thành công');
        
    }
    public function getShipRefuse($id){
    	$except = Bill::find($id);
      $bill_detail = BillDetail::find($except->id);
      // dd($bill_detail->id_product);
      $product = Product::find($bill_detail->id_product);
      if($bill_detail->id_product == $product->id){
        if($except->status = 5){
          $product->daban -= $bill_detail->quantity;
          // tính lợi nhuận từng sản phẩm
          if($product->promotion_pice != 0){
              $product->loinhuan = ($product->promotion_pice - $product->gianhap)* $product->daban;
            }else{
              $product->loinhuan = ($product->unit_price - $product->gianhap)* $product->daban;
            }
          $product->save();
          $bill_detail->save();
        }
      }
      $except->save();
        return redirect()->back()->with('danger','Từ chối nhận hàng');
    }

    public function getEditIm($id){
        $user = User::find($id);
        // dd($user);
        return view('imployeed.user.edit',['user'=>$user]);
    }
    public function postEdit(Request $req, $id){
        $user = User::find($id);
        $user->name = $req->name;
        $user->email  = $req->email;
        $user->role = 2;
        if($req->check == "on")
         {
            $this->validate($req,
            [
               'password' => 'required|min:3|max:32',
               'passwordAgain' =>'required|same:password'
            ],
            [
               'password.required' => 'Bạn chưa nhập mật khẩu',
               'password.min' => 'Mật khẩu phải chứa ít nhất 3 ký tự',
               'password.max' => 'Mật khẩu chỉ được tối đa 32 ký tự',
               'passwordAgain.required' => 'Bạn chưa nhập lại mật khẩu',
               'passwordAgain.same' => 'Mật khẩu nhập lại chưa đúng'
            ]);
          }
        $user->password =bcrypt($req->password);
        $user->save();
        return redirect('imployeed/edit/'.$id)->with('success','Bạn sửa thành công');
    }
    //Lọc đơn hàng theo trạng thais
      public function getLocDonHang($id){
       $bill = Bill::where('status',$id)->get()->toArray();
       $stt = 1;
       foreach($bill as $val)
       {
        ?>
        <tr>
            <td><?php echo '00000'.$val['id']; ?></td>
            <td><?php echo $val['id_customer']; ?></td>
            <td><?php echo $val['date_order']; ?></td>
            <td><?php echo number_format($val['total']); ?><sup>đ</sup></td>
            <td><?php if($val['fee_ship'] != 0){  ?>
              <span style=""><?php echo number_format($val['fee_ship']);?><sup>đ</sup></span>
              <?php } else{  ?>
              <span style="">10,000 <sup>đ</sup> </span>
            <?php } ?>
            </td>
            <td><?php echo $val['payment']; ?></td>
            <td><?php echo $val['note']; ?></td>
            <td> <a href="<?php echo route('product.detail.im',['id' => $val['id']]); ?>">Chi tiết</a></td>
            <td><?php if($val['status'] ==1){  ?>
                 <span style="color: blue">Chờ giao hàng</span>
                 <?php } if($val['status'] ==2){  ?>
                 <span style="color: blue">Xác nhận giao hàng</span>
                 <?php } if($val['status'] ==3){  ?>
                 <span style="color: blue">Đang giao hàng</span>
             <?php } ?>
             </td>
            <td><?php if($val['status'] ==1){  ?>
                   <a href="<?php echo route('get.status.ship2',['id' =>$val['id']]) ?>" class="btn btn-sm btn-link"><i class="fa fa-rocket fa-fw"></i> <span style="color: blue">Xác nhận giao hàng</span></a>
                   <?php } if($val['status'] ==2){  ?>
                  <a href="<?php echo route('get.status.ship3',['id' =>$val['id']]) ?>" class="btn btn-sm btn-link"><i class="fa fa-rocket fa-fw"></i><span style="color: blue">Đang giao hàng</span></a>
                 <?php } else { ?>
                    <a href="<?php echo route('get.status.ship4',['id' =>$val['id']]) ?>" class="btn btn-sm btn-link"><i class="fa fa-rocket fa-fw"></i> <span style="color: green">Giao hàng thành công</span></a>
                    <a href="<?php echo route('get.status.ship5',['id' =>$val['id']]) ?>" class="btn btn-sm btn-link"><i class="fa fa-rocket fa-fw"></i><span style="color: red">Từ chối nhận hàng</span></a>
                     <?php } ?>
             </td>
        </tr>
        <?php $stt++;
        }
    }
    //lọc đơn hàng theo thời gian
    public function postBillCountDateIm(Request $req){
        $start = $req->start."";
        $end = $req->end."";
        $bills = Bill::where('status',4)
                      // ->where('id_customer',Auth::user()->id)
                      ->whereDate('date_order','>=',$start)
                      ->whereDate('date_order','<=',$end)
                      ->get();
        return response()->json([
            'bills' => $bills,
        ]);
    }
     public function postBillCountDateRefuseIm(Request $req){
        $start = $req->start."";
        $end = $req->end."";
        $bills = Bill::where('status',5)
                      ->whereDate('date_order','>=',$start)
                      ->whereDate('date_order','<=',$end)
                      ->get();
        return response()->json([
            'bills' => $bills,
        ]);
    }

    public function getListDetailIm($id){
    $bill_detail = BillDetail::join('bills','bills.id','=','bill_detail.id_bill')
            ->where('bill_detail.id_bill','=',$id)
            ->get();
     // $bill_detail = Bill::all();
      return view('imployeed.bill.listBillDetail',compact('bill_detail'));
  }


}   
