<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Contact;

class ContactController extends Controller
{
     public function getList(){
    	$contact = Contact::where('status', 1)->get();
    	return view('admin.contact.list',['contact'=>$contact]);
    }
    //danh sách liên hệ đã xử lý
    public function getProcessed(){
        $contact = Contact::where('status', 2)->orderBy('created_at','asc')->get();

        return view('admin.contact.processed',['contact'=>$contact]);
    }

    public function getXuly(request $request,  $id){
        $contact = Contact::find($id);
        $contact->status = 2;
        $contact->save();
        return redirect()->back()->with('success','Đã xử lý liên hệ');
    }
}
