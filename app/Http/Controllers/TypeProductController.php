<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Type_ProductRequest;
use Illuminate\Support\Str;
use App\TypeProduct;

class TypeProductController extends Controller
{
     public function getList(){
        $type_pro = TypeProduct::all();
        return view('admin.typeproduct.list',['type_pro'=>$type_pro]);
    }
     public function getAdd(){
        return view('admin.typeproduct.add');
    }
    public function postAdd(Request $req){
         $this->validate($req,
        [
          'image' =>'required',
          'desc' =>'required',
          'name'=>'required'
        ],
        [
          'image.required'=>'image không được trống !!!',
          'desc.required'=>'desc không được trống !!!',
          'name.required'=>'name không được trống !!!'
        ]);
        $type_pro = new TypeProduct;
        $type_pro->name = $req->name;
        $type_pro->desc  = $req->desc ;
           if ($req->hasFile('image')) {
             $file = $req->file('image');
             $duoi = $file->getClientOriginalExtension();
             if ($duoi !='jpg'&&$duoi !='png'&&$duoi !='jpeg' &&$duoi !='gif') {
                return redirect('admin/typeproduct/add')->with('danger','không đúng định dạng');
             }
             $name = $file->getClientOriginalName();
             $image = $name;
             while(file_exists("frontend/typeproduct".$image)){
                $image = $name;
             }
             $file->move("frontend/typeproduct",$image);
             $type_pro->image=$image;
          }
        $type_pro->slug  = Str::slug($req->name,'-');
        $type_pro->save();
        return redirect('admin/typeproduct/add/')->with('success','Bạn thêm thành công');
    }
     public function getEdit($id){
        $type_pro = TypeProduct::find($id);
        return view('admin.typeproduct.edit',['type_pro'=>$type_pro]);
    }
    public function postEdit(Request $req, $id){
        // $this->validate($req,
        // [
        //   'image'=>'required',
        //   'desc'=>'required',
        //   'name'=>'required'

        // ],
        // [
        //   'image.required'=>'image không được trống',
        //   'desc.required'=>'Miêu tả không được trống',
        //   'name.required'=>'Tên không được trống'
        // ]);
        $type_pro = TypeProduct::find($id);
        $type_pro->name = $req->name;
        $type_pro->desc  = $req->desc;
         if ($req->hasFile('image')) {
             $file = $req->file('image');
             $duoi = $file->getClientOriginalExtension();
             if ($duoi !='jpg'&&$duoi !='png'&&$duoi !='jpeg' &&$duoi !='gif') {
                return redirect('admin/typeproduct/add')->with('danger','không đúng định dạng');
             }
             $name = $file->getClientOriginalName();
             $image = $name;
             while(file_exists("frontend/typeproduct/".$image)){
                $image = $name;
             }
             $file->move("frontend/typeproduct/",$image);
             $type_pro->image=$image;
          }
        $type_pro->slug  = Str::slug($req->name,'-');
        // dd($type_pro);
        $type_pro->save();
        return redirect('admin/typeproduct/edit/'.$id)->with('success','Bạn sửa thành công');
    }

    public function getDel($id)
       {
          $type_pro = TypeProduct::find($id);
          $type_pro->delete();

          return redirect('admin/typeproduct/list')->with('success','Bạn đã xóa thành công.');
       }
}
