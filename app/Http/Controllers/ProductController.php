<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use Illuminate\Support\Str;
use App\Product;
use App\TypeProduct;
use Carbon\Carbon;

class ProductController extends Controller
{
    public function __contruct(){
        $type = TypeProduct::all();
    }
     public function warehouse(){
        $data['product'] = Product::all();
        $data['type'] = TypeProduct::all();
        
      return view('admin.product.warehouse',$data);
    }
    public function getList(){
        $product = Product::all();
        $type = TypeProduct::all();
    	return view('admin.product.list',['product'=>$product,'type'=>$type]);
    }
     public function getAdd(){
        $type = TypeProduct::all();
    	return view('admin.product.add',['type'=>$type]);
    }
     public function postAdd(Request $req){
      $this->validate($req,
      [
         'name'=>'required',
         'image'=>'required',
         'promotion_pice'=>'required|numeric',
         'unit_price'=>'required|numeric',
         'desc'=>'required',
         'unit'=>'required',
         'new'=>'required',
         'id_type'=>'required',
         'ngaysx'=>'required',
         'hansd'=>'required',
         'soluong'=>'required|numeric',
         'size'=>'required'
      ],
      [
       'name.required'=>'name không được trống',
        'id_type.required'=>'id_type không được trống',
       'image.required'=>'image không được trống',
       'promotion_pice.required'=>'promotion_pice không được trống',
       'promotion_pice.numeric'=>'promotion_pice phải là số',
       'unit_price.required'=>'unit_price không được trống',
       'unit_price.numeric'=>'unit_price phải là số',
       'desc.required'=>'desc không được trống',
       'new.required'=>'new không được trống',
       'unit.required'=>'new không được trống',
       'ngaysx.required'=>'ngaysx không được trống',
       'hansd.required'=>'hansd không được trống',
       'soluong.required'=>'soluong không được trống',
       'size.required'=>'size không được trống'

      ]);
   
        $product = new Product;
        $product->name = $req->name;
        $product->id_type = $req->id_type;
        $product->desc  = $req->desc;
        $product->unit_price  = $req->unit_price;
        $product->promotion_pice  = $req->promotion_pice;
        //xử lý ảnh
           if ($req->hasFile('image')) {
             $file = $req->file('image');
             $duoi = $file->getClientOriginalExtension();
             if ($duoi !='jpg'&&$duoi !='png'&&$duoi !='jpeg' &&$duoi !='gif') {
                return redirect('admin/product/add')->with('loiIMG','không đúng định dạng');
             }
             $name = $file->getClientOriginalName();
             $image = $name;
             while(file_exists("frontend/product".$image)){
                $image = $name;
             }
             $file->move("frontend/product",$image);
             $product->image = $image;
          }
        $product->unit  = $req->unit ;
        $product->new  = $req->new ;
        $product->slug  = Str::slug($req->name,'-') ;
        $product->size  = $req->size ;
        $product->ngaysx  = $req->ngaysx ;
        if($req->hansd <= $req->ngaysx){
          return redirect('admin/product/add')->with('loiNSX','Hạn sử dụng phải lớn hơn ngày sản xuất');
        }else{
          $product->hansd  = $req->hansd;
        }

        if($req->soluong < 0){
          return redirect('admin/product/add')->with('loiSL','Số lượng phải lớn hơn 0');
        }else{
        $product->soluong  = $req->soluong;
        }

        $product->save();
        return redirect('admin/product/add/')->with('success','Bạn thêm thành công');
    }
     public function getEdit($id){
        $type = TypeProduct::all();
        $product = Product::find($id);
        $product_new = Product::get('new');
        $product_size = Product::get('size');
        return view('admin.product.edit',['product_size'=>$product_size,'product_new'=>$product_new,'product'=>$product,'type'=>$type]);
    }
    public function postEdit(Request $req, $id){
         $this->validate($req,
      [
         'name'=>'required',
         'promotion_pice'=>'required|numeric',
         'unit_price'=>'required|numeric',
         'desc'=>'required',
         'unit'=>'required',
         'ngaysx'=>'required',
         'hansd'=>'required',
         'soluong'=>'required',
         'size'=>'required',
         'new'=>'required'
      ],
      [
       'name.required'=>'name không được trống',
       'image.required'=>'image không được trống',
       'promotion_pice.required'=>'promotion_pice không được trống',
       'promotion_pice.numeric'=>'promotion_pice phải là số',
       'unit_price.required'=>'unit_price không được trống',
       'unit_price.numeric'=>'unit_price phải là số',
       'desc.required'=>'desc không được trống',
       'unit.required'=>'unit không được trống',
       'ngaysx.required'=>'ngaysx không được trống',
       'hansd.required'=>'hansd không được trống',
       'new.required'=>'new không được trống',
       'size.required'=>'size không được trống',
       'soluong.required'=>'soluong không được trống'
      ]);
        
        $product = Product::find($id);
        $product->name = $req->name;
        $product->id_type = $req->id_type;
        $product->desc  = $req->desc;
        $product->unit_price  = $req->unit_price;
        $product->promotion_pice  = $req->promotion_pice;
        //xử lý ảnh
           if ($req->hasFile('image')) {
             $file = $req->file('image');
             $duoi = $file->getClientOriginalExtension();
             if ($duoi !='jpg'&&$duoi !='png'&&$duoi !='jpeg' &&$duoi !='gif') {
                return redirect('admin/product/add')->with('danger','không đúng định dạng');
             }
             $name = $file->getClientOriginalName();
             $image = $name;
             while(file_exists("frontend/product".$image)){
                $image = $name;
             }
             $file->move("frontend/product",$image);
             $product->image = $image;
          }
        $product->unit  = $req->unit ;
        $product->new  = $req->new;
        $product->slug  = Str::slug($req->name,'-') ;
        $product->size  = $req->size ;
        if($req->hansd <= $req->ngaysx){
          return redirect()->back()->with('loiNSX','Hạn sử dụng phải lớn hơn ngày sản xuất');
        }else{
          $product->hansd  = $req->hansd;
        }

        if($req->soluong < 0){
          return redirect()->back()->with('loiSL','Số lượng phải lớn hơn 0');
        }else{
        $product->soluong  = $req->soluong;
        }
        $product->save();
        return redirect('admin/product/list/')->with('success','Bạn sửa thành công');
    }

    public function getDel($id){
       
          $product = Product::find($id);
          $product->delete();

          return redirect('admin/product/list')->with('success','Bạn đã xóa thành công.');
    }

}
