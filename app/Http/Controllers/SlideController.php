<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SlideRequest;
use Illuminate\Support\Str;
use App\Slide;

class SlideController extends Controller
{
    public function getList(){
        $slide = Slide::all();
    	return view('admin.slide.list',['slide'=>$slide]);
    }
     public function getAdd(){
    	return view('admin.slide.add');
    }
     public function postAdd(Request $req){
        $this->validate($req,
        [
          'link' =>'required',
          'image'=>'required'
        ],
        [
          
          'link.required'=>'link không được trống !!!',
          'image.required'=>'image không được trống !!!'
        ]);
        $slide = new Slide;
        $slide->link = $req->link;
       if ($req->hasFile('image')) {
            $file = $req->file('image');
            $duoi = $file->getClientOriginalExtension();
            if ($duoi !='jpg'&&$duoi !='png'&&$duoi !='jpeg' &&$duoi !='gif') {
                return redirect('admin/slide/add')->with('danger','không đúng định dạng');
            }
            $name = $file->getClientOriginalName();
            $image = $name;
            while(file_exists("frontend/slide".$image)){
                $image = $name;
            }
            $file->move("frontend/slide",$image);
            $slide->image=$image;
        }
        $slide->save();
        return redirect('admin/slide/add/')->with('success','Bạn thêm thành công');
    }
     public function getEdit($id){
        $slide = Slide::find($id);
        return view('admin.slide.edit',['slide'=>$slide]);
    }
    public function postEdit(Request $req, $id){
        $this->validate($req,
        [
          'link' =>'required',
          'image'=>'required'
        ],
        [
          
          'link.required'=>'link không được trống !!!',
          'image.required'=>'image không được trống !!!'
        ]);
        $slide = Slide::find($id);
        $slide->link = $req->link;
         if ($req->hasFile('image')) {
             $file = $req->file('image');
             $duoi = $file->getClientOriginalExtension();
             if ($duoi !='jpg'&&$duoi !='png'&&$duoi !='jpeg' &&$duoi !='gif') {
                return redirect('admin/slide/add')->with('danger','không đúng định dạng');
             }
             $name = $file->getClientOriginalName();
             $image = $name;
             while(file_exists("frontend/slide/".$image)){
                $image = $name;
             }
             $file->move("frontend/slide/",$image);
             $slide->image=$image;
          }
        $slide->save();
        return redirect('admin/slide/edit/'.$id)->with('success','Bạn sửa thành công');
    }

    public function getDel($id)
       {
          $slide = Slide::find($id);
          $slide->delete();

          return redirect('admin/slide/list')->with('success','Bạn đã xóa thành công.');
       }
}
