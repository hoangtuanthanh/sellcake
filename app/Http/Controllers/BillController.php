<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\BillRequest;
use Illuminate\Support\Str;
use App\Bill;
use App\BillDetail;
use App\Customer;
use DB;
use Carbon\Carbon;
use App\Feeship;
use Auth;

use App\Exports\BillExport;
use Maatwebsite\Excel\Facades\Excel;
class BillController extends Controller
{
  public function __construct(){
  		$customer = Customer::all();
      $billdetail = BillDetail::all();
  	}
  public function getList(){
    $data['feeship'] = Feeship::all();
    $data['bill'] = Bill::whereBetween('status',[1,3])->get();
    $data['bills'] = Bill::find(2);
     return view('admin.bill.list',$data);
   }
  public function getXuly(Request $req,$id){
    $bill = Bill::find($id);
    $bill->status = 2;
    $bill->save();
     return redirect()->back()->with('success','Đã xử lý đơn');
  }
  public function getProcessed(Request $req){
    $month = date('m');
    $data['bills'] = Bill::where('status',4)->whereMonth('date_order',$month)->orderBy('id','desc')->get();
    $data['billCount']= Bill::where('status',4)
                  ->whereMonth('date_order',$month)
                  ->sum('total');
    return view('admin.bill.success',$data);  
  }
   public function getRefuseAD(){
      $month = date('m');
      $data['bill'] = Bill::where('status', 5)->whereMonth('date_order', $month)->orderBy('id','desc')->get();
      return view('admin.bill.refuse',$data); 
  }
  public function postBillCountDatechange(Request $req){
    $start = $req->start."";
    $end = $req->end."";
    $bill_count= Bill::where('status',4)
                  ->whereDate('date_order','>=',$start)
                  ->whereDate('date_order','<=',$end)
                  ->sum('total');
    return response()->json([
        'bill_count' => $bill_count,
    ]);          
  }

  public function getListDetail($id){
    $bill_detail = BillDetail::join('bills','bills.id','=','bill_detail.id_bill')
            ->where('bill_detail.id_bill','=',$id)
            ->get();
      return view('admin.bill.listBillDetail',compact('bill_detail'));
  }
  public function getDelete($id)
 {
    $bill = Bill::find($id);
    $bill->delete();
    return redirect('admin/bill/list')->with('success','Bạn đã xóa thành công.');
 }
  public function postBillCountDate(Request $req){
    $start = $req->start."";
    $end = $req->end."";
    $bills = Bill::where('status',4)
                  ->whereDate('date_order','>=',$start)
                  ->whereDate('date_order','<=',$end)
                  ->get();
    $total = Bill::where('status',4)
                  ->whereDate('date_order','>=',$start)
                  ->whereDate('date_order','<=',$end)
                  ->sum('total');
    return response()->json([
        'bills' => $bills,
        'total' => $total
    ]);
  }
   public function postBillCountDateAD(Request $req){
    $start = $req->start."";
    $end = $req->end."";
    $bills = Bill::where('status',5)
                  ->whereDate('date_order','>=',$start)
                  ->whereDate('date_order','<=',$end)
                  ->get();
  
    return response()->json([
        'bills' => $bills,
    ]);
  }

   //Lọc đơn hàng theo trạng thais
  public function getLocDonHangBill($id){
   $bill = Bill::with('Customer')->where('status',$id)->get()->toArray();
   $stt = 1;
   foreach($bill as $val)
   {
    ?>
    <tr>
        <td><?php echo '00000'.$val['id']; ?></td>
         <td><?php echo $val['id_customer']; ?></td>
         <td><?php echo date("d-m-Y", strtotime($val['date_order'])); ?></td>
         <td><?php echo $val['payment']; ?></td>
         <td><?php echo number_format($val['total']); ?><sup>đ</sup></td>
         <td><?php if($val['status'] ==1){  ?>
             <span style="color: blue">Chờ giao hàng</span>
             <?php } if($val['status'] ==2){  ?>
             <span style="color: blue">Xác nhận giao hàng</span>
             <?php } if($val['status'] ==3){  ?>
             <span style="color: blue">Đang giao hàng</span>
         <?php } ?>
         </td>
        <td> <a href="<?php echo route('product.detail',['id' => $val['id']]); ?>">Chi tiết</a></td>
    </tr>
    <?php $stt++;
    }
  }
  public function getDowloadExcel(Request $req){
    $start = $req->start_date;
    $end = $req->end_date;
    $month = date('m');
    if ($start != "" || $end != "" ){
      $nameFile = "Bill"."_".$start."_".$end."_".date('Hms');
      $export = new BillExport(Auth::user()->id, $start, $end);
      Excel::store($export, $nameFile.'.csv');
     return Excel::download($export, $nameFile.'.xlsx');
    }else{
      $nameFile = "Bill"."_".$month."_".date('Hms');
      $export = new BillExport(Auth::user()->id, $month);
      Excel::store($export, $nameFile.'.csv');
      return Excel::download($export, $nameFile.'.xlsx');
    } 

  }
  public function getStoreExcel(Request $req){
    // $start = $req->start_date;
    // $end = $req->end_date;
    // $month = date('m');
    // if ($start != '' || $end != '' ){
    //   $nameFile = "Bill"."_".$start."_".$end."_".date('Hms');
    //   $export = new BillExport(Auth::user()->id, $start, $end);
    //  return Excel::store($export, $nameFile.'.csv');
    // }else
    //   $nameFile = "Bill"."_".$month."_".date('Hms');
    //   $export = new BillExport(Auth::user()->id, $month);
    //    return Excel::store($export, $nameFile.'.csv');
    //   'done';
  }
}

