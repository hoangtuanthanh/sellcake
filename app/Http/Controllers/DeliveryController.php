<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\Province;
use App\Wards;
use App\Feeship;
class DeliveryController extends Controller
{
	public function getList(){
		$data['ship_all'] = Feeship::all();
		return view('admin.delivery.list',$data);
	}
    public function getAdd(Request $req){
    	$data['city'] = City::all();
    	$data['province'] = Province::all();
    	$data['wards'] = Wards::all();
    	return view('admin.delivery.add',$data);
    }
    public function selectDelivery(Request $req){
    	$data = $req->all();
    	if ($data['action']) {
    		$output ='';
    		if($data['action'] =="city"){
	    		$select_pro = Province::where('matp',$data['ma_id'])->get();
	    		$output.='<option>---Chọn quận huyện---</option>';
	    		foreach ($select_pro as $key => $province){
	    			$output.='<option value="'.$province->maqh.'">'.$province->name_province.'</option>';
	    		}
    		}else{
    			$select_wards = wards::where('maqh',$data['ma_id'])->get();
    			$output.='<option>---Chọn xã phường---</option>';
	    		foreach ($select_wards as $key => $wards){
	    			$output.='<option value="'.$wards->xaid.'">'.$wards->name_ward.'</option>';
	    		}
    		}
    	}
    	echo $output;
    }
    public function insertDelivery(Request $req){
    	$data = $req->all();
    	$fee_ship = new Feeship();
    	$fee_ship->matp = $data['city'];
    	$fee_ship->maqh =$data['province'];
    	$fee_ship->xaid =$data['wards'];
    	$fee_ship->feeship =$data['fee_ship'];
    	$fee_ship->save();
    	return redirect()->back()->with('success','Thêm phí thành công');
	}
}
