<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subcriber;
class SubController extends Controller
{
     public function getList(){
        $sub = Subcriber::all();
    	return view('admin.subcriber.list',['sub'=>$sub]);
    }
    
}
