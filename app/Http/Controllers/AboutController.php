<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\About;

class AboutController extends Controller
{
    public function getList(){
    	$about = About::orderBy('time')->get();
    	return view('admin.about.list',['about'=>$about]);
    }
     public function getAdd(){
    	return view('admin.about.add');
    }
    public function postAdd(Request $req){
        $message =[
         'required' =>' :attribute không được trống !!!.',
         'numeric' => ':attribute phải là kiểu số !!!'
      ];
      $this->validate($req,
      [
         'title' => 'required',
         'address'   =>'required',
        'time'   =>'required',
         'content'=>'required',
         'image'   =>'required',
  
      ],$message);

    	$about = new About;
    	$about->title = $req->title;
        $about->time = $req->time;
    	$about->address  = $req->address;
    	$about->content  = $req->content;
    	 if ($req->hasFile('image')) {
             $file = $req->file('image');
             $duoi = $file->getClientOriginalExtension();
             if ($duoi !='jpg'&&$duoi !='png'&&$duoi !='jpeg' &&$duoi !='gif') {
                return redirect('admin/about/add')->with('danger','không đúng định dạng');
             }
             $name = $file->getClientOriginalName();
             $image = $name;
             while(file_exists("frontend/about".$image)){
                $image = $name;
             }
             $file->move("frontend/about",$image);
             $about->image = $image;
        }
    	$about->save();
    	return redirect('admin/about/add/')->with('success','Bạn thêm thành công');
    }
     public function getEdit($id){
     	$about = About::find($id);
    	return view('admin.about.edit',['about'=>$about]);
    }
    public function postEdit(Request $req, $id){
          $message =[
         'required' =>' :attribute không được trống !!!.',
         'numeric' => ':attribute phải là kiểu số !!!'
      ];
      $this->validate($req,
      [
         'title' => 'required',
         'address'   =>'required',
         'time'   =>'required',
         'content'=>'required',
         // 'image'   =>'required', 
  
      ],$message);

    	$about = About::find($id);
    	$about->title = $req->title;
        $about->time = $req->time;
    	$about->address  = $req->address;
    	$about->content  = $req->content;
    	 if ($req->hasFile('image')) {
             $file = $req->file('image');
             $duoi = $file->getClientOriginalExtension();
             if ($duoi !='jpg'&&$duoi !='png'&&$duoi !='jpeg' &&$duoi !='gif') {
                return redirect('admin/about/edit'.$id)->with('danger','không đúng định dạng');
             }
             $name = $file->getClientOriginalName();
             $image = $name;
             while(file_exists("frontend/about".$image)){
                $image = $name;
             }
             $file->move("frontend/about",$image);
             $about->image = $image;
        }
    	$about->save();
    	return redirect('admin/about/edit/'.$id)->with('success','Bạn sửa thành công');
    }

    public function getDel($id)
	   {
	      $about = About::find($id);
	      $about->delete();
	      return redirect('admin/about/list')->with('success','Bạn đã xóa thành công.');
	   }
}

