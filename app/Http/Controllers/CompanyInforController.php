<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CompanyRequest;
use App\CompanyInfo;

class CompanyInforController extends Controller
{
    public function getList(){
      $company = CompanyInfo::all();
    	return view('admin.companyinfo.list',['company'=>$company]);
    }
    
     public function getEdit($id){
        $company = CompanyInfo::find($id);
        return view('admin.companyinfo.edit',['company'=>$company]);
    }
    public function postEdit(CompanyRequest $req, $id){
        $company = CompanyInfo::find($id);
       	$company->address = $req->address ;
        $company->tax_number = $req->tax_number ;
        $company->phone = $req->phone ;
        $company->email  = $req->email ;
        $company->hotline = $req->hotline ;
        $company->website  = $req->website ;
        $company->save();
        return redirect('admin/companyinfo/edit/'.$id)->with('success','Bạn sửa thành công');
    }
    public function getDel($id)
 {
    $bill = CompanyInfo::find($id);
    $bill->delete();
    return redirect('admin/companyinfo/list')->with('success','Bạn đã xóa thành công.');
 }

}
