<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Session;
use Hash;
use Carbon\Carbon;

class LoginController extends Controller
{
    public function getLogin(){
		return view('page.login');

	}
	public function postLogin(Request $req){
		$arr= [
		'email'=>$req->email,
		'password'=>$req->password
		];
		// $user = Auth::user();
			if (Auth::attempt($arr)) {
		    	return redirect('/');
		    }
		    else{
		    	return redirect('login')->with('thongbao','Đăng nhập không thành công');
		    }
	}
	public function getRegister(){
		return view('page.signup');
	}
	public function postRegister(Request $req){
		$register = new User();

		$register->name = $req->name;

		$register->email = $req->email;
		$register->phone = $req->phone;
		$register->address = $req->address;
		$register->password =bcrypt($req->password);
		$register->role = 0;
		$register->save();
		return redirect()->back()->with('success','Đăng kí thành công');
	}

	public function getLogout(){
		Auth::logout();
		return redirect(route('login-admin-get'));
	}
}
