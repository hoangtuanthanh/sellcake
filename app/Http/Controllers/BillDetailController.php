<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Bill_DetailRequest;
use Illuminate\Support\Str;
use App\BillDetail;
use App\Product;
use App\Bill;
use DB;
class BillDetailController extends Controller
{
     public function getList(){
      $product = Product::all();
      $bill = Bill::all();
      $billdetail = BillDetail::all();
    	return view('admin.billdetail.list',['billdetail'=>$billdetail,'product'=>$product,'bill'=>$bill]);
    }
     public function getAdd(){
      $product = Product::all();
      $bill = Bill::all();
    	return view('admin.billdetail.add',['product'=>$product,'bill'=>$bill]);
    }
    public function postAdd(Request $req){
    $this->validate($req,
      [
        'quantity'=>'required|numeric',

        'unit_price'=>'required|numeric'
      ],
      [
        'quantity.required'=>'quantity không được trống',
        'quantity.numeric'=>'quantity phải là kiểu số',
        'unit_price.required'=>'unit_price không được trống',
        'unit_price.numeric'=>'unit_price phải là kiểu số'
      ]);
        $billdetail = new BillDetail;
        $billdetail->id_bill = $req->id_bill ;
        $billdetail->id_product = $req->id_product ;
        $billdetail->quantity = $req->quantity ;
        $billdetail->unit_price  = $req->unit_price ;

        $billdetail->save();
        return redirect('admin/billdetail/add/')->with('success','Bạn thêm thành công');
    }
     public function getEdit($id){
        $product = Product::all();
        $bill = Bill::all();
        $billdetail = BillDetail::find($id);
        return view('admin.billdetail.edit',['billdetail'=>$billdetail,'product'=>$product,'bill'=>$bill]);
    }
    public function postEdit(Request $req, $id){
        $this->validate($req,
      [
        'quantity'=>'required|numeric',
        'unit_price'=>'required|numeric'
      ],
      [
        'quantity.required'=>'quantity không được trống',
        'quantity.numeric'=>'quantity phải là kiểu số',
        'unit_price.required'=>'unit_price không được trống',
        'unit_price.numeric'=>'unit_price phải là kiểu số'
      ]);
        $billdetail = BillDetail::find($id);
        $billdetail->id_bill = $req->id_bill ;
        $billdetail->id_product = $req->id_product ;
        $billdetail->quantity = $req->quantity ;
        $billdetail->unit_price  = $req->unit_price ;
        $billdetail->save();
        return redirect('admin/billdetail/edit/'.$id)->with('success','Bạn sửa thành công');
    }

    public function getDel($id)
       {
          $billdetail = BillDetail::find($id);
          $billdetail->delete();

          return redirect('admin/billdetail/list')->with('success','Bạn đã xóa thành công.');
       }
}
