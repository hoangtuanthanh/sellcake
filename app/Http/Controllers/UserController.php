<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\User;
use Mash;
class UserController extends Controller
{
    public function getList(){
        $user = User::all();
    	return view('admin.user.list',['user'=>$user]);
    }
    
    public function getLockUser($id){
      $user = User::find($id);
      $user->lock = 2;
      $user->save();
      return redirect()->back()->with('success','Tài khoản đã bị khóa');
    }
    public function getUnlockUser($id){
      $user = User::find($id);
      $user->lock = 1;
      $user->save();
      return redirect()->back()->with('success','Đã mở khóa tài khoản');
    }
     public function getAdd(){
    	return view('admin.user.add');
    }

   public function postAdd(Request $req){
        $user = new User;
        $user->name = $req->name;
        $user->email  = $req->email;
        $user->password =bcrypt($req->password);
        if($user->role == $req->check_user){
            $user->role = 0;     
        }else{
            $user->role = 1;
        }
        $user->lock  = 1;
        $user->save();
        return redirect()->back()->with('success','Bạn thêm thành công');
    }
     public function getEdit($id){
        $user = User::find($id);
        return view('admin.user.edit',['user'=>$user]);
    }
    public function postEdit(Request $req, $id){
        $user = User::find($id);
        $user->name = $req->name;
        $user->email  = $req->email;
        $user->role = $req->role;
        if($req->check == "on")
         {
            $this->validate($req,
            [
               'password' => 'required|min:3|max:32',
               'passwordAgain' =>'required|same:password'
            ],
            [
               'password.required' => 'Bạn chưa nhập mật khẩu',
               'password.min' => 'Mật khẩu phải chứa ít nhất 3 ký tự',
               'password.max' => 'Mật khẩu chỉ được tối đa 32 ký tự',
               'passwordAgain.required' => 'Bạn chưa nhập lại mật khẩu',
               'passwordAgain.same' => 'Mật khẩu nhập lại chưa đúng'
            ]);
          }
        $user->password =bcrypt($req->password);
        $user->save();
        return redirect('admin/user/edit/'.$id)->with('success','Bạn sửa thành công');
    }
    public function getDel($id)
       {
          $user = user::find($id);
          $user->delete();
          return redirect('admin/user/list')->with('success','Bạn đã xóa thành công.');
       }
}
