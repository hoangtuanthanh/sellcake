<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Http\Requests\ContactRequest;
use Session;
use App\Cart;
use lluminate\Database\Eloquent\Collection;
use App\Slide;
use App\Product;
use App\Bill;
use App\BillDetail;
use App\TypeProduct;
use  App\Customer;
use  App\News;
use  App\CompanyInfo;
use  App\Subcriber;
use App\About;
use App\Contact;
use Mail;
use App\Mail\ShoppingMail;
use Carbon\Carbon;
use App\City;
use App\Province;
use App\Wards;
use App\Feeship;
use App\Voucher;
use DB;

class PageController extends Controller
{
  public function getAllProduct(Request $req){

    $data['data']= Product::all();
    return view('page.allProduct',$data);
  }
  public function upPro(){
    $data['data'] = Product::orderBy('unit_price','asc')->get();
      return view('page.filter.up',$data);
  }
    public function downPro(){
    $data['data'] = Product::orderBy('unit_price','desc')->get();
     return view('page.filter.down',$data);
  }
  public function pricePro0(){
    $data['data'] = Product::whereBetween('unit_price',[0,100000])->orderBy('unit_price','asc')->get();
      return view('page.filter.price0',$data);
  }
  public function pricePro1(){
    $data['data'] = Product::whereBetween('unit_price',[100000,200000])->orderBy('unit_price','asc')->get();
     return view('page.filter.price100',$data);
  }
  public function pricePro2(){
    $data['data'] = Product::whereBetween('unit_price',[200000,300000])->orderBy('unit_price','asc')->get();
      return view('page.filter.price200',$data);
  }
  public function pricePro3(){
    $data['data'] = Product::whereBetween('unit_price',[300000,9900000])->orderBy('unit_price','asc')->get();
      return view('page.filter.price300',$data);
  }
  public function sizeM(){
    $data['data'] = Product::Where('size','M')->orderBy('unit_price','asc')->get();
     return view('page.filter.SizeM',$data);
  }
  public function sizeS(){
    $data['data'] = Product::Where('size','S')->orderBy('unit_price','asc')->get();
      return view('page.filter.SizeS',$data);
  }
  public function sizeL(){
    $data['data'] = Product::Where('size','L')->orderBy('unit_price','asc')->get();
      return view('page.filter.SizeL',$data);
  }

  public function getAllCateProduct(Request $req){
    //lọc giá 
      $product = DB::table('products')
      ->join('type_products','type_products.id','products.id_type');
      if($req->cat != ''):
        $cat_id = $req->cat;
        $product->where('products.id_type',$cat_id);
      endif;
      if($req->price != ''):
        $price = $req->price;
        $pieces = explode("|", $price);
        $product->whereBetween('products.unit_price', $pieces);
      endif;
      $data['product'] =$product->get();
      return response()->json([
            'data' => $data,
        ]);

      
       // price are array
       // if($cat_id!="" && $price1!="0"){
       //   $price = explode("-",$req->price);

       //    $start = $price[0];
       //    $end = $price[1];

       //    //echo "both are selected";
       //    $data = DB::table('products')
       //    ->join('type_products','type_products.id','products.id_type')
       //    ->where('products.id_type',$cat_id)
       //    ->where('products.unit_price', ">=", $start)
       //    ->where('products.unit_price', "<=", $end)
       //    ->get();
       //  // return response()->json([
       //  //     'data' => $data,
       //  // ]);
       // }
       // else if($price1!="0"){
       //   $price = explode("-",$req->price);
       //   $start = $price[0];
       //   $end = $price[1];

       //   //echo "price is selected";
       //   $data = DB::table('products')
       //   ->join('type_products','type_products.id','products.id_type')
       //   ->where('products.unit_price', ">=", $start)
       //   ->where('products.unit_price', "<=", $end)
       //   ->get(); 
       //  // return response()->json([
       //  //     'data' => $data,
       //  // ]);

       // }
       // else if($cat_id!=""){
       //   //echo "cat is selected";
       //   $data = DB::table('products')
       //   ->join('type_products','type_products.id','products.id_type')
       //   ->where('products.id_type',$cat_id)
       //   ->get();
       //  //   return response()->json([
       //  //     'data' => $data,
       //  // ]);
       // }
       // else{
       //   //echo "nothing is slected";
       //   return "<h1 align='center'>Please select atleast one filter from dropdown</h1>";

       // }
  }

 public function getIndex(Request $req){
  $slide = Slide::all();
  $new_product = Product::where('new','Mới')->paginate(4);
  $sp_kmai = Product::where('promotion_pice','<>',0)->paginate(8);
  $sp_banchay = Product::Where('new','Bán chạy')->paginate(4);
  $type_pro = TypeProduct::all();
  $news = News::all();
  $Info = CompanyInfo::all();
  return view ('page.trangchu',['type_pro'=>$type_pro,'slide'=>$slide,'new_product'=>$new_product,'sp_kmai'=>$sp_kmai,'sp_banchay'=>$sp_banchay,'news'=>$news,'Info'=>$Info]);                                  // cos the su dung 1 trong 2 //
    // return view ('page.trangchu',compact('slide','new_product'));   //                           //
   }
   public function getSub(){
    return view('page.trangchu');
   }
   public function postSub(Request $req){
    $sub = new Subcriber;
    $sub->email= $req->email;
    $sub->save();
    return redirect()->back()->with('success','đăng kí thành công');
   }
   public function getLoaisp($id, Request $req){
    $sp_theoloai= Product::Where('id_type',$id)->paginate(3);
    $sp_khac = Product::where('id_type','<>',$id)->paginate(3);
    $loai = TypeProduct::all();
    $loai_sp = TypeProduct::where('id',$id)->first();

      //lọc giá 
      // $id = $req->id;
      // $priceCount = $req->price;
      // if($id != "" && $priceCount !="0"){
      //   $price = explode("-",$req->price);
      //   $start = $price[0];
      //   $end = $price[1];

      //   $new_product = DB::table('products')->join('type_products','type_products.id','products.id_type')
      //   ->where('products.id_type', $id)
      //   ->where('products.unit_price','>=',$start)
      //   ->where('products.unit_price','<=',$end)
      //   ->get();
      // }
      

    //Lọc sản phẩm theo size
    if(!empty($_GET['size'])){
      $sizeArray = explode('-', $_GET['size']);
      $loai_sp = $loai_sp->whereIn('products.size',$sizeArray);
    }
      $sizeArray = Product::select('size')->groupBy('size')->get();
      $sizeArray = array_flatten(json_decode(json_encode($sizeArray),true));
      $sizeUrl = "";
      if(!empty($data['sizeFilter'])){
        foreach ($data['sizeFilter'] as $size) {
          if (empty($sizeUrl)) {
            $sizeUrl = "&size".$size;
          }else{
            $sizeUrl .= "-".$size;
          }
        }
      }
      
      // echo "<pre>";print_r($sizeArray);

    return view ('page.product_type',compact('sp_theoloai','sp_khac','loai','loai_sp','sizeArray'));
  }
  
  public function getChitiet(Request $req){
    $sanpham = Product::where('id',$req->id)->first();
    $sp_giong = Product::where('id_type',$sanpham->id_type)->paginate(3);
    $best = Product::where('promotion_pice','<>',0)->paginate(4);
    $n_pro = Product::where('new','Mới')->paginate(4);
   
    return view('page.product_detail',compact('sanpham','sp_giong','best','n_pro'));
  }
  public function getThemGioHang(Request $req,$id){
    $product = Product::find($id);
    if($product != null){
       $oldCart = Session('cart')?Session::get('cart'):null; //kieemr tra - dung toan tu? 3 ngoi.
      $cart = new Cart($oldCart);
      $cart->add($product,$id);//phuong thuc add trong Cart.php truyen vao 2 tham so la` items va id
      $req->session()->put('cart',$cart); //gan gio? hang vao session cart bang phuowng thuc put
    } 
    return redirect()->back();

  }
  public function getDelCart($id){
    $oldCart = Session::has('cart')?Session::get('cart'):null;
    $cart = new Cart($oldCart);
    $cart->removeItem($id);
    if(count($cart->items)>0){
      Session::put('cart',$cart);
    }
    else{
      Session::forget('cart');
    }
    return redirect()->back();
  }
  public function getDatHang(Request $req){
    $city =City::all();
    $province =Province::all();
    $wards =Wards::all();
    $feeship =Feeship::all();

    return view('page.checkout',compact('city','province','wards','feeship'));
  }
   public function getShopCart(){
    return view('page.shopping_cart');
  } 
  
  public function postDatHang(Request $req){
    $cart = Session::get('cart');

    // dd($cart);
    // var_dump($cart);
    // $BillDetails = [];
    $customer = new Customer;
    $customer->name = $req->name;
    $customer->gioitinh = $req->gioitinh;
    $customer->email = $req->email;
    $customer->address = $req->address;
    $customer->phone = $req->phone;
    $customer->note = $req->note;
    $customer->save();

    $bill= new Bill;
    $bill->id_customer = $customer->id;
    $bill->date_order = date('Y-m-d');
    if(Session::get('fee') && Session::get('coupon')){
      $bill->total = $cart->totalPrice + Session::get('fee') - Session::get('coupon')['discount_amount'];
    }
    elseif(Session::get('fee') && !Session::get('coupon')){
      $bill->total = $cart->totalPrice + Session::get('fee');
    }
    elseif(Session::get('coupon') && !Session::get('fee')){
      $bill->total = $cart->totalPrice - Session::get('coupon')['discount_amount'];
    }
    $bill->fee_ship =Session::get('fee');
    $bill->payment = $req->payment;
    $bill->note = $req->note;
    $bill->status = 1;
    $bill->save();
    
    $bill_details = [];
    foreach ($cart->items as $key => $value) {
      // $billdetail= BillDetail::find($id);
      $bill_detail = new BillDetail;
      $bill_detail->id_bill = $bill->id;
      $bill_detail->id_product = $key;
      $bill_detail->quantity = $value['qty'];
      $bill_detail->unit_price = ($value['price']/$value['qty']);
      $bill_details[$key] = $bill_detail->save();

      //sp đã bán 
      $product = Product::find($bill_detail->id_product);
        if($product->id = $bill_detail->id_product){
          if($bill->status == 1){
            $product->daban +=  $bill_detail->quantity;
            if($product->promotion_pice != 0){
              $product->loinhuan = ($product->promotion_pice - $product->gianhap)* $product->daban;
            }else{
              $product->loinhuan = ($product->unit_price - $product->gianhap)* $product->daban;
            }
          }
        }
        $product->save();
      }
      // $voucher = Voucher::find($bill_detail->id_product);
      // if( $bill->status == 1){
      // $voucher->used_user = $voucher->used_user + 1;
      // }
      // $voucher->save();
    Mail::to($bill->Customer['email'])->send(new ShoppingMail($bill,$bill_details));
    Session::forget('cart');
    Session::forget('fee');
    Session::forget('coupon');
    return redirect()->back()->with('success','Gửi đơn hàng thành công');

  }
  public function getSearch(Request $req){
    $product = Product::where('name','like','%'.$req->key.'%') //key - name của form search
        ->orWhere('unit_price',$req->key)
        ->get();
      return view('page.search',compact('product'));
  }

  public function getLienhe(){
    $contact = Contact::all();
    return view ('page.contacts',compact('contact'));
  }
    public function postLienhe(ContactRequest $req){
    $contact = new Contact;
    $contact->name = $req->name;
    $contact->email = $req->email;
    $contact->address = $req->address;
    $contact->phone = $req->phone;
    $contact->content = $req->content;
    $contact->status = 1;
     $contact->save();
     return redirect()->back()->with('success','Gửi thành công');
  }
  public function getGioithieu(){
    $data['about'] = About::orderBy('time')->get();
    $data['customer'] = Customer::all();
    $data['news'] = News::all();
    $data['product'] = Product::all();
    $data['bill'] = Bill::count('id') + 1000;
    return view ('page.about',$data);
  }


  
  //chon địa điểm vận chuyển
   public function selectDeliveryHome(Request $req){
    $data = $req->all();
    if ($data['action']) {
      $output ='';
      if($data['action'] =="city"){
        $select_pro = Province::where('matp',$data['ma_id'])->get();
        $output.='<option>---Chọn quận huyện---</option>';
        foreach ($select_pro as $key => $province){
          $output.='<option value="'.$province->maqh.'">'.$province->name_province.'</option>';
        }
      }else{
        $select_wards = wards::where('maqh',$data['ma_id'])->get();
        $output.='<option>---Chọn xã phường---</option>';
        foreach ($select_wards as $key => $wards){
          $output.='<option value="'.$wards->xaid.'">'.$wards->name_ward.'</option>';
        }
      }
    }
    echo $output;
  }

  //tính phi ship
  public function CalculateFee(Request $req){
    $data = $req->all();
    if($data['matp']){
      $feeship = Feeship::where('matp',$data['matp'])->where('maqh',$data['maqh'])->where('xaid',$data['xaid'])->get();
      if($feeship->count()>0){
        foreach ($feeship as $key => $fee) {
          Session::put('fee',$fee->feeship);
          Session::save();
        }
      }else{
          Session::put('fee',10000);
          Session::save();
      }
    }
  }
}   