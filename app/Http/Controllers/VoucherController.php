<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Voucher;
use Session;
use Carbon\Carbon;
class VoucherController extends Controller
{
    public function postVoucher(Request $req){
    	$coupon = Voucher::where('code',$req->coupon_code)->first();
        $coupon->used_user +=1;
        $coupon->save();
    	if(!$coupon){
    		return redirect()->route('dat-hang')->with('danger','Mã voucher không tồn tại, mời nhập lại');
    	}
    	// $coupon_count = Voucher::all();
    	if($coupon ->used_user >= $coupon->max_uses_user){
			return redirect()->route('dat-hang')->with('danger','Mã voucher đã hết lượt sử dụng ');
		}
		if($coupon->expires_at <= Carbon::now()){
			return redirect()->route('dat-hang')->with('danger','Mã voucher đã quá hạn sử dụng ');
		}
    	session()->put('coupon',[
    		'name'=>$coupon->code,
    		'discount_amount'=>$coupon->discount_amount,
    	]);
    	return redirect(route('dat-hang'))->with('success','Mã voucher được sử dụng');
    }
    public function delVoucher(){
    	Session::forget('coupon');
		return redirect()->route('dat-hang')->with('success','Mã voucher đã được hủy ');

    }
    public function getList(){
        $voucher = Voucher::all();
        return view('admin.voucher.list',['voucher'=>$voucher]);
    }
    public function getAdd(){
        return view('admin.voucher.add');
    }
    public function postAdd(Request $req){
    $this->validate($req,
        [
            'code'=>'required',
            'name'=>'required',
            'description'=>'required',
            'max_uses_user'=>'required',
            'discount_amount'=>'required',
            'starts_at'=>'required',
            'expires_at'=>'required'
        ],
        [
            'code.required'=>'code không được trống !!!',
            'name.required'=>'name không được trống !!!',
            'description.required'=>'description không được trống !!!',
            'max_uses_user.required'=>'max_uses_user không được trống !!!',
            'discount_amount.required'=>'discount_amount không được trống !!!',
            'starts_at.required'=>'starts_at không được trống !!!',
            'expires_at.required'=>'expires_at không được trống !!!',
        ]);
        $voucher = new Voucher;
        $voucher->code = $req->code ;
        $voucher->name = $req->name ;
        $voucher->description = $req->description ;
        $voucher->max_uses_user  = $req->max_uses_user;
        $voucher->discount_amount = $req->discount_amount ;
        $voucher->starts_at = $req->starts_at;
        $voucher->expires_at = $req->expires_at;
        $voucher->save();
        return redirect('admin/voucher/add/')->with('success','Bạn thêm thành công');
    }
    public function getEdit($id){
        $voucher = voucher::find($id);
        return view('admin.voucher.edit',['voucher'=>$voucher]);
    }
    public function postEdit(Request $req, $id){
         $this->validate($req,
         [
            'code'=>'required',
            'name'=>'required',
            // 'description'=>'required',
            'max_uses_user'=>'required',
            'discount_amount'=>'required',
            'starts_at'=>'required',
            'expires_at'=>'required'
        ],
        [
            'code.required'=>'code không được trống !!!',
            'name.required'=>'name không được trống !!!',
            // 'description.required'=>'description không được trống !!!',
            'max_uses_user.required'=>'max_uses_user không được trống !!!',
            'discount_amount.required'=>'discount_amount không được trống !!!',
            'starts_at.required'=>'starts_at không được trống !!!',
            'expires_at.required'=>'expires_at không được trống !!!',
        ]);
        $voucher = Voucher::find($id);
        $voucher->code = $req->code;
        $voucher->name = $req->name;
        $voucher->expires_at = $req->expires_at;
        $voucher->description = $req->description ;
        $voucher->max_uses_user  = $req->max_uses_user;
        $voucher->discount_amount = $req->discount_amount ;
        $voucher->starts_at = $req->starts_at;
        $voucher->save();
        return redirect('admin/voucher/edit/'.$id)->with('success','Bạn sửa thành công');
    }
    public function getDel($id)
       {
          $voucher = Voucher::find($id);
          $voucher->delete();

          return redirect('admin/voucher/list')->with('success','Bạn đã xóa thành công.');
       }
}
