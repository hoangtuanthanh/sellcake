<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CustomerRequest;
use Illuminate\Support\Str;
use App\Customer;

class CustomerController extends Controller
{
    public function getList(){
    	$customer = Customer::all();
    	return view('admin.customer.list',['customer'=>$customer]);
    }
     public function getAdd(){
    	return view('admin.customer.add');
    }
    public function postAdd(Request $req){
        $message =[
         'required' =>' :attribute không được trống !!!.',
         'numeric' => ':attribute phải là kiểu số !!!'
      ];
      $this->validate($req,
      [
         'name' => 'required',
         'content'   =>'required',
         'gioitinh'=>'required',
         'email'   =>'required',
         'address'=>'required',
         'phone'=>'required|numeric' 
      ],$message);

    	$customer = new Customer;
    	$customer->name = $req->name;
    	$customer->gioitinh  = $req->gioitinh;
    	$customer->email  = $req->email;
    	$customer->address  = $req->address;
    	$customer->phone  = $req->phone;
    	$customer->note  = $req->content; //$req->content: content la name của form
    	$customer->save();
    	return redirect('admin/customer/add/')->with('success','Bạn thêm thành công');
    }
     public function getEdit($id){
     	$customer = Customer::find($id);
    	return view('admin.customer.edit',['customer'=>$customer]);
    }
    public function postEdit(Request $req, $id){
          $message =[
         'required' =>' :attribute không được trống !!!.',
         'numeric' => ':attribute phải là kiểu số !!!',
      ];
      $this->validate($req,
      [
         'name' => 'required',
         'note '   =>'required',
         'gioitinh '=>'required',
         'email '   =>'required',
         'address '=>'required',
         'phone '=>'required|numeric' 
      ],$message);
    	$customer = Customer::find($id);
    	$customer->name =$req->name;
    	$customer->gioitinh  =$req->gioitinh ;
    	$customer->gioitinh  =$req->gioitinh ;
    	$customer->address  =$req->address ;
    	$customer->phone  =$req->phone ;
    	$customer->note  =$req->note ;
    	$customer->save();
    	return redirect('admin/customer/edit/'.$id)->with('success','Bạn sửa thành công');
    }

    public function getDel($id)
	   {
	      $customer = Customer::find($id);
	      $customer->delete();

	      return redirect('admin/customer/list')->with('success','Bạn đã xóa thành công.');
	   }
}
