<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use Illuminate\Support\Facades\Auth;
use App\Product;
use App\User;
use App\Bill;
use App\News;
use App\Customer;
use App\BillDetail;
use App\Subcriber;
use App\Charts\sales;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Charts\BillChart;
use App\Charts\UserChart;
use App\Charts\SubcriberChart;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
// use ConsoleTVs\Charts\Charts;
// use ArielMejiaDev\LarapexCharts\LarapexCharts;
// use ArielMejiaDev\LarapexCharts;
// use LarapexChart;
// use ArielMejiaDev\LarapexCharts\LarapexCharts;
// use ArielMejiaDev\LarapexCharts\LarapexCharts;
// 

class LoginController extends Controller
{
	public function getLocDonHangBill($id){
	   $bill = Bill::where('status',$id)->orderBy('date_order','DESC')->get()->toArray();
	   $stt = 1;
	   foreach($bill as $val)
	   {
	    ?>
	    <tr>
	        <td><?php echo '00000'.$val['id']; ?></td>
	        <td><?php echo $val['id_customer']; ?></td>
	        <td><?php echo $val['date_order']; ?></td>
	        <td><?php echo number_format($val['total']).'đ'; ?></td>
	       
	        <td><?php echo $val['payment']; ?></td>
	        <td><?php if($val['status'] ==1){  ?>
	            <span style="color: blue">Chờ giao hàng</span>
	            <?php } if($val['status'] ==2){  ?>
	            <span style="color: blue">Xác nhận giao hàng</span>
	            <?php } if($val['status'] ==3){  ?>
	            <span style="color: blue">Đang giao hàng</span>
	        <?php } ?>
	        </td>
	        <td> <a href="<?php echo route('product.detail',['id' => $val['id']]); ?>">Chi tiết</a></td>
	    </tr>
	    <?php $stt++;
	    }
	}
	public function getDash(Request $req){
	 	$customer_count = Customer::count();
	 	$bill_count = Bill::all();
	 	$product_count = Product::count();
	 	$news_count = News::count();
	 	$bill = Bill::whereBetween('status',[1,3])
                ->orderBy('date_order','desc')->get();

        /////chart
         $chart_status = new BillChart;
         $month = date('m');
        //Biểu đồ trạng thái đơn hàng
        $status_pending = Bill::where('status',1)->whereMonth('created_at',$month)->count();
        $status_except = Bill::where('status',2)->whereMonth('created_at',$month)->count();
        $status_shipping = Bill::where('status',3)->whereMonth('created_at',$month)->count();
        $chart_status->labels(['Chờ giao hàng','Xác nhận đơn hàng','Đang giao hàng']);
        $chart_status->dataset('Trạng thái đơn hàng trong tháng: '.$month,'line',[$status_pending,$status_except,$status_shipping])
         	->color('#D9534F')
            ->backgroundcolor('#e08e8d');
        //Biểu đồ đơn hàng đã giao - đơn hàng từ chối trong tháng
       $chart_bill = new BillChart;
        $success = Bill::where('status',4)->whereMonth('created_at','=',$month)->count();
        $refuse = Bill::where('status',5)->whereMonth('created_at','=',$month)->count();

        $chart_bill->labels(['Đã giao hàng','Từ chối nhận hàng']);
        $chart_bill->dataset('Đơn hàng trong tháng: '.$month,'bar',[$success,$refuse])
        			->color(['#f0ad4e','#5cb85c'])
        			->backgroundcolor(['#e2b77a','#90ce90']);

        //Biểu đồ thống kê doanh thu theo từng tháng
        $chart_total = new BillChart;
        $nownow1 = Carbon::now();
        $nownow2 = Carbon::now();
        $nownow3 = Carbon::now();
        $nownow4 = Carbon::now();
        $nownow5 = Carbon::now();
        $nownow6 = Carbon::now();
        $nownow7 = Carbon::now();
        $nownow8 = Carbon::now();
        $nownow9 = Carbon::now();
        $nownow10 = Carbon::now();
        $nownow11 = Carbon::now();
        $now1 =  $nownow1->subMonths(1);
        $now2 =  $nownow2->subMonths(2);
        $now3 =  $nownow3->subMonths(3);
        $now4 =  $nownow4->subMonths(4);
        $now5 =  $nownow5->subMonths(5);
        $now6 =  $nownow6->subMonths(6);
        $now7 =  $nownow7->subMonths(7);
        $now8 =  $nownow8->subMonths(8);
        $now9 =  $nownow9->subMonths(9);
        $now10 =  $nownow10->subMonths(10);
        $now11 =  $nownow11->subMonths(11);
        $month = date('m');
        $year = date('Y');
        $total_m9 = Bill::where('status',4)->whereMonth('date_order',$now11)->sum('total');
        $total_m10 = Bill::where('status',4)->whereMonth('date_order',$now10)->sum('total');
        $total_m11 = Bill::where('status',4)->whereMonth('date_order',$now9)->sum('total');
        $total_m12 = Bill::where('status',4)->whereMonth('date_order',$now8)->sum('total');
        $total_m1 = Bill::where('status',4)->whereMonth('date_order',$now7)->sum('total');
        $total_m2 = Bill::where('status',4)->whereMonth('date_order',$now6)->sum('total');
        $total_m3 = Bill::where('status',4)->whereMonth('date_order',$now5)->sum('total');
        $total_m4 = Bill::where('status',4)->whereMonth('date_order',$now4)->sum('total');
        $total_m5 = Bill::where('status',4)->whereMonth('date_order',$now3)->sum('total');
        $total_m6 = Bill::where('status',4)->whereMonth('date_order',$now2)->sum('total');
        $total_m7 = Bill::where('status',4)->whereMonth('date_order',$now1)->sum('total');
        $total_m8 = Bill::where('status',4)->whereMonth('date_order',$month)->sum('total');
      
        $chart_total->labels(['Jan', 'Feb', 'Mar','April', 'May', 'Jun','July', 'August', 'Sep','Oct', 'Nov', 'Dec']);
        $chart_total->dataset('Thống kê doanh thu từng tháng trong năm '.$year,'line',[$total_m1,$total_m2,$total_m3,$total_m4,$total_m5,$total_m6,$total_m7,$total_m8,$total_m9,$total_m10,$total_m11,$total_m12])
        			->color(['#e2c091','#e2bc85','#e5b97b','#e5b470','#e0ac62','#e0a757','#e0a24c','#db993d','#db9432','#db9027','#db8c1e','#d88613'])
            		->backgroundcolor(['#e0a24c']);
        //thống kê số tài khoản user và tài khoản sô tài khoản trong tháng
         $user = new UserChart;
         $user_month =User::where('role',0)->whereMonth('created_at','=',$month)->count();
         $user_total =User::where('role',0)->count();
         $imploy_month =User::where('role',2)->count();
         $admin_total =User::where('role',1)->count();
         $user->labels(['Số User trong tháng '.$month,'Tổng số User','Tổng số Imployee','Tổng số Admin']);
         $user->dataset('Thống kê tổng số User trong hệ thống','bar',[$user_month,$user_total,$imploy_month,$admin_total])
         		->color(['#f0ad4e','#5cb85c','#337AB7','#D9534F'])
        		->backgroundcolor(['#e2b77a','#90ce90','#77abd6','#db8583']);
         			
        //thống kê số người đăng kí tổng và số người đăng kí trong tháng
        $chart_sub = new SubcriberChart;
        $sub_month = Subcriber::whereMonth('created_at',$month)->count();
        $sub_total = Subcriber::count();
        $chart_sub->labels(['Số người đăng kí trong tháng','Tổng số đăng kí ']);
        $chart_sub->dataset('Thống kê khách hàng đăng kí email nhận thông tin','pie',[$sub_month,$sub_total])
        		->color(['#337AB7','#D9534F'])
        		->backgroundcolor(['#77abd6','#db8583']);
                
    return view ('admin.dashboard',['customer_count'=>$customer_count,'bill_count'=>$bill_count,'product_count'=>$product_count,'news_count'=>$news_count,'bill'=>$bill,'chart_bill'=>$chart_bill,'chart_status'=>$chart_status,'chart_total'=>$chart_total,'user'=>$user,'chart_sub'=>$chart_sub]);
   }
	public function getLoginAdmin(){
		return view('admin.loginAdmin');

	}
	public function postLoginAdmin(Request $req){
		 $message =[
         'required' =>' :attribute không được trống !!!.',
          'email' =>' :attribute phải có định dạng email !!!.',
         'numeric' => ':attribute phải là kiểu số !!!'
	      ];
	      $this->validate($req,
	      [
	        
	         'email'   =>'required|email',
	         'password'=>'required',
	      ],$message);
		$arr= [
		'email'=>$req->email,
		'password'=>$req->password,
		'lock' =>1
		];
		if (Auth::attempt($arr)){

		    	return redirect('admin/dash');
	    }
	    else{
	    	return redirect()->back()->with('thongbao','Đăng nhập thất bại hoặc tài khoản bị khóa');
	    }
		
	}
	public function getlistBill(){
		$listboard = Bill::all();
		// dd($listboard );
		return view('admin.dashboard',['listboard'=>$listboard]);
	}

    function fetch_data(Request $request)
   {
    if($request->ajax())
	    {
		    if($request->form_date != '' && $request->to_date != '')
		    {
		      $data = DB::table('bills')
		          ->whereBetween('created_at',array($request->form_date,$request->to_date))
		          ->get();
		    }
		    else
		    {
		      $data = DB::table('bills')->orderBy('id','desc')->get();
		    }
		    echo json_encode($data);
	    }
	}
	public function getLogout(){
		Auth::logout();
		return redirect('login-admin');
	}
	

}
