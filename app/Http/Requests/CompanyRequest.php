<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address'=>'required|min:2|max:50'.$this->id,
            'tax_number'=>'required|numeric',
            'phone'=>'required',
            'email'=>'required|email',
            'hotline'=>'required',
            'website'=>'required',
        ];
    }
}
