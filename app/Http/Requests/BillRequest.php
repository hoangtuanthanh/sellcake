<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'id_customer'=>'required|numeric'.$this->id,
            'date_order'=>'required|date',
            'total'=>'required|numeric',
            'payment'=>'required',
            'note'=>'required',
        ];
    }
}
