<?php

namespace App;
use App\Product;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";
    public function BillDetail(){
    	return $this->hasMany('App\BillDetail','id_product','id');
    }
    public function TypeProduct(){
    	return $this->belongsTo('App\TypeProduct','id_type','id');
    }

}



