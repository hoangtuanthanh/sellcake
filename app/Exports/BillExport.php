<?php

namespace App\Exports;

use App\Bill;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Illuminate\Contracts\View\View;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

// use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromView;
// 
class BillExport implements FromCollection,WithMapping,WithHeadings,WithEvents
{
	use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */
    // public function __construct( string $_start, string $_end)
    // {
    //     $this->start_date = $_start;
    //     $this->end_date = $_end;
    // }
    public function registerEvents(): array
    {
        $styleArray = [
            'font' => [
                'bold' => true,
            ]
        ];
            // Handle by a closure.
        return [
            AfterSheet::class => function(AfterSheet $event) use($styleArray) {
                $event->sheet->getStyle('A1:I1')->applyFromArray($styleArray);
                $event->sheet->setCellValue('I2','=SUM(E2:E10000)');
            },
            
           
        ];
    }

    public function collection()
    {
    	// $start = $this->start_date;
    	// $end = $this->end_date;
   		// return Bill::where('status',4)
     //              ->whereDate('date_order','>=',$start)
     //              ->whereDate('date_order','<=',$end)
     //              ->orderBy('id','desc')
     //              ->get();
                
    $month = date('m');
    return Bill::where('status',4)->whereMonth('date_order',$month)->orderBy('id','desc')->get();
    }
    //đổ dữ liệu
     public function map($bill): array
    {
        return [
            '00000'.$bill->id,
            $bill->Customer->name,
            Carbon::parse($bill->date_order )->format('d-m-Y'),
            'No name',
            // number_format($bill->total).' '.'VNĐ',
            $bill->total,
            $bill->payment,
            $bill->note,
           	Carbon::parse($bill->updated_at )->format('d-m-Y'),
        ];
    }
    //đặt tên cột
    public function headings(): array
    {
        return [
            'Mã đơn hàng',
            'Tên khách hàng',
            'Ngày đặt hàng',
            'Người gửi',
            'Giá(vnđ)',
            'Hình thức thanh toán',
            'Ghi chú',
            'Ngày cập nhật',
            'Tổng tiền(vnđ)',
        ];
    }
}
