<?php

namespace App\Exports\Sheets;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;

class BillPerMonth implements WithTitle
{
  

    /**
     * @return Builder
     */
    public function view(string $start,string $end): view
    {
        $start= $this->start_date;
        $end= $this->end_date;
        return Bill
            ::query()
                    ->where('status',4)
                  ->whereDate('date_order','>=',$start)
                  ->whereDate('date_order','<=',$end)
                  ->get();
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Month';
    }
}