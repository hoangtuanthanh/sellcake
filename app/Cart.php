<?php

namespace App;
use App\Product;
class Cart
{
	public $items = null;
	public $totalQty = 0;
	public $totalPrice = 0;
	

	public function __construct($oldCart){
		if($oldCart){
			$this->items = $oldCart->items;
			$this->totalQty = $oldCart->totalQty;
			$this->totalPrice = $oldCart->totalPrice;
		}
	}
	public function add($item, $id){
	  $giohang = ['qty'=>0, 'price' => $item->unit_or_promotion_pice, 'unit_price' => $item->unit_price, 'promotion_pice' => $item->promotion_pice, 'item' => $item];
	  if($this->items){
	   if(array_key_exists($id, $this->items)){
	    $giohang = $this->items[$id];
	   }
	  }
	  $giohang['qty']++;
	  if($item->promotion_pice == 0) {
	   $item->unit_or_promotion_pice = $item->unit_price;
	  } else {
	   $item->unit_or_promotion_pice = $item->promotion_pice;
	  }
	  $giohang['price'] = $item->unit_or_promotion_pice * $giohang['qty'];
	  $this->items[$id] = $giohang;
	  $this->totalQty++;
	  if($item->promotion_pice == 0) {
	   $this->totalPrice +=  $item->unit_price;
	  }else {
	   $this->totalPrice +=  $item->promotion_pice;
	  }
	 }
	// public function add($item, $id, $qty = NULL){
	// 	$price_or_discount = $item->price;
	// 	if($item->discount != 0){
	// 		$price_or_discount = $item->discount;
	// 	}

	// 	$cart = ['qty'=>0, 'price' => $price_or_discount, 'unit'=>$item->unit, 'item' => $item];
	// 	if($this->items){
	// 		if(array_key_exists($id, $this->items)){
	// 			$cart = $this->items[$id];
	// 		}
	// 	}

	// 	if($qty != NULL){
	// 		$cart['qty']   += $qty;
	// 		$cart['price']  = $price_or_discount * $cart['qty'];

	// 		if($this->items){
	// 			if(array_key_exists($id, $this->items)){
	// 				$this->removeItem($id);
	// 				$this->totalQty   += $cart['qty'];
	// 				$this->totalPrice += $cart['price'];
	// 				$this->items[$id]  = $cart;
	// 			}
	// 			else{
	// 				$this->totalQty   += $cart['qty'];
	// 				$this->totalPrice += $cart['price'];
	// 				$this->items[$id]  = $cart;
	// 			}
	// 		}
	// 		else{
	// 			$this->totalQty   += $cart['qty'];
	// 			$this->totalPrice += $cart['price'];
	// 			$this->items[$id]  = $cart;
	// 		}
	// 	}
	// 	else{
	// 		$cart['qty']++;
	// 		$cart['price'] = $price_or_discount * $cart['qty'];

	// 		$this->totalQty++;
	// 		$this->totalPrice += $price_or_discount;
	// 		$this->items[$id]  = $cart;
	// 	}
	// }
	// ///////////////
	// 
	// public function add($item, $id){
	// 	$giohang = ['qty'=>0, 'price' => $item->unit_price, 'item' => $item];
	// 	if($this->items){
	// 		if(array_key_exists($id, $this->items)){
	// 			$giohang = $this->items[$id];
	// 		}
	// 	}
	// 	$giohang['qty']++;
	// 	$giohang['price'] = $item->unit_price * $giohang['qty'];
	// 	$this->items[$id] = $giohang;
	// 	$this->totalQty++;
	// 	$this->totalPrice += $item->unit_price;
	// }
	// public function add($item, $id){
	// 	$giohang = ['qty'=>0, 'price' => $item->unit_or_promotion_pice, 'unit_price' => $item->unit_price, 'promotion_pice' => $item->promotion_pice, 'item' => $item];
	// 	if($this->items){
	// 		if(array_key_exists($id, $this->items)){
	// 			$giohang = $this->items[$id];
	// 		}
	// 	}
	// 	$giohang['qty']++;
	// 	if($item->promotion_pice == 0) {
	// 		$item->unit_or_promotion_pice = $item->unit_price;
	// 	} else {
	// 		$item->unit_or_promotion_pice = $item->promotion_pice;
	// 	}
	// 	$giohang['price'] = $item->unit_or_promotion_pice * $giohang['qty'];
	// 	$this->items[$id] = $giohang;
	// 	$this->totalQty++;
	// 	$this->totalPrice += $item->unit_or_promotion_pice;
	// }
	//xóa 1
	public function reduceByOne($id){
		$this->items[$id]['qty']--;
		$this->items[$id]['price'] -= $this->items[$id]['item']['price'];
		$this->totalQty--;
		$this->totalPrice -= $this->items[$id]['item']['price'];
		if($this->items[$id]['qty']<=0){
			unset($this->items[$id]);
		}
	}
	//xóa nhiều
	public function removeItem($id){
		$this->totalQty -= $this->items[$id]['qty'];
		$this->totalPrice -= $this->items[$id]['price'];
		unset($this->items[$id]);
	}
}
// use App\Product;
// class Cart{
// 	public $product = null;
// 	public $totalPrice = 0;
// 	public $totalQty = 0;
// 	public function __construct($cart){
// 		if($cart){
// 			$this->product = $cart->products;
// 			$this->totalPrice = $cart->totalPrice;
// 			$this->totalQty = $cart->totalQty;
// 		}
// 	}
// 	public function AddCart($product,$id){
// 		$newProduct = ['qty'=>0,'price'=>$product->price,'productInfo'=>$products];
// 		if($this->products){
// 			if(array_key_exists($id, $products)){
// 				$newProduct = $products[$id];
// 			}
// 		}
// 		$newProduct['qty']++;
// 		$newProduct['price'] =  $newProduct['qty'] * $products->price;
// 		$this->products[$id] = $newProduct;
// 		$this->totalPrice += $products->price;
// 		$this->totalQty++;
// 	}
// }
